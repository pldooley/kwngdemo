source ./env.sh
docker stop $HCN_CONT_PROJ
docker rm $HCN_CONT_PROJ
docker run -it -p $HCN_HOST:$HCN_PORT:$HCN_PORT -v $PWD:$HCN_DIR_PROJ --name $HCN_CONT_PROJ $HCN_IMG_PROJ
