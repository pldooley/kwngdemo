/**********************************************************************
 *
 * kw/type/kwLinkType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

export class kwLinkType
{
	sUrl: string;
	sIcon: string;
}
