/**********************************************************************
 *
 * kw/type/kwTitleType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
// @formatter:on

export class kwTitleType
{
	sMain: string;
	sSub: string;
	sThird?: string;
	nHeight?: number;
}
