/**********************************************************************
 *
 * kw/type/kwLogoType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

export class kwLogoType
{
	sUrl: string;
	sAlt?: string
}
