// @formatter:off
import { NgModule }							from '@angular/core';

import { JsonSchemaFormModule }             from 'angular4-json-schema-form';

import { APP_CONFIG, AppConfig }			from "./kwAppBS";
import { kwErrData }						from "./state/err/kwErrData";
import { kwHttpSrvc }						from "./http/kwHttpSrvc";
import { kwHttpUrlSrvc }					from "./http/kwHttpUrlSrvc";
import { kwKeySrvc }						from "./key/kwKeySrvc";

import { kwApisApi }					    from "./main/apis/kwApisApi";
import { kwApisData }					    from "./main/apis/kwApisData";
import { kwApisHttp }					    from "./main/apis/kwApisHttp";
import { kwApisMsg }					    from "./main/apis/kwApisMsg";
import { kwApisState }						from "./main/apis/kwApisState";
import { kwAppData }					    from "./state/app/kwAppData";
import { kwAppState }						from "./state/app/kwAppState";
import { kwBSApi }		                    from "./util/BS/kwBSApi";
import { kwBSData }		                    from "./util/BS/kwBSData";
import { kwBSHttp }					        from "./util/BS/kwBSHttp";
import { kwBSMsg }		                    from "./util/BS/kwBSMsg";
import { kwBSState }					    from "./util/BS/kwBSState";
import { kwErrData }					    from "./state/err/kwErrData";
import { kwHostKey }						from "./key/host/kwHostKey";
import { kwHostKeyData }					from "./key/host/kwHostKeyData";
import { kwIdKey }							from "./key/id/kwIdKey";
import { kwIdKeyData }						from "./key/id/kwIdKeyData";
import { kwMetricApi }				        from "./main/metric/kwMetricApi";
import { kwMetricData }				        from "./main/metric/kwMetricData";
import { kwMetricForm }				        from "./main/metric/kwMetricForm";
import { kwMetricHttp }					    from "./main/metric/kwMetricHttp";
import { kwMetricMdl }				        from "./main/metric/kwMetricMdl";
import { kwMetricMsg }			            from "./main/metric/kwMetricMsg";
import { kwMetricState }					from "./main/metric/kwMetricState";
import { kwModelsApi }				        from "./main/models/kwModelsApi";
import { kwModelsData }				        from "./main/models/kwModelsData";
import { kwModelsHttp }					    from "./main/models/kwModelsHttp";
import { kwModelsMsg }			            from "./main/models/kwModelsMsg";
import { kwModelsState }					from "./main/models/kwModelsState";
import { kwOrgApi }					        from "./main/org/kwOrgApi";
import { kwOrgData }					    from "./main/org/kwOrgData";
import { kwOrgForm }				        from "./main/org/kwOrgForm";
import { kwOrgHttp }						from "./main/org/kwOrgHttp";
import { kwOrgMdl }				            from "./main/org/kwOrgMdl";
import { kwOrgMsg }					        from "./main/org/kwOrgMsg";
import { kwOrgState }						from "./main/org/kwOrgState";
import { kwPortKey }						from "./key/port/kwPortKey";
import { kwPortKeyData }					from "./key/port/kwPortKeyData";
import { kwRedirectApi }				    from "./main/redirect/kwRedirectApi";
import { kwRedirectData }				    from "./main/redirect/kwRedirectData";
import { kwRedirectForm }				    from "./main/redirect/kwRedirectForm";
import { kwRedirectHttp }					from "./main/redirect/kwRedirectHttp";
import { kwRedirectMdl }				    from "./main/redirect/kwRedirectMdl";
import { kwRedirectMsg }			        from "./main/redirect/kwRedirectMsg";
import { kwRedirectState }					from "./main/redirect/kwRedirectState";

// @formatter:on

@NgModule(
	{
		imports:
			[],
		declarations:
			[
				JsonSchemaFormModule,
				kwApisHttp,
				kwApisState,
				kwAppState,
				kwBSHttp,
				kwBSState,
				kwHostKey,
				kwIdKey,
				kwMetricHttp,
				kwMetricState,
				kwOrgHttp,
				kwOrgState,
				kwPortKey,
				kwRedirectHttp,
				kwRedirectState,
			],
		providers:
			[
				{
					provide: APP_CONFIG,
					useValue: AppConfig
				},
				kwApisApi,
				kwApisMsg,
				kwApisData,
				kwAppData,
				kwBSApi,
				kwBSData,
				kwBSMsg,
				kwErrData,
				kwHostKeyData,
				kwHttpSrvc,
				kwHttpUrlSrvc,
				kwIdKeyData,
				kwKeySrvc,
				kwMetricApi,
				kwMetricData,
				kwMetricForm,
				kwMetricMdl,
				kwMetricMsg,
				kwModelsApi,
				kwModelsData,
				kwModelsMsg,
				kwOrgApi,
				kwOrgData,
				kwOrgForm,
				kwOrgMdl,
				kwOrgMsg,
				kwPortKeyData,
				kwRedirectApi,
				kwRedirectData,
				kwRedirectForm,
				kwRedirectMdl,
				kwRedirectMsg,
			],
		exports:
			[
				kwApisHttp,
				kwApisState,
				kwAppState,
				kwBSHttp,
				kwBSState,
				kwHostKey,
				kwIdKey,
				kwMetricHttp,
				kwMetricState,
				kwModelsHttp,
				kwModelsState,
				kwOrgHttp,
				kwOrgState,
				kwPortKey,
				kwRedirectHttp,
				kwRedirectState,
			]
	})
export class kwModule {}
