/**********************************************************************
 *
 * /kw/main/BS/kwBSMsg.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:on
import { EventEmitter }             from '@angular/core';
import { Injectable }	            from '@angular/core';

import { kwApi }                    from "../../class/api/kwApi";
import { kwHttpAct }                from "../../http/kwHttpAct";
import { kwMsg }                    from "../../class/msg/kwMsg";

import { kwBSApi }                  from "./kwBSApi";
// @formatter:off


@Injectable()
export class kwBSMsg
{

	private broadcast: EventEmitter<kwMsg>;

	data: kwMsg;

	changed$: EventEmitter<kwMsg>;

	constructor(private srvcApi: kwBSApi)
	{
		//console.log("srvcHttpMetrixkwMsg::constructor() called.");

		this.broadcast = new EventEmitter<kwMsg>();
		this.changed$=this.broadcast;
	}

	actionAdd(data, params)
	{
		//console.log("kwBSMsg::actionAdd() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwBSMsg::actionAdd() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.add(data, params, api);
		if (!kwMsg.is(msg))
		{
			console.error("kwBSMsg::actionAdd() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionDelete(params)
	{
		//console.log("kwBSMsg::actionDelete() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwBSMsg::actionDelete() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.delete(params, api);
		if (!kwMsg.is(msg))
		{
			console.error("kwBSMsg::actionDelete() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionEdit(data, params)
	{
		//console.log("kwBSMsg::actionEdit() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwBSMsg::actionEdit() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.edit(data, params, api);
		if (!kwMsg.is(msg))
		{
			console.error("kwBSMsg::actionEdit() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionGet(params)
	{
		//console.log("kwBSMsg::actionGet() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwBSMsg::actionGet() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.get(params, api);
		if (!kwMsg.is(msg))
		{
			console.error("kwBSMsg::actionGet() msg is invalid.");
			return;
		}

		this.change(msg);

	};

	change(data: kwMsg)
	{
		//console.log("srvcHttpMetrixAction::change() consolealled.");

		if (!data)
		{
			console.error("srvcHttpMetrixAction::change() data is invalid.");
			return
		}
		//console.info("srvcHttpMetrixAction::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("srvcHttpMetrixAction::clear() called.");
		this.data = null;
	};

	retrieve(): kwMsg
	{
		//console.log("srvcHttpMetrixAction::retrieve() called.");

		return this.data;
	};

}
