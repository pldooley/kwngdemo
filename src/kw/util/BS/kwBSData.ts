/**********************************************************************
 *
 * kw/util/BS/kwBSData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }                 from '@angular/core';
import { Injectable }                   from '@angular/core';

import { kwBSType }                     from './kwBSType';
// @formatter:on


@Injectable()
export class kwBSData
{

	private broadcast: EventEmitter<kwBSType>;

	data: kwBSType;

	changed$: EventEmitter<kwBSType>;

	constructor()
	{
		//console.log("kwBSData::constructor() called.");

		this.broadcast = new EventEmitter<kwBSType>();
		this.changed$ = this.broadcast;
	}

	change(data: kwBSType)
	{
		//console.log("kwBSData::change() called.");

		if( !data )
		{
			console.error("kwBSData::change() data is invalid.");
			return
		}
		console.info("kwBSData::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("kwBSData::clear() called.");
		this.data = null;
	};

	retrieve(): kwBSType
	{
		//console.log("kwBSData::retrieve() called.");
		return this.data;
	};

}
