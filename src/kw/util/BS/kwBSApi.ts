/**********************************************************************
 *
 * kw/util/BS/kwBSApi.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/
// @formatter:off
import {EventEmitter, Injectable}   from '@angular/core';

import { kwApi }                    from "../../class/api/kwApi";
import { kwApiSrvc }                from "../../class/api/kwApiSrvc";

// @formatter:on


@Injectable()
export class kwBSApi
{

	private broadcast: EventEmitter<kwApi>;

	data: kwApi;

	changed$: EventEmitter<kwApi>;

	constructor()
	{
		//console.log("kwBSApi::constructor() called.");
		this.broadcast = new EventEmitter<kwApi>();
		this.changed$ = this.broadcast;
	}

	change(data: kwApi): void
	{
		//console.log("kwBSApi::change() called.");

		if(!kwApiSrvc.isType(data))
		{
			console.error("kwBSApi::change() data is invalid.");
			return
		}
		//console.info("kwBSApi::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("kwBSApi::clear() called.");
		this.data = null;
	};

	retrieve(): kwApi
	{
		//console.log("kwBSApi::retrieve() called.");
		return this.data;
	};

}
