/**********************************************************************
 *
 * kw/util/BS/kwBSState.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
import { Component }						from '@angular/core';
import { OnInit }							from '@angular/core';
import { OnDestroy }						from '@angular/core';
import { Subscription }						from 'rxjs/Subscription';

import { kw }		                        from "../../kw";
import { kwApiKeyData }                     from "../../key/api/kwApiKeyData";
import { kwIdKeyData }						from "../../key/id/kwIdKeyData";
import { kwHostKeyData }					from "../../key/host/kwHostKeyData";
import { kwPortKeyData }					from "../../key/port/kwPortKeyData";

import { kwBSMsg }                          from "./kwBSMsg";
import { kwBSType }					        from "./kwBSType";
// @formatter:on


@Component({
	selector: 'kw-bootstrap-state',
	template: ``,
})
export class kwBSState implements OnInit, OnDestroy
{
	subApi:     Subscription;
	subHost:    Subscription;
	subId:      Subscription;
	subPort:    Subscription;

	constructor(    private srvcApi:    kwApiKeyData,
	                private srvcHost:   kwHostKeyData,
	                private srvcId:     kwIdKeyData,
					private srvcPort:   kwPortKeyData,
					private srvcMsg:    kwBSMsg         )
	{
		//console.log("kwBSState::constructor()called");
	}

	ngOnInit()
	{
		//console.log("kwBSState::ngOnInit()called");

		this.subApi = this.srvcApi.changed$.subscribe(sHost =>
		{
			this.store();
		});

		this.subHost = this.srvcHost.changed$.subscribe(sHost =>
		{
			this.store();
		});

		this.subId = this.srvcId.changed$.subscribe(nId =>
		{
			this.store();
		});

		this.subPort = this.srvcPort.changed$.subscribe(nPort =>
		{
			this.store();
		});

		this.store();
		this.load();
	};

	ngOnDestroy()
	{
		//console.log("kwBSState::ngOnDestroy()called.");

		this.subApi.unsubscribe();
		this.subHost.unsubscribe();
		this.subId.unsubscribe();
		this.subPort.unsubscribe();
	}

	load()
	{
		//console.log("kwBSState::load() called.");
		this.srvcMsg.actionGet([]);
	}

	store()
	{
		//console.log("kwBSState::store() called.");

		let sApi: string = this.srvcApi.retrieve();
		if( !kw.isString(sApi ))
		{
			//console.info("kwBSState::store() sApi not available.");
			return;
		}
		//console.info("kwBSState::store() sApi is [", sApi, "].");

		let sHost: string = this.srvcHost.retrieve();
		if( !kw.isString(sHost ))
		{
			//console.info("kwBSState::store() sHost not available.");
			return;
		}
		//console.info("kwBSState::store() sHost is [", sHost, "].");

		let nId: number = this.srvcId.retrieve();
		if( !kw.isNumber(nId ))
		{
			//console.info("kwBSState::store() nId not available.");
			return;
		}
		//console.info("kwBSState::store() nId is [", nId, "].");

		let nPort: number = this.srvcPort.retrieve();
		if( !kw.isNumber(nPort ))
		{
			//console.info("kwBSState::store() nPort not available.");
			return;
		}
		//console.info("kwBSState::store() nPort is [", nPort, "].");

		let data: kwBSType =
		{
			sApi: sApi,
			sHost: sHost,
			nId: nId,
			nPort: nPort,
		}
		//console.info("kwBSState::store() nPort is [", nPort, "].");

	}


}
