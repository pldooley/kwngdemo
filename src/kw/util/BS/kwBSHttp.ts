/**********************************************************************
 *
 * kw/main/BS/kwBSHttp.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Component }					from '@angular/core';
import { OnInit }						from '@angular/core';
import { OnDestroy }					from '@angular/core';
import { Subscription }					from 'rxjs/Subscription';

import { kw }					        from '../../kw';
import { kwHttpMsg }					from '../../http/kwHttpMsg';
import { kwMsg }                        from "../../class/msg/kwMsg";

import { kwBSData }                     from "./kwBSData";
import { kwBSMsg }                      from "./kwBSMsg";
// @formatter:on


const sSTATE: string = "account";

@Component({
	selector: 'kw-bootstrap-http',
	template: ``,
})
export class kwBSHttp implements OnInit, OnDestroy
{

	subMsg: Subscription;

	constructor(    private srvcMsg: kwBSMsg,
	                private srvcData: kwBSData )
	{
		//console.log("kwBSHttp::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwBSHttp::ngOnInit() called");

		this.subMsg = this.srvcMsg.changed$.subscribe(msg =>
		{
			this.inspect(msg);
		});
	};

	ngOnDestroy()
	{
		//console.log("kwBSHttp::ngOnDestroy() called.");

		this.subMsg.unsubscribe();
	}

	execute(msg: any): void
	{
		//console.log("kwBSHttp::execute() called");

		let promise = kwHttpMsg.single(msg);

		if (!kw.isNull(promise))
		{
			console.error("kwBSHttp::execute() promise is invalid.");
			return;
		}

		promise.then(function (data)
		{
			//console.info("kwBSHttp::execute::then() called.");

			if (!kw.isNull(data))
			{
				if (!msg.isDelete())
				{
					console.error("kwAccHttp::execute::then() data is invalid");
					return;
				}
			}

			//console.info("kwBSHttp::execute::then() data is ", data);

			this.srvcData.change(data);

		}), function(reason)
		{
			console.error("kwAccHttp::execute::error() called");
			alert('Failed: ' + reason);
			this.srvcData.change(null);
		}
	};

	inspect(msg)
	{
		//console.log("kwBSHttp::inspectAction() called");

		if (!kwMsg.is(msg))
		{
			console.error("kwBSHttp::inspect() msg is invalid.");
			return;
		}
		console.info("kwBSHttp::inspect() msg is valid - executing.");

		this.execute(msg);
	}

}
