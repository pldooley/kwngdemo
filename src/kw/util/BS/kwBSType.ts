/**********************************************************************
 *
 * kw/util/BS/kwBSData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
// @formatter:on

export class kwBSType
{
	sApi: string;
	sHost: string;
	nId: number;
	nPort: number;
}
