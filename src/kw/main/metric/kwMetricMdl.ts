/**********************************************************************
 *
 * hcn/app/metric/kwMetricMdl.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }             from '@angular/core';
import { Injectable }	            from '@angular/core';

import { kw }			            from '../../kw';
import { kwModelSrvc }              from "../../class/model/kwModelSrvc";
// @formatter:off



@Injectable()
export class kwMetricMdl
{

	// Observable email sources
	private broadcast: EventEmitter<any>;

	data: any;

	// Observable string streams
	changed$ = this.broadcast;


	constructor()
	{
		//console.log("kwMetricMdl::constructor() called.");

		this.broadcast = new EventEmitter<any>();
		this.changed$=this.broadcast;
	}

	change(dataNew)
	{
		//console.log("kwMetricMdl::changed() called.");

		if (kw.isNull(dataNew))
		{
			console.error("kwMetricMdl::changed() dataNew is invalid.");
			return;
		}

		this.data = dataNew;
		//console.info("kwMetricMdl::changed() data is ", data);

		this.broadcast.emit(this.data);
	};

	clear()
	{
		//console.log("kwMetricMdl::clear() called.");
		data = null;
	};

	create()
	{
		//console.log("kwMetricMdl::create() called.");

		if (kw.isNull(this.data))
		{
			console.error("kwMetricMdl::create() data is invalid.");
			return false;
		}

		let rec = this.data.createRecord();
		if (kw.isNull(rec))
		{
			console.error("kwMetricMdl::xImport() rec is invalid.");
			return;
		}

		return rec;
	};

	retrieve()
	{
		return this.data;
	};

	xExport(recs)
	{
		return kwModelSrvc.xExport(this.data, recs);
	};

	xExportRec(rec)
	{
		return kwModelSrvc.xExportRec(this.data, rec);
	};

	xImport(recs)
	{
		return kwModelSrvc.xImport(this.data, recs);
	};

	xImportRec(rec)
	{
		return kwModelSrvc.xImportRec(this.data, rec);
	};

}