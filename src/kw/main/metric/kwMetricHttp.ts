/**********************************************************************
 *
 * app/util/metric/kwMetricHttp.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Component }					from '@angular/core';
import { OnInit }						from '@angular/core';
import { OnDestroy }					from '@angular/core';
import { Subscription }					from 'rxjs/Subscription';

import { kw }					        from '../../kw';
import { kwHttpMsg }					from '../../http/kwHttpMsg';

import { kwMetricMsg }	                from './kwMetricMsg';
import { kwMetricData }		            from './kwMetricData';
// @formatter:on


const sSTATE: string = "metric";

@Component({
	selector: 'hcn-metric-http',
	template: ``,
})
export class kwMetricHttp implements OnInit, OnDestroy
{

	subMsg: Subscription;

	constructor(    private srvcMsg: kwMetricMsg,
	                private srvcData: kwMetricData )
	{
		//console.log("kwMetricHttp::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwMetricHttp::ngOnInit() called");

		this.subAct = this.srvcMsg.changed$.subscribe(info =>
		{
			this.inspect(info);
		});
	};

	ngOnDestroy()
	{
		//console.log("kwMetricHttp::ngOnDestroy() called.");

		this.subAct.unsubscribe();
	}

	execute(msg: any): void
	{
		//console.log("kwMetricHttp::execute() called");

		let promise = kwHttpMsg.single(msg);

		if (!kw.isNull(promise))
		{
			console.error("kwMetricHttp::execute() promise is invalid.");
			return;
		}

		promise.then(function (data)
		{
			//console.info("kwMetricHttp::execute::then() called.");

			if (!kw.isNull(data))
			{
				if (!msg.isDelete())
				{
					console.error("kwMetricHttp::execute::then() data is invalid");
					return;
				}
			}

			//console.info("kwMetricHttp::execute::then() data is ", data);

			this.srvcData.change(data);

		}), function(reason)
		{
			console.error("kwMetricHttp::execute::error() called");
			alert('Failed: ' + reason);
			this.srvcData.change(null);
		}
	};

	inspect(msg)
	{
		//console.log("kwMetricHttp::inspectAction() called");

		if (!kwMsg.is(msg))
		{
			console.error("kwMetricHttp::inspectAction() msg is invalid.");
			return;
		}

		if (	!msg.isAdd()
		&&		!msg.isDelete()
		&&		!msg.isEdit()	)
		{
			console.info("kwMetricHttp::inspectAction() msg is invalid - doing nothing.");
			return;
		}

		console.info("kwMetricHttp::inspectAction() msg is valid - esxecuting.");

		this.execute(msg);
	}

}
