/**********************************************************************
 *
 * app/util/metric/kwMetricState.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Component }			        from '@angular/core';
import { OnInit }				        from '@angular/core';
import { OnDestroy }			        from '@angular/core';
import { Subscription }	                from 'rxjs/Subscription';

import { kwApi }                        from "../../class/api/kwApi";
import { kwApisData }                   from '../apis/kwApisData';

import { kwMetricApi }	                from './kwMetricApi';
// @formatter:off


const sSTATE: string = "metric";


@Component({
	selector: 'hcn-metric-state',
	template: ``,
})
export class kwMetricState implements OnInit, OnDestroy
{
	subApis: Subscription;

	constructor(    private srvcApi: kwMetricApi,
	                private srvcApis: kwApisData    )
	{
		//console.log("kwMetricState::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwMetricState::ngOnInit() called");

		this.subApis = this.srvcApis.changed$.subscribe(info =>
		{
			this.loadApi()
		});
	};

	ngOnDestroy()
	{
		//console.log("kwMetricState::ngOnDestroy() called.");

		this.subApis.unsubscribe();
	}

	loadApi()
	{
		//console.log("kwMetricState::loadApi() called");

		let api: kwApi = this.srvcApis.retrieveItem(sSTATE);
		if (!kwApi.is(api))
		{
			console.error("kwMetricState::retrieveApi() api is invalid.");
			return;
		}
		//console.info("kwMetricState::loadApi() api is ", api);

		this.srvcApi.change(api);
	};

}
