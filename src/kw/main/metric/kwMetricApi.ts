/**********************************************************************
 *
 * kw/main/metric/kwMetricApi.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/
// @formatter:off
import {EventEmitter, Injectable}   from '@angular/core';

import { kwApi }                    from "../../class/api/kwApi";
import { kwApiSrvc }                from "../../class/api/kwApiSrvc";

// @formatter:on


@Injectable()
export class kwMetricApi
{

	private broadcast: EventEmitter<kwApi>;

	data: kwApi;

	changed$: EventEmitter<kwApi>;

	constructor()
	{
		//console.log("kwMetricApi::constructor() called.");
		this.broadcast = new EventEmitter<kwApi>();
		this.changed$ = this.broadcast;
	}

	change(data: kwApi): void
	{
		//console.log("kwMetricApi::change() called.");

		if(!kwApiSrvc.isType(data))
		{
			console.error("kwMetricApi::change() data is invalid.");
			return
		}
		//console.info("kwMetricApi::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("kwMetricApi::clear() called.");
		this.data = null;
	};

	retrieve(): kwApi
	{
		//console.log("kwMetricApi::retrieve() called.");
		return this.data;
	};

}
