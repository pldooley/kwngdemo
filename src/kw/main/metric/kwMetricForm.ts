/**********************************************************************
 *
 * kw/util/mectric/kwMetricForm.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Injectable }	            from '@angular/core';

import { kw }			            from '../../kw';
import { kwFormEnum }               from "../../class/form/kwFormEnum";
import { kwFormSrvc }               from "../../class/form/kwFormSrvc";

import { kwMetricStateAct }         from "./kwMetricMsg";
import { kwMetricMdl }              from "./kwMetricMdl";
// @formatter:off


@Injectable()
export class kwMetricForm
{

	constructor(    private srvcMsg: kwMetricStateAct,
					private srvcMdl: kwMetricMdl  )
	{
		//console.log("kwMetricForm::constructor() called.");
	}

	create(nForm: kwFormEnum, obj: any)
	{
		//console.log("kwMetricForm::create() called");
		return kwFormSrvc.create(nForm, obj, this, this.srvcMdl);
	}

	loadMeta(obj)
	{
		//console.log("kwMetricForm::loadMeta() called.");

		if (kw.isNull(obj))
		{
			console.error("kwMetricForm::loadMeta() obj is invalid");
			return;
		}

		return obj;

	};

	save(nForm: kwFormEnum, obj): boolean
	{
		//console.log("kwMetricForm::save() called");
		return kwFormSrvc.save(nform, obj, srvcMdl, srvcAct);
	}

}




