/**********************************************************************
 *
 * app/util/metric/kwMetricData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }			 from '@angular/core';
import { Injectable }			 from '@angular/core';

import { kw }                    from "../../kw";
// @formatter:off



@Injectable()
export class kwMetricData
{

	private broadcast: EventEmitter<object>;

	data: object;

	changed$: EventEmitter<object>;

	constructor()
	{
		//console.log("kwMetricData::constructor() called.");

		this.broadcast = new EventEmitter<object>();
		this.changed$=this.broadcast;
	}

	change(data: object)
	{
		//console.log("kwMetricData::change() called.");

		if (kw.isNull(data))
		{
			console.error("kwMetricData::change() data is invalid.");
			return
		}
		//console.info("kwMetricData::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("kwMetricData::clear() called.");
		this.data = null;
	};

	retrieve() : object
	{
		//console.log("kwMetricData::retrieve() called.");
		return this.data;
	};

}
