/**********************************************************************
 *
 * /kw/main/apis/kwApisMsg.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:on
import { EventEmitter }             from '@angular/core';
import { Injectable }	            from '@angular/core';

import { kwActSrvc }                from "../../class/act/kwActSrvc";
import { kwActType }				from '../../class/act/kwActType';
import { kwApisType }               from "../../class/apis/kwApisType";
import { kwHttpAct }                from "../../http/kwHttpAct";
import { kwMsg }                    from "../../class/msg/kwMsg";

import { kwApisApi }                from "./kwApisApi";
import { kwApisSrvc }               from "../../class/apis/kwApisSrvc";
// @formatter:off


@Injectable()
export class kwApisMsg
{

	private broadcast: EventEmitter<kwMsg>;

	data: kwMsg;

	changed$: EventEmitter<kwMsg>;

	constructor(private srvc: kwApisApi)
	{
		//console.log("srvcHttpMetrixkwMsgType::constructor() called.");

		this.broadcast = new EventEmitter<kwMsg>();
		this.changed$=this.broadcast;
	}

	actionAdd(data, params)
	{
		//console.log("kwApisMsg::actionAdd() called.");

		let act: kwActType = this.srvc.retrieve();
		if (!kwActSrvc.isType(act))
		{
			console.error("kwApisAct::actionAdd() act is invalid.");
			return;
		}

		let msg: kwMsg	 = kwHttpAct.add(data, params, act)
		if (!kwMsg.is(msg))
		{
			console.error("kwApisAct::actionAdd() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionDelete(params)
	{
		//console.log("kwApisMsg::actionDelete() called.");

		let apis: kwApisType = kwApisHttpApi.retrieve();
		if (!kwApisSrvc.isType(apis))
		{
			console.error("kwApisAct::actionDelete() apis is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.delete(data, params, apis)
		if (!kwMsg.is(msg))
		{
			console.error("kwApisMsg::actionDelete() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionEdit(data, params)
	{
		//console.log("kwApisMsg::actionEdit() called.");

		let apis: kwApisType = kwApisHttpApi.get();
		if (!kwApisSrvc.isType(apis))
		{
			console.error("kwApisMsg::actionEdit() apis is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.edit(data, params, apis)
		if (!kwMsg.is(msg))
		{
			console.error("kwApisMsg::actionEdit() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionGet(params)
	{
		//console.log("kwApisMsg::actionGet() called.");

		let apis: kwApisType = kwApisHttpApi.get();
		if (!kwApisSrvc.isType(apis))
		{
			console.error("kwApisMsg::actionGet() apis is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.get(data, params, apis)
		if (!kwMsg.is(msg))
		{
			console.error("kwApisMsg::actionGet() msg is invalid.");
			return;
		}

		this.change(msg);

	};

	change(data: kwMsg)
	{
		//console.log("srvcHttpMetrixAction::change() consolealled.");

		if (!kwMsg.is(data))
		{
			console.error("srvcHttpMetrixAction::change() data is invalid.");
			return
		}
		//console.info("srvcHttpMetrixAction::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("srvcHttpMetrixAction::clear() called.");
		this.data = null;
	};

	retrieve(): kwMsg
	{
		//console.log("srvcHttpMetrixAction::retrieve() called.");

		return this.data;
	};

}
