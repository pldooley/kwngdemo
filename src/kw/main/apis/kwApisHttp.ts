/**********************************************************************
 *
 * kw/http/apis/kwApisHttp.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Component }					from '@angular/core';
import { OnInit }						from '@angular/core';
import { OnDestroy }					from '@angular/core';
import { Subscription }					from 'rxjs/Subscription';

import { kw }					        from '../../kw';
import { kwHttpMsg }					from '../../http/kwHttpMsg';

import { kwApisMsg }                    from "./kwApisMsg";
import { kwApisData }                   from "./kwApisData";

// @formatter:on


const sSTATE: string = "apis";

@Component({
	selector: 'kw-apis-http',
	template: ``,
})
export class kwApisHttp implements OnInit, OnDestroy
{

	subMsg: Subscription;

	constructor(    private srvcMsg: kwApisMsg,
	                private srvcData: kwApisData )
	{
		//console.log("kwApisHttp::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwApisHttp::ngOnInit() called");

		this.subMsg = this.srvcMsg.changed$.subscribe(msg =>
		{
			this.inspect(msg);
		});
	};

	ngOnDestroy()
	{
		//console.log("kwApisHttp::ngOnDestroy() called.");

		this.subMsg.unsubscribe();
	}

	execute(msg: any): void
	{
		//console.log("kwApisHttp::execute() called");

		let promise = kwHttpMsg.multiple(msg);

		if (!kw.isNull(promise))
		{
			console.error("kwApisHttp::execute() promise is invalid.");
			return;
		}

		promise.then(function (data)
		{
			//console.info("kwApisHttp::execute::then() called.");

			if (!kw.isNull(data))
			{
				if (!msg.isDelete())
				{
					console.error("kwApisHttp::execute::then() data is invalid");
					return;
				}
			}

			//console.info("kwApisHttp::execute::then() data is ", data);

			this.srvcData.change(data);

		}), function(reason)
		{
			console.error("kwApisHttp::execute::error() called");
			alert('Failed: ' + reason);
			this.srvcData.change(null);
		}
	};

	inspect(msg)
	{
		//console.log("kwApisHttp::inspect() called");

		if (!kwMsg.is(msg))
		{
			console.error("kwApisHttp::inspect() msg is invalid.");
			return;
		}
		console.info("kwApisHttp::inspect() msg is valid - executing.");

		this.execute(msg);
	}

}
