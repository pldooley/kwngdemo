/**********************************************************************
 *
 * kw/util/apis/kwApisState.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
import { Component }						from '@angular/core';
import { OnInit }							from '@angular/core';
import { OnDestroy }						from '@angular/core';
import { Subscription }						from 'rxjs/Subscription';

import { kw }		                        from "../../kw";
import { kwActType }						from "../../class/act/kwActType";
import { kwActEnum }						from "../../class/act/kwActEnum";
import { kwIdKeyData }						from "../../key/id/kwIdKeyData";
import { kwHostKeyData }					from "../../key/host/kwHostKeyData";
import { kwPortKeyData }					from "../../key/port/kwPortKeyData";

import { kwApisMsg }                        from "./kwApisMsg";
import { kwApisType }                       from "../../class/apis/kwApisType";
// @formatter:on


@Component({
	selector: 'kw-apis-state',
	template: ``,
})
export class kwApisState implements OnInit, OnDestroy
{
	subHost:    Subscription;
	subId:      Subscription;
	subPort:    Subscription;

	constructor(    private srvcApi:    kwIdKeyData,
	                private srvcId:     kwIdKeyData,
	                private srvcHost:   kwHostKeyData,
					private srvcPort:   kwPortKeyData,
					private srvcMsg:    kwApisMsg      )
	{
		//console.log("kwApisState::constructor()called");
	}

	ngOnInit()
	{
		//console.log("kwApisState::ngOnInit()called");

		this.retrieve();

		this.subHost = this.srvcHost.changed$.subscribe(sHost =>
		{
			this.store();
		});

		this.subId = this.srvcId.changed$.subscribe(nId =>
		{
			this.store();
		});

		this.subPort = this.srvcPort.changed$.subscribe(nPort =>
		{
			this.store();
		});

		this.store();
		this.load();
	};


	ngOnDestroy()
	{
		//console.log("kwApisState::ngOnDestroy()called.");

		this.subHost.unsubscribe();
		this.subId.unsubscribe();
		this.subPort.unsubscribe();
	}

	load()
	{
		//console.log("kwApisState::load() called.");

		let act: kwActType = {
			nMethod: kwActEnum.Get,
			data: null
		}

		this.srvcAct.change(act);
	}

	retrieve()
	{
		//console.log("kwApisState::retrieve() called.");

		this.srvcMsg.actionGet([])
	}

	store()
	{
		//console.log("kwApisState::store() called.");

		let sHost: string = this.srvcHost.retrieve();
		if( !kw.isString(sHost))
		{
			//console.info("kwApisState::store() sHost not available.");
			return;
		}
		//console.info("kwApisState::store() sHost is [", sHost, "].");

		let nId: number = this.srvcId.retrieve();
		if( !kw.isNumber(nId))
		{
			//console.info("kwApisState::store() nId not available.");
			return;
		}
		//console.info("kwApisState::store() nId is [", nId, "].");

		let nPort: number = this.srvcPort.retrieve();
		if( !kw.isNumber(nPort))
		{
			//console.info("kwApisState::store() nPort not available.");
			return;
		}
		//console.info("kwApisState::store() nPort is [", nPort, "].");

		let data: kwApisType =	{
			sHost: sHost,
			nId: nId,
			nPort: nPort,
		}
		//console.info("kwApisState::store() nPort is [", nPort, "].");

		this.srvcApi.change(data);
	}


}
