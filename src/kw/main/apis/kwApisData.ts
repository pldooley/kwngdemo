/**********************************************************************
 *
 * kw/state/apis/kwApisData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }             from '@angular/core';
import { Injectable }	            from '@angular/core';

import { kw }				        from '../../kw';
import { kwApisSrvc }               from "../../class/apis/kwApisSrvc"; import { kwApi } from "../../class/api/kwApi";
// @formatter:off



@Injectable()
export class kwApisData
{

	// Observable email sources
	private broadcast: EventEmitter<object>;

	data: object;

	// Observable string streams
	changed$ = this.broadcast;

	constructor()
	{
		//console.log("kwApisData::constructor() called.");

		this.broadcast = new EventEmitter<object>();
		this.changed$=this.broadcast;
	}

	change(newData: object)
	{
		//console.log("kwApisData::change() called.");

		if (kw.isNull(newData))
		{
			console.error("kwApisData::change() data is invalid.");
			return
		}
		console.info("kwApisData::change() data is [", newData, "]");

		this.data = newData;

		this.broadcast.emit(this.data);
	}

	clear()
	{
		//console.log("kwApisData::clear() called.");
		this.data = null
	};

	retrieve() : object
	{
		//console.log("kwApisData::retrieve() called.");
		return this.data;
	};

	retrieveItem(sItem: string): kwApi
	{
		//console.log("kwApisData::retrieve() called.");
		return kwApisSrvc.getItem(sItem, this.data);
	};

}
