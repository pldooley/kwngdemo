/**********************************************************************
 *
 * kw/http/apis/kwApisApi.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }                 from '@angular/core';
import { Injectable }                   from '@angular/core';

import { kwApi }                        from "../../class/api/kwApi";
import { kwApiSrvc }                    from "../../class/api/kwApiSrvc";

// @formatter:on



@Injectable()
export class kwApisApi
{

	private broadcast: EventEmitter<kwApi>;

	data: kwApi = URL;

	changed$: EventEmitter<kwApi>;

	constructor()
	{
		//console.log("kwApisApi::constructor() called.");

		this.broadcast = new EventEmitter<kwApi>();
		this.changed$ = this.broadcast;
	}

	change(data: kwApi): void
	{
		//console.log("kwApisApi::change() called.");

		if( !kwApiSrvc.isType(data) )
		{
			console.error("kwApisApi::change() data is invalid.");
			return
		}
		//console.info("kwApisApi::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("kwApisApi::clear() called.");
		this.data = null;
	};

	retrieve(): kwApi
	{
		//console.log("kwApisApi::retrieve() called.");
		return this.data;
	};


}
