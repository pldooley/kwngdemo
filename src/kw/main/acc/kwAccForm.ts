/**********************************************************************
 *
 * kw/main/acc/kwAccForm.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
import { Injectable }	            from '@angular/core';

import { kw }			            from '../../kw';
import { kwFormEnum }               from "../../class/form/kwFormEnum";
import { kwFormSrvc }               from "../../class/form/kwFormSrvc";

import { kwAccMdl }			        from './kwAccMdl';
// @formatter:off



@Injectable()
export class kwAccForm
{

	constructor(    private srvcMdl: kwAccMdl  )
	{
		//console.log("kwAccForm::constructor() called.");
	}

	create(nForm: kwFormEnum, obj: any)
	{
		//console.log("kwAccForm::create() called");
		return kwFormSrvc.create(nForm, obj, this, this.srvcMdl);
	}

	loadMeta(obj)
	{
		//console.log("kwAccForm::loadMeta() called.");

		if (kw.isNull(obj))
		{
			console.error("kwAccForm::loadMeta() obj is invalid");
			return;
		}

		let record = angular.copy(obj);

		let currencies = kwCurrData.get();
		if (kw.isNull(currencies))
		{
			console.error("kwAccForm::load() currencies is invalid");
			return;
		}
		//console.info("kwAccForm::formLoad() currencies is", currencies);
		record.currencies = currencies;

		let languages = kwLangData.get();
		if (kw.isNull(languages))
		{
			console.error("kwAccForm::load() languages is invalid");
			return;
		}
		//console.info("kwAccForm::formLoad() languages is", languages);
		record.languages = languages;

		let timeZones = kwTZData.get();
		if (kw.isNull(timeZones))
		{
			console.error("kwAccForm::load() timeZones is invalid");
			return;
		}
		//console.info("kwAccForm::load() timeZones is", timeZones);
		record.timeZones = timeZones;

		return record;
	};

	save(nForm: kwFormEnum, obj): boolean
	{
		//console.log("kwAccForm::save() called");
		return kwFormSrvc.save(nform, obj, srvcMdl, srvcMsg);
	}

}




