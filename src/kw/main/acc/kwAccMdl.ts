/**********************************************************************
 *
 * kw/main/acc/kwAccMdl.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }             from '@angular/core';
import { Injectable }	            from '@angular/core';

import { kw }			            from '../../kw';
// @formatter:off



@Injectable()
export class kwAccMdl
{

	// Observable email sources
	private broadcast: EventEmitter<any>;

	data: any;

	// Observable string streams
	changed$ = this.broadcast;


	constructor()
	{
		//console.log("hcnEmailsStateData::constructor() called.");

		this.broadcast = new EventEmitter<any[]>();
		this.changed$=this.broadcast;
	}

	change(dataNew)
	{
		//console.log("kwAccMdlModel::changed() called.");

		if (kw.isNull(dataNew))
		{
			console.error("kwAccMdlModel::changed() dataNew is invalid.");
			return;
		}

		this.data = dataNew;
		//console.info("kwAccMdlModel::changed() data is ", data);

		this.broadcast.emit(this.data);
	};

	clear()
	{
		//console.log("kwAccMdlModel::clear() called.");

		data = null;
	};

	create()
	{
		//console.log("kwAccMdlModel::create() called.");

		if (kw.isNull(this.data))
		{
			console.error("kwAccMdlModel::create() data is invalid.");
			return false;
		}

		let rec = this.data.createRecord();
		if (kw.isNull(rec))
		{
			console.error("kwAccMdlModel::xImport() rec is invalid.");
			return;
		}

		return rec;
	};

	retrieve()
	{
		//console.log("kwAccMdlModel::get() called.");

		return data;
	};

	xExport(recs)
	{
		//console.log("kwAccMdlModel::xExport() called.");

		if (kw.isNull(this.data))
		{
			console.error("kwAccMdlModel::xExport() data is invalid.");
			return;
		}

		if (!kw.isArray(recs))
		{
			console.error("kwAccMdlModel::xExport() recs is invalid.");
			return;
		}

		let recsX = this.data.xExport(recs);
		if (!kw.isArray(recsX))
		{
			console.error("kwAccMdlModel::xExport() recsX is invalid.");
			return;
		}

		return recsX;
	};

	xExportRec(record)
	{
		//console.log("kwAccMdlModel::xExportRec() called.");

		if (kw.isNull(this.data))
		{
			console.error("kwAccMdlModel::xExportRec() data is invalid.");
			return;
		}

		if (kw.isNull(record))
		{
			console.error("kwAccMdlModel::xExportRec() record is invalid.");
			return;
		}

		let recX = this.data.xExportRec(record);
		if (kw.isNull(recX))
		{
			console.error("kwAccMdlModel::xExportRec() recX is invalid.");
			return;
		}

		return recX;
	};

	xImport(recs)
	{
		//console.log("kwAccMdlModel::xImport() called.");

		if (kw.isNull(this.data))
		{
			console.error("kwAccMdlModel::xImport() data is invalid.");
			return false;
		}

		if (!kw.isArray(recs))
		{
			console.error("kwAccMdlModel::xImport() recs is invalid.");
			return false;
		}

		let recsX = this.data.xImport(recs);
		if (!kw.isArray(recsX))
		{
			console.error("kwAccMdlModel::xImport() recsX is invalid.");
			return;
		}

		return recsX;
	};

	xImportRec(rec)
	{
		//console.log("kwAccMdlModel::xImportRec() called.");

		if (kw.isNull(this.data))
		{
			console.error("kwAccMdlModel::xImportRec() data is invalid.");
			return false;
		}

		if (kw.isNull(rec))
		{
			console.error("kwAccMdlModel::xImportRec() rec is invalid.");
			return false;
		}

		let recX = this.data.xImportRec(rec);
		if (kw.isNull(recX))
		{
			console.error("kwAccMdlModel::xImportRec() recX is invalid.");
			return;
		}

		return recX;
	};

}