/**********************************************************************
 *
 * kw/main/acc/kwAccData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }       from '@angular/core';
import { Injectable }         from '@angular/core';

// @formatter:on



@Injectable()
export class kwAccData
{

	private broadcast: EventEmitter<any>;

	data: any;

	changed$: EventEmitter<any>;

	constructor()
	{
		//console.log("kwAccData::constructor() called.");

		this.broadcast = new EventEmitter<any>();
		this.changed$ = this.broadcast;

	}

	change(data: any): void
	{
		//console.log("kwAccData::change() called.");

		if(! data)
		{
			console.error("kwAccData::change() data is invalid.");
			return
		}
		//console.info("kwAccData::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("kwAccData::clear() called.");

		this.data = null;
	};

	retrieve(): any
	{
		//console.log("kwAccData::retrieve() called.");

		return this.data;
	};

}
