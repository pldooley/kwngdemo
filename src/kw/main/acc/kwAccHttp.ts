/**********************************************************************
 *
 * kw/main/acc/kwAccHttp.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Component }					from '@angular/core';
import { OnInit }						from '@angular/core';
import { OnDestroy }					from '@angular/core';
import { Subscription }					from 'rxjs/Subscription';

import { kw }					        from '../../kw';
import { kwHttpMsg }					from '../../http/kwHttpMsg';

import { kwAccMsg }	                    from './kwAccMsg';
import { kwAccData }		            from './kwAccData';
// @formatter:on


const sSTATE: string = "account";

@Component({
	selector: 'kw-acc-http',
	template: ``,
})
export class kwAccHttp implements OnInit, OnDestroy
{

	subMsg: Subscription;

	constructor(    private srvcMsg: kwAccMsg,
	                private srvcData: kwAccData )
	{
		//console.log("kwAccHttp::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwAccHttp::ngOnInit() called");

		this.subMsg = this.srvcMsg.changed$.subscribe(msg =>
		{
			this.inspect(msg);
		});
	};

	ngOnDestroy()
	{
		//console.log("kwAccHttp::ngOnDestroy() called.");

		this.subMsg.unsubscribe();
	}

	execute(msg: any): void
	{
		//console.log("kwAccHttp::execute() called");

		let promise = kwHttpMsg.single(msg);

		if (!kw.isNull(promise))
		{
			console.error("kwAccHttp::execute() promise is invalid.");
			return;
		}

		promise.then(function (data)
		{
			//console.info("kwAccHttp::execute::then() called.");

			if (!kw.isNull(data))
			{
				if (!msg.isDelete())
				{
					console.error("kwAccHttp::execute::then() data is invalid");
					return;
				}
			}

			//console.info("kwAccHttp::execute::then() data is ", data);

			this.srvcData.change(data);

		}), function(reason)
		{
			console.error("kwAccHttp::execute::error() called");
			alert('Failed: ' + reason);
			this.srvcData.change(null);
		}
	};

	inspect(msg)
	{
		//console.log("kwAccHttp::inspectAction() called");

		if (!kwMsg.is(msg))
		{
			console.error("kwAccHttp::inspectAction() msg is invalid.");
			return;
		}
		console.info("kwAccHttp::inspectAction() msg is valid - executing.");

		this.execute(msg);
	}

}
