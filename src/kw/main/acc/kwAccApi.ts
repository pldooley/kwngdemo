/**********************************************************************
 *
 * kw/util/acc/kwAccApi.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/
// @formatter:off
import {EventEmitter, Injectable}   from '@angular/core';

import { kwApi }                    from "../../class/api/kwApi";
import { kwApiSrvc }                from "../../class/api/kwApiSrvc";

// @formatter:on


@Injectable()
export class kwAccApi
{

	private broadcast: EventEmitter<kwApi>;

	data: kwApi;

	changed$: EventEmitter<kwApi>;

	constructor()
	{
		//console.log("kwAccApi::constructor() called.");
		this.broadcast = new EventEmitter<kwApi>();
		this.changed$ = this.broadcast;
	}

	change(data: kwApi): void
	{
		//console.log("kwAccApi::change() called.");

		if(!kwApiSrvc.isType(data))
		{
			console.error("kwAccApi::change() data is invalid.");
			return
		}
		//console.info("kwAccApi::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("kwAccApi::clear() called.");
		this.data = null;
	};

	retrieve(): kwApi
	{
		//console.log("kwAccApi::retrieve() called.");
		return this.data;
	};

}
