/**********************************************************************
 *
 * kw/main/acc/kwAccState.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/
// @formatter:off

import { Component }						from '@angular/core';
import { OnInit }							from '@angular/core';
import { OnDestroy }						from '@angular/core';
import { Subscription }						from 'rxjs/Subscription';

import { kwModelsData }				        from "../models/kwModelsData";

import { kwAccData}				            from "../accCreate/kwAccData";
import { kwAccData}					        from "./kwAccData";
// @formatter:off


@Component({
	selector: 'kw-acc-state',
	template: ``,
})
export class kwAccState implements OnInit, OnDestroy
{
	subCreate: Subscription;
	subMdl: Subscription;

	constructor(	private srvcData: kwAccData,
					private srvcCreate: kwAccData )
	{
		//console.log("kwAccState::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwAccState::ngOnInit() called");

		this.subCreate = this.srvcCreate.changed$.subscribe(info =>
		{
			this.inspectCreate(info);
		});

		this.subMdl = this.srvcCreate.changed$.subscribe(info =>
		{
			this.inspectCreate(info);
		});

		this.retrieveEmails();
		this.retrieveGuest();
	};

	ngOnDestroy()
	{
		//console.log("kwAccState::ngOnDestroy() called.");

		this.sub.unsubscribe();
	}

	inspectCreate(data)
	{
		//console.log("kwAccState::inspectCreate() called");

		if (!isObject(data))
		{
			//console.info("kwAccState::inspectCreate() data is invalid - doing nothing.");
			return;
		}
		//console.info("kwAccState::inspectCreate() data is ", data);

		kwAccData.changed(data);
	};

	retrieveModel = function ()
	{
		//console.log("kwAccState::retrieveModel() called");

		let model = srvcStateModels.getModel(sSTATE);
		if (!isObject(model))
		{
			console.error("kwAccState::retrieveModel() model is invalid.");
			return;
		}
		//console.info("kwAccState::retrieveModel() model is ", model);

		kwAccDataModel.changed(model);
	};

}
