/**********************************************************************
 *
 * /kw/main/actTags/kwActTagsMsg.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:on
import { EventEmitter }             from '@angular/core';
import { Injectable }	            from '@angular/core';

import { kwApi }                    from "../../class/api/kwApi";
import { kwApiSrvc }                from "../../class/api/kwApiSrvc";
import { kwApisType }               from "../../class/apis/kwApisType";
import { kwHttpAct }                from "../../http/kwHttpAct";
import { kwMsg }                    from "../../class/msg/kwMsg";
import { kwMsgSrvc }                from "../../class/msg/kwMsgSrvc";

import { kwActTagsApi }             from "./kwActTagsApi";
import { kwApisSrvc } from "../../class/apis/kwApisSrvc";
// @formatter:off


@Injectable()
export class kwActTagsMsg
{

	private broadcast: EventEmitter<kwMsg>;

	data: kwMsg;

	changed$: EventEmitter<kwMsg>;

	constructor(private srvcApi: kwActTagsApi)
	{
		//console.log("kwActTagsMsg::constructor() called.");

		this.broadcast = new EventEmitter<kwMsg>();
		this.changed$=this.broadcast;
	}

	actionAdd(data, params)
	{
		//console.log("kwActTagsMsg::actionAdd() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApis.is(api))
		{
			console.error("kwActTagsMsg::actionAdd() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.add(data, params, api)
		if (!kwMsg.is(msg))
		{
			console.error("kwActTagsMsg::actionAdd() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionDelete(params)
	{
		//console.log("kwActTagsMsg::actionDelete() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApis.is(api))
		{
			console.error("kwActTagsMsg::actionDelete() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.delete(data, params, api)
		if (!kwMsg.is(msg))
		{
			console.error("kwActTagsMsg::actionDelete() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionEdit(data, params)
	{
		//console.log("kwActTagsMsg::actionEdit() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApis.is(api))
		{
			console.error("kwActTagsMsg::actionEdit() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.edit(data, params, api)
		if (!kwMsg.is(msg))
		{
			console.error("kwActTagsMsg::actionEdit() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionGet(params)
	{
		//console.log("kwActTagsMsg::actionGet() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApis.is(api))
		{
			console.error("kwActTagsMsg::actionGet() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.get(data, params, api)
		if (!kwMsg.is(msg))
		{
			console.error("kwActTagsMsg::actionGet() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	change(data: kwMsg)
	{
		//console.log("srvcHttpMetrixAction::change() consolealled.");

		if (!kwMsg.is(data))
		{
			console.error("kwActTagsMsg::change() data is invalid.");
			return
		}
		//console.info("kwActTagsMsg::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("kwActTagsMsg::clear() called.");
		this.data = null;
	};

	retrieve(): kwMsg
	{
		//console.log("kwActTagsMsg::retrieve() called.");
		return this.data;
	};

}
