/**********************************************************************
 *
 * kw/main/actTags/kwActTagsState.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Component, OnInit, OnDestroy}       from '@angular/core';
import { Subscription}                       from 'rxjs/Subscription';

import { kwApi }                             from "../../class/api/kwApi";
import { kwApiSrvc}                          from '../../class/api/kwApiSrvc';
import { kwApisData}                         from '../apis/kwApisData';

import { kwActTagsApi }                      from "./kwActTagsApi";
// @formatter:on


const sSTATE: string = "activityTags";


@Component({
	selector: 'kw-act-tags-state',
	template: ``,
})
export class kwActTagsState implements OnInit,
	OnDestroy
{
	subApis: Subscription;

	constructor(    private srvcApi: kwActTagsApi,
	                private srvcApis: kwApisData)
	{
		//console.log("kwActTagsState::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwActTagsState::ngOnInit() called");

		this.subApis = this.srvcApis.changed$.subscribe(apis =>
		{
			this.load();
		});

		this.load();
	};

	ngOnDestroy()
	{
		//console.log("kwActTagsState::ngOnDestroy() called.");

		this.subApis.unsubscribe();
	}

	load(): void
	{
		//console.log("kwActTagsState::load() called.");

		let api: kwApi = this.srvcApis.retrieveItem(sSTATE);
		if (!kwApi.is(api))
		{
			console.error("kwActTagsState::retrieveApi() api is invalid.");
			return;
		}
		console.info("kwActTagsState::loadApi() api is ", api);

		this.srvcApi.change(api);
	}

}
