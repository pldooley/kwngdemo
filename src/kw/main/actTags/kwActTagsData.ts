/**********************************************************************
 *
 * kw/main/actTags/kwActTagsData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }         from '@angular/core';
import { Injectable }           from '@angular/core';

import { kw }                   from "../../kw";
// @formatter:on



@Injectable()
export class kwActTagsData
{

	private broadcast: EventEmitter<object[]>;

	data: object[];

	changed$: EventEmitter<object[]>;

	constructor()
	{
		//console.log("kwActTagsData::constructor() called.");

		this.broadcast = new EventEmitter<object[]>();
		this.changed$ = this.broadcast;

	}

	change(data: object[]): void
	{
		//console.log("kwActTagsData::change() called.");

		if(kw.isNull(data))
		{
			console.error("kwActTagsData::change() data is invalid.");
			return
		}
		//console.info("kwActTagsData::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("kwActTagsData::clear() called.");
		this.data = null;
	};

	retrieve(): object[]
	{
		//console.log("kwActTagsData::retrieve() called.");
		return this.data;
	};

}
