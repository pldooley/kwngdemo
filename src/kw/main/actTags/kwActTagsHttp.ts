/**********************************************************************
 *
 * /kw/main/actTag/kwActTagsHttp.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Component }				from '@angular/core';
import { OnInit }					from '@angular/core';
import { OnDestroy }				from '@angular/core';
import { Subscription }             from 'rxjs/Subscription';

import { kw }		                from "../../kw";
import { kwHttpMsg }		        from "../kwHttpMsg";
import { kwMsgSrvc }                from "../../class/msg/kwMsgSrvc";

import { kwActTagsData }            from "./kwActTagsData";

// @formatter:on


const sSTATE: string = "activityTag";


@Component({
	selector: 'kw-act-tags-http',
	template: ``,
})
export class kwActTagsHttp implements OnInit, OnDestroy
{

	subMsg: Subscription;

	constructor(private srvcMsg: kwActTagsMsg,
	            private srvcData: kwActTagsData   )
	{
		//console.log("kwActTagsHttp::constructor() called");
	}

	ngOnInit(): void
	{
		//console.log("kwActTagsHttp::ngOnInit() called");
		this.subMsg = this.srvcMsg.changed$.subscribe(msg =>
		{
			this.inspect(msg);
		});
	};

	ngOnDestroy()
	{
		//console.log("kwActTagHttp::ngOnDestroy() called.");
		this.subMsg.unsubscribe();
	}

	execute(msg: kwMsg): void
	{
		//console.log("kwActTagsHttp::execute() called");

		let promise = kwHttpMsg.multiple(msg);

		if (!kw.isNull(promise))
		{
			console.error("kwActTagsHttp::execute() promise is invalid.");
			return;
		}

		promise.then(function (data)
		{
			//console.info("kwActTagsHttp::execute::then() called.");

			if (!kw.isNull(data))
			{
				if (!msg.isDelete())
				{
					console.error("kwActTagsHttp::execute::then() data is invalid");
					return;
				}
			}

			//console.info("kwActTagsHttp::execute::then() data is ", data);

			this.srvcData.change(data);

		}), function(reason)
		{
			console.error("kwActTagsHttp::execute::error() called");
			alert('Failed: ' + reason);
			this.srvcData.change(null);
		}
	};

	inspect(msg)
	{
		//console.log("kwActTagState::inspectAction() called");

		if (!kwMsg.is(msg))
		{
			console.error("kwActTagState::inspectAction() msg is invalid.");
			return;
		}
		//console.info("kwActTagState::inspectAction() msg is valid - esxecuting.");

		this.execute(msg);
	}

}
