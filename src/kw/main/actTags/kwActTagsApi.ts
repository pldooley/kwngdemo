/**********************************************************************
 *
 * /kw/main/actTags/kwActTagsApi.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }         from '@angular/core';
import { Injectable }           from '@angular/core';

import { kwApi }                from "../../class/api/kwApi";
import { kwApiSrvc }            from "../../class/api/kwApiSrvc";
// @formatter:on



@Injectable()
export class kwActTagsApi
{

	private broadcast: EventEmitter<kwApi>;

	data: kwApi;

	changed$: EventEmitter<kwApi>;

	constructor()
	{
		//console.log("kwActTagsApi::constructor() called.");
		this.broadcast = new EventEmitter<kwApi>();
		this.changed$ = this.broadcast;

	}

	change(data: kwApi): void
	{
		//console.log("kwActTagsApi::change() called.");

		if(!kwApi.is(data))
		{
			console.error("kwActTagsApi::change() data is invalid.");
			return
		}
		//console.info("kwActTagsApi::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("kwActTagsApi::clear() called.");
		this.data = null;
	};

	retrieve(): kwApi
	{
		//console.log("kwActTagsApi::retrieve() called.");
		return this.data;
	};


}
