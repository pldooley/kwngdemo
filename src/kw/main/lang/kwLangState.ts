/**********************************************************************
 *
 * kw/main/lang/kwLangState.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Component }			        from '@angular/core';
import { OnInit }				        from '@angular/core';
import { OnDestroy }			        from '@angular/core';
import { Subscription }	                from 'rxjs/Subscription';

import { kwApi }                        from "../../class/api/kwApi";
import { kwApiSrvc }					from '../../class/api/kwApiSrvc';
import { kwApisData }                   from '../apis/kwApisData';

import { kwLangApi }	                from './kwLangApi';
// @formatter:off


const sSTATE: string = "language";


@Component({
	selector: 'hcn-lang-state',
	template: ``,
})
export class kwLangState implements OnInit, OnDestroy
{
	subApis: Subscription;

	constructor(    private srvcApi: kwLangApi,
	                private srvcApis: kwApisData    )
	{
		//console.log("kwLangState::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwLangState::ngOnInit() called");

		this.subApis = this.srvcApis.changed$.subscribe(apis =>
		{
			this.loadApi()
		});
	};

	ngOnDestroy()
	{
		//console.log("kwLangState::ngOnDestroy() called.");

		this.subApis.unsubscribe();
	}

	loadApi()
	{
		//console.log("kwLangState::loadApi() called");

		let api: kwApi = this.srvcApis.retrieveItem(sSTATE);
		if (!kwApi.is(api))
		{
			console.error("kwLangState::retrieveApi() api is invalid.");
			return;
		}
		//console.info("kwLangState::loadApi() api is ", api);

		this.srvcApi.change(api);
	};

}
