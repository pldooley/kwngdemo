/**********************************************************************
 *
 * kw/main/lang/kwLangData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }			 from '@angular/core';
import { Injectable }			 from '@angular/core';

import { kwLangSrvc }            from "../../class/lang/kwLangSrvc";
import { kwLangType }            from "../../class/lang/kwLangType";

// @formatter:off



@Injectable()
export class kwLangData
{

	private broadcast: EventEmitter<kwLangType>;

	data: kwLangType;

	changed$: EventEmitter<kwLangType>;

	constructor()
	{
		//console.log("kwLangData::constructor() called.");

		this.broadcast = new EventEmitter<kwLangType>();
		this.changed$=this.broadcast;
	}

	change(data: kwLangType)
	{
		//console.log("kwLangData::change() called.");

		if (!kwLangSrvc.isType(data)) {
			console.error("kwLangData::change() data is invalid.");
			return
		}
		//console.info("kwLangData::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("kwLangData::clear() called.");
		this.data = null;
	};

	retrieve() : kwLangType
	{
		//console.log("kwLangData::retrieve() called.");
		return this.data;
	};

}
