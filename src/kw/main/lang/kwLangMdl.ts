/**********************************************************************
 *
 * kw/main/lang/kwLangMdl.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }             from '@angular/core';
import { Injectable }	            from '@angular/core';

import { kw }			            from '../../kw';
import { kwModelSrvc }              from "../../class/model/kwModelSrvc";
// @formatter:off



@Injectable()
export class kwLangMdl
{

	// Observable email sources
	private broadcast: EventEmitter<object>;

	data: object;

	// Observable string streams
	changed$ = this.broadcast;


	constructor()
	{
		//console.log("kwLangMdl::constructor() called.");

		this.broadcast = new EventEmitter<object>();
		this.changed$=this.broadcast;
	}

	change(dataNew)
	{
		//console.log("kwLangMdl::changed() called.");

		if (kw.isNull(dataNew))
		{
			console.error("kwLangMdl::changed() dataNew is invalid.");
			return;
		}

		this.data = dataNew;
		//console.info("kwLangMdl::changed() data is ", data);

		this.broadcast.emit(this.data);
	};

	clear()
	{
		//console.log("kwLangMdl::clear() called.");
		this.data = null;
	};

	create()
	{
		//console.log("kwLangMdl::create() called.");

		if (kw.isNull(this.data))
		{
			console.error("kwLangMdl::create() data is invalid.");
			return false;
		}

		let rec = this.data.createRecord();
		if (kw.isNull(rec))
		{
			console.error("kwLangMdl::xImport() rec is invalid.");
			return;
		}

		return rec;
	};

	retrieve(): object
	{
		return this.data;
	};

	xExport(recs)
	{
		return kwModelSrvc.xExport(this.data, recs);
	};

	xExportRec(rec)
	{
		return kwModelSrvc.xExportRec(this.data, rec);
	};

	xImport(recs)
	{
		return kwModelSrvc.xImport(this.data, recs);
	};

	xImportRec(rec)
	{
		return kwModelSrvc.xImportRec(this.data, rec);
	};

}