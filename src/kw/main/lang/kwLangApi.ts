/**********************************************************************
 *
 * kw/main/lang/kwLangApi.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/
// @formatter:off
import {EventEmitter, Injectable}   from '@angular/core';

import { kwApi }                    from "../../class/api/kwApi";
// @formatter:on


@Injectable()
export class kwLangApi
{

	private broadcast: EventEmitter<kwApi>;

	data: kwApi;

	changed$: EventEmitter<kwApi>;

	constructor()
	{
		//console.log("kwLangApi::constructor() called.");
		this.broadcast = new EventEmitter<kwApi>();
		this.changed$ = this.broadcast;
	}

	change(data: kwApi): void
	{
		//console.log("kwLangApi::change() called.");

		if(!kwApi.is(data))
		{
			console.error("kwLangApi::change() data is invalid.");
			return
		}
		//console.info("kwLangApi::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("kwLangApi::clear() called.");
		this.data = null;
	};

	retrieve(): kwApi
	{
		//console.log("kwLangApi::retrieve() called.");
		return this.data;
	};

}
