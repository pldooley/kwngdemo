/**********************************************************************
 *
 * kw/main/lang/kwLangForm.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Injectable }	            from '@angular/core';

import { kw }			            from '../../kw';
import { kwFormEnum }               from "../../class/form/kwFormEnum";
import { kwFormSrvc }               from "../../class/form/kwFormSrvc";

import { kwLangMsg }                from "./kwLangMsg";
import { kwLangMdl }                from "./kwLangMdl";
// @formatter:off


@Injectable()
export class kwLangForm
{

	constructor(    private srvcMsg: kwLangMsg,
					private srvcMdl: kwLangMdl  )
	{
		//console.log("kwLangForm::constructor() called.");
	}

	create(nForm: kwFormEnum, obj: any)
	{
		//console.log("kwLangForm::create() called");
		return kwFormSrvc.create(nForm, obj, this, this.srvcMdl);
	}

	loadMeta(obj)
	{
		//console.log("kwLangForm::loadMeta() called.");

		if (kw.isNull(obj))
		{
			console.error("kwLangForm::loadMeta() obj is invalid");
			return;
		}

		return obj;

	};

	save(nForm: kwFormEnum, obj): boolean
	{
		//console.log("kwLangForm::save() called");
		return kwFormSrvc.save(nform, obj, this.srvcMdl, this.srvcAct);
	}

}




