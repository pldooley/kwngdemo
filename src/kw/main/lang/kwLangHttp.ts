/**********************************************************************
 *
 * kw/main/lang/kwLangHttp.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Component }					from '@angular/core';
import { OnInit }						from '@angular/core';
import { OnDestroy }					from '@angular/core';
import { Subscription }					from 'rxjs/Subscription';

import { kw }					        from '../../kw';
import { kwHttpMsg }				    from '../../http/kwHttpMsg';

import { kwLangMsg }	                from './kwLangMsg';
import { kwLangData }		            from './kwLangData';
// @formatter:on



@Component({
	selector: 'hcn-lang-http',
	template: ``,
})
export class kwLangHttp implements OnInit, OnDestroy
{

	subMsg: Subscription;

	constructor(    private srvcMsg: kwLangMsg,
	                private srvcData: kwLangData )
	{
		//console.log("kwLangHttp::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwLangHttp::ngOnInit() called");

		this.subAct = this.srvcMsg.changed$.subscribe(info =>
		{
			this.inspect(info);
		});
	};

	ngOnDestroy()
	{
		//console.log("kwLangHttp::ngOnDestroy() called.");

		this.subAct.unsubscribe();
	}

	execute(msg: any): void
	{
		//console.log("kwLangHttp::execute() called");

		let promise = kwHttpMsg.single(msg);

		if (!kw.isNull(promise))
		{
			console.error("kwLangHttp::execute() promise is invalid.");
			return;
		}

		promise.then(function (data)
		{
			//console.info("kwLangHttp::execute::then() called.");

			if (!kw.isNull(data))
			{
				if (!msg.isDelete())
				{
					console.error("kwLangHttp::execute::then() data is invalid");
					return;
				}
			}

			//console.info("kwLangHttp::execute::then() data is ", data);

			this.srvcData.change(data);

		}), function(reason)
		{
			console.error("kwLangHttp::execute::error() called");
			alert('Failed: ' + reason);
			this.srvcData.change(null);
		}
	};

	inspect(msg)
	{
		//console.log("kwLangHttp::inspectAction() called");

		if (!kwMsg.is(msg))
		{
			console.error("kwLangHttp::inspectAction() msg is invalid.");
			return;
		}

		if (	!msg.isAdd()
		&&		!msg.isDelete()
		&&		!msg.isEdit()	)
		{
			console.info("kwLangHttp::inspectAction() msg is invalid - doing nothing.");
			return;
		}

		console.info("kwLangHttp::inspectAction() msg is valid - esxecuting.");

		this.execute(msg);
	}

}
