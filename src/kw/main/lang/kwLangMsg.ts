/**********************************************************************
 *
 * /kw/main/lang/kwLangMsg.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/
// @formatter:on
import { EventEmitter, Injectable }     from '@angular/core';

import { kwApi }                        from "../../class/api/kwApi";
import { kwHttpAct }                    from "../../http/kwHttpAct";
import { kwMsg }                        from "../../class/msg/kwMsg";
import { kwMsgSrvc }                    from "../../class/msg/kwMsgSrvc";

import { kwLangApi }                    from "./kwLangApi";
// @formatter:off


@Injectable()
export class kwLangMsg
{

	private broadcast: EventEmitter<kwMsg>;

	data: kwMsg;

	changed$: EventEmitter<kwMsg>;

	constructor(private srvcApi: kwLangApi)
	{
		//console.log("kwLangMsg::constructor() called.");

		this.broadcast = new EventEmitter<kwMsg>();
		this.changed$=this.broadcast;
	}

	actionAdd(data, params)
	{
		//console.log("kwLangMsg::actionAdd() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwLangMsg::actionAdd() api is invalid.");
			return;
		}

		let msg: kwMsg	 = kwHttpAct.add(data, params, api)
		if (!kwMsg.is(msg))
		{
			console.error("kwLangMsg::actionAdd() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionDelete(params)
	{
		//console.log("kwLangMsg::actionDelete() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwLangMsg::actionDelete() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.delete(params, api);
		if (!kwMsg.is(msg))
		{
			console.error("kwLangMsg::actionDelete() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionEdit(data, params)
	{
		//console.log("kwLangMsg::actionEdit() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwLangMsg::actionEdit() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.edit(data, params, api);
		if (!kwMsg.is(msg))
		{
			console.error("kwLangMsg::actionEdit() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionGet(params)
	{
		//console.log("kwLangMsg::actionGet() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwLangMsg::actionGet() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.get(params, api);
		if (!kwMsg.is(msg))
		{
			console.error("kwLangMsg::actionGet() msg is invalid.");
			return;
		}

		this.change(msg);

	};

	change(data: kwMsg)
	{
		//console.log("kwLangMsg::change() consolealled.");

		if (!kwMsg.is(data))
		{
			console.error("kwLangMsg::change() data is invalid.");
			return
		}
		//console.info("kwLangMsg::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("kwLangMsg::clear() called.");
		this.data = null;
	};

	retrieve(): kwMsg
	{
		//console.log("kwLangMsg::retrieve() called.");
		return this.data;
	};

}
