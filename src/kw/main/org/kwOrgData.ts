/**********************************************************************
 *
 * kw/main/org/kwOrgData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/
// @formatter:off
import {EventEmitter, Injectable}       from '@angular/core';

import { kw }                           from "../../kw";
// @formatter:on


@Injectable()
export class kwOrgData
{

	private broadcast: EventEmitter<object>;

	data: object;

	changed$: EventEmitter<object>;

	constructor()
	{
		//console.log("kwOrgData::constructor() called.");

		this.broadcast = new EventEmitter<object>();
		this.changed$ = this.broadcast;
	}

	// Service org commands
	change(data: object)
	{
		//console.log("kwOrgData::change() called.");

		if( kw.isNull(data) )
		{
			console.error("kwOrgData::change() data is invalid.");
			return
		}
		console.info("kwOrgData::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("kwOrgData::clear() called.");
		this.data = null;
	};

	retrieve(): object
	{
		//console.log("kwOrgData::retrieve() called.");
		return this.data;
	};

}
