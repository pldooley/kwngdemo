/**********************************************************************
 *
 * /kw/main/org/kwOrgMsg.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/
// @formatter:on
import { EventEmitter, Injectable }     from '@angular/core';

import { kwApi }                        from "../../class/api/kwApi";
import { kwApiSrvc }                    from "../../class/api/kwApiSrvc";
import { kwHttpAct }                    from "../../http/kwHttpAct";
import { kwMsg }                        from "../../class/msg/kwMsg";
import { kwMsgSrvc }                    from "../../class/msg/kwMsgSrvc";

import { kwOrgApi }                     from "./kwOrgApi";

// @formatter:off


@Injectable()
export class kwOrgMsg
{

	private broadcast: EventEmitter<kwMsg>;

	data: kwMsg;

	changed$: EventEmitter<kwMsg>;

	constructor(private srvcApi: kwOrgApi)
	{
		//console.log("kwOrgMsg::constructor() called.");

		this.broadcast = new EventEmitter<kwMsg>();
		this.changed$=this.broadcast;
	}

	actionAdd(data, params)
	{
		//console.log("kwOrgMsg::actionAdd() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwOrgMsg::actionAdd() api is invalid.");
			return;
		}

		let msg: kwMsg	 = kwHttpAct.add(data, params, api)
		if (!kwMsg.is(msg))
		{
			console.error("kwOrgMsg::actionAdd() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionDelete(params)
	{
		//console.log("kwOrgMsg::actionDelete() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwOrgMsg::actionDelete() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.delete(params, api);
		if (!kwMsg.is(msg))
		{
			console.error("kwOrgMsg::actionDelete() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionEdit(data, params)
	{
		//console.log("kwOrgMsg::actionEdit() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwOrgMsg::actionEdit() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.edit(data, params, api)
		if (!kwMsg.is(msg))
		{
			console.error("kwOrgMsg::actionEdit() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionGet(params)
	{
		//console.log("kwOrgMsg::actionGet() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwOrgMsg::actionGet() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.get(params, api);
		if (!kwMsg.is(msg))
		{
			console.error("kwOrgMsg::actionGet() msg is invalid.");
			return;
		}

		this.change(msg);

	};

	change(data: kwMsg)
	{
		//console.log("kwOrgMsg::change() consolealled.");

		if (!kwMsg.is(data))
		{
			console.error("kwOrgMsg::change() data is invalid.");
			return
		}
		//console.info("kwOrgMsg::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("kwOrgMsg::clear() called.");
		this.data = null;
	};

	retrieve(): kwMsg
	{
		//console.log("kwOrgMsg::retrieve() called.");
		return this.data;
	};

}
