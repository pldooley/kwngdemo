/**********************************************************************
 *
 * kw/main/org/kwOrgHttp.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/
// @formatter:off
import {Component, OnInit, OnDestroy}       from '@angular/core';
import {Subscription}                       from 'rxjs/Subscription';

import {kw}                                 from '../../kw';
import {kwHttpMsg}                          from '../../http/kwHttpMsg';
import { kwMsg }                            from "../../class/msg/kwMsg";

import {kwOrgMsg}                           from './kwOrgMsg';
import {kwOrgData}                          from './kwOrgData';
// @formatter:on


const sSTATE: string = "org";

@Component({
	selector: 'hcn-org-http',
	template: ``,
})
export class kwOrgHttp implements OnInit, OnDestroy
{

	subMsg: Subscription;

	constructor(    private srvMsg: kwOrgMsg,
	                private srvcData: kwOrgData )
	{
		//console.log("kwOrgHttp::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwOrgHttp::ngOnInit() called");

		this.subMsg = this.srvMsg.changed$.subscribe(msg =>
		{
			this.inspect(msg);
		});
	};

	ngOnDestroy()
	{
		//console.log("kwOrgHttp::ngOnDestroy() called.");

		this.subMsg.unsubscribe();
	}

	execute(msg: any): void
	{
		//console.log("kwOrgHttp::execute() called");

		let promise = kwHttpMsg.single(msg);

		if (!kw.isNull(promise))
		{
			console.error("kwOrgHttp::execute() promise is invalid.");
			return;
		}

		promise.then(function (data)
		{
			//console.info("kwOrgHttp::execute::then() called.");

			if (!kw.isNull(data))
			{
				if (!msg.isDelete())
				{
					console.error("kwOrgHttp::execute::then() data is invalid");
					return;
				}
			}

			//console.info("kwOrgHttp::execute::then() data is ", data);

			this.srvcData.change(data);

		}), function(reason)
		{
			console.error("kwOrgHttp::execute::error() called");
			alert('Failed: ' + reason);
			this.srvcData.change(null);
		}
	};

	inspect(msg)
	{
		//console.log("kwOrgHttp::inspectAction() called");

		if (!kwMsg.is(msg))
		{
			console.error("kwOrgHttp::inspectAction() msg is invalid.");
			return;
		}
		console.info("kwOrgHttp::inspectAction() msg is valid - esxecuting.");

		this.execute(msg);
	}

}
