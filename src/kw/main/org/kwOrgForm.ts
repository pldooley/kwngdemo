/**********************************************************************
 *
 * kw/main/org/kwOrgForm.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/
// @formatter:off
import {Injectable} from '@angular/core';

import {kw} from '../../kw';
import {kwFormEnum} from "../../class/form/kwFormEnum";
import {kwFormSrvc} from "../../class/form/kwFormSrvc";

import {kwOrgMsg} from "./kwOrgMsg";
import {kwOrgMdl} from './kwOrgMdl';
// @formatter:off



@Injectable()
export class kwOrgForm
{

	constructor(    private srvcMsg: kwOrgMsg,
					private srvcMdl: kwOrgMdl  )
	{
		//console.log("kwOrgForm::constructor() called.");
	}

	create(nForm: kwFormEnum, obj: any)
	{
		//console.log("kwOrgForm::create() called");
		return kwFormSrvc.create(nForm, obj, this, this.srvcMdl);
	}

	loadMeta(obj)
	{
		//console.log("kwOrgForm::loadMeta() called.");

		if (kw.isNull(obj))
		{
			console.error("kwOrgForm::loadMeta() obj is invalid");
			return;
		}

		let record = angular.copy(obj);

		return record;
	};

	save(nForm: kwFormEnum, obj): boolean
	{
		//console.log("kwOrgForm::save() called");
		return kwFormSrvc.save(nform, obj, this.srvcMdl, this.srvcMsg);
	}

}




