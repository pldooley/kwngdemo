/**********************************************************************
 *
 * kw/main/acc/kwOrgMdl.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/
// @formatter:off
import {EventEmitter, Injectable} from '@angular/core';

import {kw} from '../../kw';
// @formatter:off



@Injectable()
export class kwOrgMdl
{

	// Observable email sources
	private broadcast: EventEmitter<any>;

	data: any;

	// Observable string streams
	changed$ = this.broadcast;


	constructor()
	{
		//console.log("hcnEmailsStateData::constructor() called.");

		this.broadcast = new EventEmitter<any[]>();
		this.changed$=this.broadcast;
	}

	change(dataNew)
	{
		//console.log("kwOrgMdlModel::changed() called.");

		if (kw.isNull(dataNew))
		{
			console.error("kwOrgMdlModel::changed() dataNew is invalid.");
			return;
		}

		this.data = dataNew;
		//console.info("kwOrgMdlModel::changed() data is ", data);

		this.broadcast.emit(this.data);
	};

	clear()
	{
		//console.log("kwOrgMdlModel::clear() called.");

		data = null;
	};

	create()
	{
		//console.log("kwOrgMdlModel::create() called.");

		if (kw.isNull(this.data))
		{
			console.error("kwOrgMdlModel::create() data is invalid.");
			return false;
		}

		let rec = this.data.createRecord();
		if (kw.isNull(rec))
		{
			console.error("kwOrgMdlModel::xImport() rec is invalid.");
			return;
		}

		return rec;
	};

	retrieve()
	{
		//console.log("kwOrgMdlModel::get() called.");

		return data;
	};

	xExport(recs)
	{
		//console.log("kwOrgMdlModel::xExport() called.");

		if (kw.isNull(this.data))
		{
			console.error("kwOrgMdlModel::xExport() data is invalid.");
			return;
		}

		if (!kw.isArray(recs))
		{
			console.error("kwOrgMdlModel::xExport() recs is invalid.");
			return;
		}

		let recsX = this.data.xExport(recs);
		if (!kw.isArray(recsX))
		{
			console.error("kwOrgMdlModel::xExport() recsX is invalid.");
			return;
		}

		return recsX;
	};

	xExportRec(record)
	{
		//console.log("kwOrgMdlModel::xExportRec() called.");

		if (kw.isNull(this.data))
		{
			console.error("kwOrgMdlModel::xExportRec() data is invalid.");
			return;
		}

		if (kw.isNull(record))
		{
			console.error("kwOrgMdlModel::xExportRec() record is invalid.");
			return;
		}

		let recX = this.data.xExportRec(record);
		if (kw.isNull(recX))
		{
			console.error("kwOrgMdlModel::xExportRec() recX is invalid.");
			return;
		}

		return recX;
	};

	xImport(recs)
	{
		//console.log("kwOrgMdlModel::xImport() called.");

		if (kw.isNull(this.data))
		{
			console.error("kwOrgMdlModel::xImport() data is invalid.");
			return false;
		}

		if (!kw.isArray(recs))
		{
			console.error("kwOrgMdlModel::xImport() recs is invalid.");
			return false;
		}

		let recsX = this.data.xImport(recs);
		if (!kw.isArray(recsX))
		{
			console.error("kwOrgMdlModel::xImport() recsX is invalid.");
			return;
		}

		return recsX;
	};

	xImportRec(rec)
	{
		//console.log("kwOrgMdlModel::xImportRec() called.");

		if (kw.isNull(this.data))
		{
			console.error("kwOrgMdlModel::xImportRec() data is invalid.");
			return false;
		}

		if (kw.isNull(rec))
		{
			console.error("kwOrgMdlModel::xImportRec() rec is invalid.");
			return false;
		}

		let recX = this.data.xImportRec(rec);
		if (kw.isNull(recX))
		{
			console.error("kwOrgMdlModel::xImportRec() recX is invalid.");
			return;
		}

		return recX;
	};

}