/**********************************************************************
 *
 * kw/util/org/kwOrgApi.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/
// @formatter:off
import {EventEmitter, Injectable}   from '@angular/core';

import { kwApi }                    from "../../class/api/kwApi";
import { kwApiSrvc }                from "../../class/api/kwApiSrvc";

// @formatter:on


@Injectable()
export class kwOrgApi
{

	private broadcast: EventEmitter<kwApi>;

	data: kwApi;

	changed$: EventEmitter<kwApi>;

	constructor()
	{
		//console.log("kwOrgApi::constructor() called.");
		this.broadcast = new EventEmitter<kwApi>();
		this.changed$ = this.broadcast;
	}

	change(data: kwApi): void
	{
		//console.log("kwOrgApi::change() called.");

		if(!kwApiSrvc.isType(data))
		{
			console.error("kwOrgApi::change() data is invalid.");
			return
		}
		//console.info("kwOrgApi::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("kwOrgApi::clear() called.");
		this.data = null;
	};

	retrieve(): kwApi
	{
		//console.log("kwOrgApi::retrieve() called.");
		return this.data;
	};

}
