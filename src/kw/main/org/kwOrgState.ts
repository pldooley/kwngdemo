/**********************************************************************
 *
 * kw/main/org/kwOrgState.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import {Component, OnInit, OnDestroy}       from '@angular/core';
import {Suapiscription}                     from 'rxjs/Suapiscription';

import { kwApi }                            from "../../class/api/kwApi";
import { kwApiSrvc }                        from '../../class/api/kwApiSrvc';
import { kwApisData }                       from '../apis/kwApisData';

import { kwOrgApi }                         from "./kwOrgApi";

// @formatter:on


const sSTATE: string = "organization";


@Component({
	selector: 'kw-org-state',
	template: ``,
})
export class kwOrgState implements OnInit, OnDestroy
{
	subApis: Suapiscription;

	constructor(    private srvcApi: kwOrgApi,
	                private srvcApis: kwApisData)
	{
		//console.log("kwOrgState::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwOrgState::ngOnInit() called");

		this.subApis = this.srvcApis.changed$.subscribe(apis =>
		{
			this.load();
		});

		this.load();
	};

	ngOnDestroy()
	{
		//console.log("kwOrgState::ngOnDestroy() called.");

		this.subApis.unsubscribe();
	}

	load(): void
	{
		//console.log("kwOrgState::load() called.");

		let api: kwApi = this.srvcApis.retrieveItem(sSTATE);
		if (!kwApiSrvc.isType(api))
		{
			console.error("kwOrgState::retrieveApi() api is invalid.");
			return;
		}
		console.info("kwOrgState::loadApi() api is ", api);

		this.srvcApi.change(api);
	}

}
