/**********************************************************************
 *
 * kw/util/mectric/kwCurrForm.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Injectable }	            from '@angular/core';

import { kw }			            from '../../kw';
import { kwFormEnum }               from "../../class/form/kwFormEnum";
import { kwFormSrvc }               from "../../class/form/kwFormSrvc";

import { kwCurrMsg }                from "./kwCurrMsg";
import { kwCurrMdl }                from "./kwCurrMdl";
// @formatter:off


@Injectable()
export class kwCurrForm
{

	constructor(    private srvcMsg: kwCurrMsg,
					private srvcMdl: kwCurrMdl  )
	{
		//console.log("kwCurrForm::constructor() called.");
	}

	create(nForm: kwFormEnum, obj: object)
	{
		//console.log("kwCurrForm::create() called");
		return kwFormSrvc.create(nForm, obj, this, this.srvcMdl);
	}

	loadMeta(obj: object)
	{
		//console.log("kwCurrForm::loadMeta() called.");

		if (kw.isNull(obj))
		{
			console.error("kwCurrForm::loadMeta() obj is invalid");
			return;
		}

		return obj;

	};

	save(nForm: kwFormEnum, obj): boolean
	{
		//console.log("kwCurrForm::save() called");
		return kwFormSrvc.save(nform, obj, this.srvcMdl, this.srvcMsg);
	}

}




