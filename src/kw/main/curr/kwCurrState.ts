/**********************************************************************
 *
 * app/util/curr/kwCurrState.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Component }			        from '@angular/core';
import { OnInit }				        from '@angular/core';
import { OnDestroy }			        from '@angular/core';
import { Subscription }	                from 'rxjs/Subscription';

import { kwApi }                        from "../../class/api/kwApi";
import { kwApiSrvc }                    from "../../class/api/kwApiSrvc";
import { kwApisData }                   from '../apis/kwApisData';

import { kwCurrApi }	                from './kwCurrApi';
// @formatter:off


const sSTATE: string = "currency";


@Component({
	selector: 'hcn-curr-state',
	template: ``,
})
export class kwCurrState implements OnInit, OnDestroy
{
	subApis: Subscription;

	constructor(    private srvcApi: kwCurrApi,
	                private srvcApis: kwApisData    )
	{
		//console.log("kwCurrState::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwCurrState::ngOnInit() called");

		this.subApis = this.srvcApis.changed$.subscribe(apis =>
		{
			this.loadApi()
		});
	};

	ngOnDestroy()
	{
		//console.log("kwCurrState::ngOnDestroy() called.");

		this.sub.unsubscribe();
	}

	loadApi()
	{
		//console.log("kwCurrState::loadApi() called");

		let api: kwApi = this.srvcApis.retrieveItem(sSTATE);
		if (!kwApi.is(api))
		{
			console.error("kwCurrState::retrieveApi() api is invalid.");
			return;
		}
		//console.info("kwCurrState::loadApi() api is ", api);

		this.srvcApi.change(api);
	};

}
