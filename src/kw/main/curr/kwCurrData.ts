/**********************************************************************
 *
 * app/util/curr/kwCurrData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }			    from '@angular/core';
import { Injectable }			    from '@angular/core';

import { kwCurrSrvc }               from "../../class/curr/kwCurrSrvc";
import { kwCurrType }               from "../../class/curr/kwCurrType";

// @formatter:off



@Injectable()
export class kwCurrData
{

	private broadcast: EventEmitter<kwCurrType>;

	data: kwCurrType;

	changed$: EventEmitter<kwCurrType>;

	constructor()
	{
		//console.log("kwCurrData::constructor() called.");

		this.broadcast = new EventEmitter<kwCurrType>();
		this.changed$=this.broadcast;
	}

	change(data: kwCurrType)
	{
		//console.log("kwCurrData::change() called.");

		if (!kwCurrSrvc.isType(data)) {
			console.error("kwCurrData::change() data is invalid.");
			return
		}
		//console.info("kwCurrData::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("kwCurrData::clear() called.");
		this.data = null;
	};

	retrieve() : kwCurrType
	{
		//console.log("kwCurrData::retrieve() called.");
		return this.data;
	};

}
