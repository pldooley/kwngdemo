/**********************************************************************
 *
 * app/util/curr/kwCurrHttp.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Component }					from '@angular/core';
import { OnInit }						from '@angular/core';
import { OnDestroy }					from '@angular/core';
import { Subscription }					from 'rxjs/Subscription';

import { kw }					        from '../../kw';
import { kwHttpMsg }					from '../../http/kwHttpMsg';
import { kwMsg }                        from "../../class/msg/kwMsg";

import { kwCurrMsg }	                from './kwCurrMsg';
import { kwCurrData }		            from './kwCurrData';
// @formatter:on



@Component({
	selector: 'hcn-curr-http',
	template: ``,
})
export class kwCurrHttp implements OnInit, OnDestroy
{

	subMsg: Subscription;

	constructor(    private srvcMsg: kwCurrMsg,
	                private srvcData: kwCurrData )
	{
		//console.log("kwCurrHttp::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwCurrHttp::ngOnInit() called");

		this.subMsg = this.srvcMsg.changed$.subscribe(msg =>
		{
			this.inspect(msg);
		});
	};

	ngOnDestroy()
	{
		//console.log("kwCurrHttp::ngOnDestroy() called.");

		this.subMsg.unsubscribe();
	}

	execute(msg: kwMsg): void
	{
		//console.log("kwCurrHttp::execute() called");

		let promise = kwHttpMsg.single(msg);

		if (!kw.isNull(promise))
		{
			console.error("kwCurrHttp::execute() promise is invalid.");
			return;
		}

		promise.then(function (data)
		{
			//console.info("kwCurrHttp::execute::then() called.");

			if (!kw.isNull(data))
			{
				if (!msg.isDelete())
				{
					console.error("kwCurrHttp::execute::then() data is invalid");
					return;
				}
			}

			//console.info("kwCurrHttp::execute::then() data is ", data);

			this.srvcData.change(data);

		}), function(reason)
		{
			console.error("kwCurrHttp::execute::error() called");
			alert('Failed: ' + reason);
			this.srvcData.change(null);
		}
	};

	inspect(msg: kwMsg)
	{
		//console.log("kwCurrHttp::inspectAction() called");

		if (!kwMsg.is(msg))
		{
			console.error("kwCurrHttp::inspect() msg is invalid.");
			return;
		}
		console.info("kwCurrHttp::inspect() msg is valid - esxecuting.");

		this.execute(msg);
	}

}
