/**********************************************************************
 *
 * kw/util/curr/kwCurrApi.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/
// @formatter:off
import {EventEmitter, Injectable}   from '@angular/core';

import { kwApi }                    from "../../class/api/kwApi";
import { kwApiSrvc }                from "../../class/api/kwApiSrvc";

// @formatter:on


@Injectable()
export class kwCurrApi
{

	private broadcast: EventEmitter<kwApi>;

	data: kwApi;

	changed$: EventEmitter<kwApi>;

	constructor()
	{
		//console.log("kwCurrApi::constructor() called.");
		this.broadcast = new EventEmitter<kwApi>();
		this.changed$ = this.broadcast;
	}

	change(data: kwApi): void
	{
		//console.log("kwCurrApi::change() called.");

		if(!kwApiSrvc.isType(data))
		{
			console.error("kwCurrApi::change() data is invalid.");
			return
		}
		//console.info("kwCurrApi::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("kwCurrApi::clear() called.");
		this.data = null;
	};

	retrieve(): kwApi
	{
		//console.log("kwCurrApi::retrieve() called.");
		return this.data;
	};

}
