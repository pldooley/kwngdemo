/**********************************************************************
 *
 * kw/main/actTag/kwActTagData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import {EventEmitter, Injectable}           from '@angular/core';

import { kw }                               from "../../kw";
// @formatter:on


@Injectable()
export class kwActTagData
{

	private broadcast: EventEmitter<object>;

	data: object;

	changed$: EventEmitter<object>;

	constructor()
	{
		//console.log("kwActTagData::constructor() called.");

		this.broadcast = new EventEmitter<object>();
		this.changed$ = this.broadcast;
	}

	change(data: object)
	{
		//console.log("kwActTagData::change() called.");

		if( kw.isNull(data) )
		{
			console.error("kwActTagData::change() data is invalid.");
			return
		}
		console.info("kwActTagData::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("kwActTagData::clear() called.");
		this.data = null;
	};

	retrieve(): object
	{
		//console.log("kwActTagData::retrieve() called.");
		return this.data;
	};

}
