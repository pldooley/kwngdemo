/**********************************************************************
 *
 * kw/main/acc/kwActTagMdl.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/
// @formatter:off
import {EventEmitter, Injectable}   from '@angular/core';

import {kw}                         from '../../kw';
// @formatter:off



@Injectable()
export class kwActTagMdl
{

	// Observable email sources
	private broadcast: EventEmitter<object>;

	data: object;

	// Observable string streams
	changed$ = this.broadcast;


	constructor()
	{
		//console.log("hcnEmailsStateData::constructor() called.");

		this.broadcast = new EventEmitter<object>();
		this.changed$=this.broadcast;
	}

	change(dataNew: object)
	{
		//console.log("kwActTagMdlModel::changed() called.");

		if (kw.isNull(dataNew))
		{
			console.error("kwActTagMdlModel::changed() dataNew is invalid.");
			return;
		}

		this.data = dataNew;
		//console.info("kwActTagMdlModel::changed() data is ", data);

		this.broadcast.emit(this.data);
	};

	clear(): void
	{
		//console.log("kwActTagMdlModel::clear() called.");
		this.data = null;
	};

	create(): object
	{
		//console.log("kwActTagMdlModel::create() called.");

		if (kw.isNull(this.data))
		{
			console.error("kwActTagMdlModel::create() data is invalid.");
			return false;
		}

		let rec = this.data.createRecord();
		if (kw.isNull(rec))
		{
			console.error("kwActTagMdlModel::xImport() rec is invalid.");
			return;
		}

		return rec;
	};

	retrieve(): object
	{
		//console.log("kwActTagMdlModel::get() called.");
		return data;
	};

	xExport(recs)
	{
		//console.log("kwActTagMdlModel::xExport() called.");

		if (kw.isNull(this.data))
		{
			console.error("kwActTagMdlModel::xExport() data is invalid.");
			return;
		}

		if (!kw.isArray(recs))
		{
			console.error("kwActTagMdlModel::xExport() recs is invalid.");
			return;
		}

		let recsX = this.data.xExport(recs);
		if (!kw.isArray(recsX))
		{
			console.error("kwActTagMdlModel::xExport() recsX is invalid.");
			return;
		}

		return recsX;
	};

	xExportRec(record)
	{
		//console.log("kwActTagMdlModel::xExportRec() called.");

		if (kw.isNull(this.data))
		{
			console.error("kwActTagMdlModel::xExportRec() data is invalid.");
			return;
		}

		if (kw.isNull(record))
		{
			console.error("kwActTagMdlModel::xExportRec() record is invalid.");
			return;
		}

		let recX = this.data.xExportRec(record);
		if (kw.isNull(recX))
		{
			console.error("kwActTagMdlModel::xExportRec() recX is invalid.");
			return;
		}

		return recX;
	};

	xImport(recs)
	{
		//console.log("kwActTagMdlModel::xImport() called.");

		if (kw.isNull(this.data))
		{
			console.error("kwActTagMdlModel::xImport() data is invalid.");
			return false;
		}

		if (!kw.isArray(recs))
		{
			console.error("kwActTagMdlModel::xImport() recs is invalid.");
			return false;
		}

		let recsX = this.data.xImport(recs);
		if (!kw.isArray(recsX))
		{
			console.error("kwActTagMdlModel::xImport() recsX is invalid.");
			return;
		}

		return recsX;
	};

	xImportRec(rec)
	{
		//console.log("kwActTagMdlModel::xImportRec() called.");

		if (kw.isNull(this.data))
		{
			console.error("kwActTagMdlModel::xImportRec() data is invalid.");
			return false;
		}

		if (kw.isNull(rec))
		{
			console.error("kwActTagMdlModel::xImportRec() rec is invalid.");
			return false;
		}

		let recX = this.data.xImportRec(rec);
		if (kw.isNull(recX))
		{
			console.error("kwActTagMdlModel::xImportRec() recX is invalid.");
			return;
		}

		return recX;
	};

}