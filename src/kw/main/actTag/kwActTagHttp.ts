/**********************************************************************
 *
 * kw/main/actTag/kwActTagHttp.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/
// @formatter:off
import {Component, OnInit, OnDestroy}   from '@angular/core';
import {Subscription}                   from 'rxjs/Subscription';

import {kw}                             from '../../kw';
import {kwHttpMsg}                      from '../../http/kwHttpMsg';

import {kwActTagMsg}                    from './kwActTagMsg';
import {kwActTagData}                   from './kwActTagData'; import { kwMsgSrvc } from "../../class/msg/kwMsgSrvc";
// @formatter:on


const sSTATE: string = "actTag";

@Component({
	selector: 'kw-act-tag-http',
	template: ``,
})
export class kwActTagHttp implements OnInit, OnDestroy
{

	subMsg: Subscription;

	constructor(    private srvMsg: kwActTagMsg,
	                private srvcData: kwActTagData )
	{
		//console.log("kwActTagHttp::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwActTagHttp::ngOnInit() called");

		this.subMsg = this.srvMsg.changed$.subscribe(msg =>
		{
			this.inspect(msg);
		});
	};

	ngOnDestroy()
	{
		//console.log("kwActTagHttp::ngOnDestroy() called.");

		this.subMsg.unsubscribe();
	}

	execute(msg: any): void
	{
		//console.log("kwActTagHttp::execute() called");

		let promise = kwHttpMsg.single(msg);

		if (!kw.isNull(promise))
		{
			console.error("kwActTagHttp::execute() promise is invalid.");
			return;
		}

		promise.then(function (data)
		{
			//console.info("kwActTagHttp::execute::then() called.");

			if (!kw.isNull(data))
			{
				if (!msg.isDelete())
				{
					console.error("kwActTagHttp::execute::then() data is invalid");
					return;
				}
			}

			//console.info("kwActTagHttp::execute::then() data is ", data);

			this.srvcData.change(data);

		}), function(reason)
		{
			console.error("kwActTagHttp::execute::error() called");
			alert('Failed: ' + reason);
			this.srvcData.change(null);
		}
	};

	inspect(msg)
	{
		//console.log("kwActTagHttp::inspectAction() called");

		if (!kwMsg.is(msg))
		{
			console.error("kwActTagHttp::inspectAction() msg is invalid.");
			return;
		}
		console.info("kwActTagHttp::inspectAction() msg is valid - esxecuting.");

		this.execute(msg);
	}

}
