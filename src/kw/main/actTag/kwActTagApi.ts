/**********************************************************************
 *
 * kw/main/actTag/kwActTagApi.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/
// @formatter:off
import {EventEmitter, Injectable} from '@angular/core';

import { kwApi }                    from "../../class/api/kwApi";
import { kwApiSrvc }                from "../../class/api/kwApiSrvc";

// @formatter:on



@Injectable()
export class kwActTagApi
{

	private broadcast: EventEmitter<kwApi>;

	data: kwApi;

	changed$: EventEmitter<kwApi>;

	constructor()
	{
		//console.log("kwActTagApi::constructor() called.");

		this.broadcast = new EventEmitter<kwApi>();
		this.changed$ = this.broadcast;

	}

	change(data: kwApi): void
	{
		//console.log("kwActTagApi::change() called.");

		if( !kwApi.is(data) )
		{
			console.error("kwActTagApi::change() data is invalid.");
			return
		}
		//console.info("kwActTagApi::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("kwActTagApi::clear() called.");
		this.data = null;
	};

	retrieve(): kwApi
	{
		//console.log("kwActTagApi::retrieve() called.");
		return this.data;
	};


}
