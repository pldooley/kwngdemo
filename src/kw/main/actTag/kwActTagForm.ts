/**********************************************************************
 *
 * kw/main/actTag/kwActTagForm.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/
// @formatter:off
import {Injectable} from '@angular/core';

import {kw} from '../../kw';
import {kwFormEnum} from "../../class/form/kwFormEnum";
import {kwFormSrvc} from "../../class/form/kwFormSrvc";

import {kwActTagMsg} from "./kwActTagMsg";
import {kwActTagMdl} from './kwActTagMdl';
// @formatter:off



@Injectable()
export class kwActTagForm
{

	constructor(    private srvcMsg: kwActTagMsg,
					private srvcMdl: kwActTagMdl  )
	{
		//console.log("kwActTagForm::constructor() called.");
	}

	create(nForm: kwFormEnum, obj: any)
	{
		//console.log("kwActTagForm::create() called");
		return kwFormSrvc.create(nForm, obj, this, this.srvcMdl);
	}

	loadMeta(obj)
	{
		//console.log("kwActTagForm::loadMeta() called.");

		if (kw.isNull(obj))
		{
			console.error("kwActTagForm::loadMeta() obj is invalid");
			return;
		}

		let record = angular.copy(obj);

		return record;
	};

	save(nForm: kwFormEnum, obj): boolean
	{
		//console.log("kwActTagForm::save() called");
		return kwFormSrvc.save(nform, obj, this.srvcMdl, this.srvcMsg);
	}

}




