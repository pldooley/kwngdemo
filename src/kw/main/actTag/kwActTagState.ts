/**********************************************************************
 *
 * kw/main/accs/kwAccsState.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Component, OnInit, OnDestroy}       from '@angular/core';
import { Subscription}                       from 'rxjs/Subscription';

import { kwApi }                             from "../../class/api/kwApi";
import { kwApiSrvc}                          from '../../class/api/kwApiSrvc';
import { kwApisData}                         from '../apis/kwApisData';

import { kwAccsApi }                         from "../accs/kwAccsApi";
// @formatter:on


const sSTATE: string = "accounts";


@Component({
	selector: 'kw-accs-state',
	template: ``,
})
export class kwAccsState implements OnInit,
	OnDestroy
{
	subApis: Subscription;

	constructor(    private srvcApi: kwAccsApi,
	                private srvcApis: kwApisData)
	{
		//console.log("kwAccsState::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwAccsState::ngOnInit() called");

		this.subApis = this.srvcApis.changed$.subscribe(apis =>
		{
			this.load();
		});

		this.load();
	};

	ngOnDestroy()
	{
		//console.log("kwAccsState::ngOnDestroy() called.");

		this.subApis.unsubscribe();
	}

	load(): void
	{
		//console.log("kwAccsState::load() called.");

		let api: kwApi = this.srvcApis.retrieveItem(sSTATE);
		if (!kwApi.is(api))
		{
			console.error("kwAccsState::retrieveApi() api is invalid.");
			return;
		}
		console.info("kwAccsState::loadApi() api is ", api);

		this.srvcApi.change(api);
	}

}
