/**********************************************************************
 *
 * /kw/main/actTag/kwActTagMsg.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/
// @formatter:on
import { EventEmitter, Injectable }     from '@angular/core';

import { kwApi }                        from "../../class/api/kwApi";
import { kwApiSrvc }                    from "../../class/api/kwApiSrvc";
import { kwHttpAct }                    from "../../http/kwHttpAct";
import { kwMsg }                        from "../../class/msg/kwMsg";
import { kwMsgSrvc }                    from "../../class/msg/kwMsgSrvc";

import { kwActTagApi }                  from "../../http/actTag/kwActTagApi";

// @formatter:off


@Injectable()
export class kwActTagMsg
{

	private broadcast: EventEmitter<kwMsg>;

	data: kwMsg;

	changed$: EventEmitter<kwMsg>;

	constructor(private srvcApi: kwActTagApi)
	{
		//console.log("kwActTagMsg::constructor() called.");

		this.broadcast = new EventEmitter<kwMsg>();
		this.changed$=this.broadcast;
	}

	actionAdd(data, params)
	{
		//console.log("kwActTagMsg::actionAdd() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwActTagMsg::actionAdd() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.add(data, params, api);
		if (!kwMsg.is(msg))
		{
			console.error("kwActTagMsg::actionAdd() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionDelete(params)
	{
		//console.log("kwActTagMsg::actionDelete() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwActTagMsg::actionDelete() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.delete(params, api);
		if (!kwMsg.is(msg))
		{
			console.error("kwActTagMsg::actionDelete() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionEdit(data, params)
	{
		//console.log("kwActTagMsg::actionEdit() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwActTagMsg::actionEdit() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.edit(data, params, api);
		if (!kwMsg.is(msg))
		{
			console.error("kwActTagMsg::actionEdit() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionGet(params)
	{
		//console.log("kwActTagMsg::actionGet() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwActTagMsg::actionGet() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.get(params, api);
		if (!kwMsg.is(msg))
		{
			console.error("kwActTagMsg::actionGet() msg is invalid.");
			return;
		}

		this.change(msg);

	};

	change(data: kwMsg)
	{
		//console.log("kwActTagMsg::change() consolealled.");

		if (!kwMsg.is(data))
		{
			console.error("kwActTagMsg::change() data is invalid.");
			return
		}
		//console.info("kwActTagMsg::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("kwActTagMsg::clear() called.");
		this.data = null;
	};

	retrieve(): kwMsg
	{
		//console.log("kwActTagMsg::retrieve() called.");
		return this.data;
	};

}
