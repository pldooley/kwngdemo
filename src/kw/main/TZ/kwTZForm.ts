/**********************************************************************
 *
 * kw/main/tZ/kwTZForm.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Injectable }	            from '@angular/core';

import { kw }			            from '../../kw';
import { kwFormEnum }               from "../../class/form/kwFormEnum";
import { kwFormSrvc }               from "../../class/form/kwFormSrvc";

import { kwTZMsg }                  from "./kwTZMsg";
import { kwTZMdl }                  from "./kwTZMdl";
// @formatter:off


@Injectable()
export class kwTZForm
{

	constructor(    private srvcMsg: kwTZMsg,
					private srvcMdl: kwTZMdl  )
	{
		//console.log("kwTZForm::constructor() called.");
	}

	create(nForm: kwFormEnum, obj: any)
	{
		//console.log("kwTZForm::create() called");
		return kwFormSrvc.create(nForm, obj, this, this.srvcMdl);
	}

	loadMeta(obj)
	{
		//console.log("kwTZForm::loadMeta() called.");

		if (kw.isNull(obj))
		{
			console.error("kwTZForm::loadMeta() obj is invalid");
			return;
		}

		return obj;

	};

	save(nForm: kwFormEnum, obj): boolean
	{
		//console.log("kwTZForm::save() called");
		return kwFormSrvc.save(nform, obj, this.srvcMdl, this.srvcMsg);
	}

}




