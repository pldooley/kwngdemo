/**********************************************************************
 *
 * kw/main/TZ/kwTZHttp.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Component }					from '@angular/core';
import { OnInit }						from '@angular/core';
import { OnDestroy }					from '@angular/core';
import { Subscription }					from 'rxjs/Subscription';

import { kw }					        from '../../kw';
import { kwHttpMsg }					from '../../http/kwHttpMsg';
import { kwMsg }                        from "../../class/msg/kwMsg";

import { kwTZMsg }	                    from './kwTZMsg';
import { kwTZData }		                from './kwTZData';
// @formatter:on


const sSTATE: string = "timezone";

@Component({
	selector: 'hcn-timezone-http',
	template: ``,
})
export class kwTZHttp implements OnInit, OnDestroy
{

	subMsg: Subscription;

	constructor(    private srvcMsg: kwTZMsg,
	                private srvcData: kwTZData )
	{
		//console.log("kwTZHttp::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwTZHttp::ngOnInit() called");

		this.subMsg = this.srvcMsg.changed$.subscribe(msg =>
		{
			this.inspect(msg);
		});
	};

	ngOnDestroy()
	{
		//console.log("kwTZHttp::ngOnDestroy() called.");

		this.subAct.unsubscribe();
	}

	execute(msg: any): void
	{
		//console.log("kwTZHttp::execute() called");

		let promise = kwHttpMsg.single(msg);

		if (!kw.isNull(promise))
		{
			console.error("kwTZHttp::execute() promise is invalid.");
			return;
		}

		promise.then(function (data)
		{
			//console.info("kwTZHttp::execute::then() called.");

			if (!kw.isNull(data))
			{
				if (!msg.isDelete())
				{
					console.error("kwTZHttp::execute::then() data is invalid");
					return;
				}
			}

			//console.info("kwTZHttp::execute::then() data is ", data);

			this.srvcData.change(data);

		}), function(reason)
		{
			console.error("kwTZHttp::execute::error() called");
			alert('Failed: ' + reason);
			this.srvcData.change(null);
		}
	};

	inspect(msg)
	{
		//console.log("kwTZHttp::inspectAction() called");

		if (!kwMsg.is(msg))
		{
			console.error("kwTZHttp::inspectAction() msg is invalid.");
			return;
		}
		console.info("kwTZHttp::inspectAction() msg is valid - esxecuting.");

		this.execute(msg);
	}

}
