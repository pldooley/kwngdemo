/**********************************************************************
 *
 * /kw/main/tZ/kwTZMsg.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/
// @formatter:on
import { EventEmitter, Injectable }     from '@angular/core';

import { kwApi }                        from "../../class/api/kwApi";
import { kwHttpAct }                    from "../../http/kwHttpAct";
import { kwMsg }                        from "../../class/msg/kwMsg";

import { kwTZApi }                      from "./kwTZApi";
// @formatter:off


@Injectable()
export class kwTZMsg
{

	private broadcast: EventEmitter<kwMsg>;

	data: kwMsg;

	changed$: EventEmitter<kwMsg>;

	constructor(private srvcApi: kwTZApi)
	{
		//console.log("kwTZMsg::constructor() called.");

		this.broadcast = new EventEmitter<kwMsg>();
		this.changed$=this.broadcast;
	}

	actionAdd(data, params)
	{
		//console.log("kwTZMsg::actionAdd() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwTZMsg::actionAdd() api is invalid.");
			return;
		}

		let msg: kwMsg	 = kwHttpAct.add(data, params, api)
		if (!kwMsg.is(msg))
		{
			console.error("kwTZMsg::actionAdd() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionDelete(params)
	{
		//console.log("kwTZMsg::actionDelete() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwTZMsg::actionDelete() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.delete(params, api);
		if (!kwMsg.is(msg))
		{
			console.error("kwTZMsg::actionDelete() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionEdit(data, params)
	{
		//console.log("kwTZMsg::actionEdit() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwTZMsg::actionEdit() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.edit(data, params, api);
		if (!kwMsg.is(msg))
		{
			console.error("kwTZMsg::actionEdit() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionGet(params)
	{
		//console.log("kwTZMsg::actionGet() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwTZMsg::actionGet() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.get(params, api);
		if (!kwMsg.is(msg))
		{
			console.error("kwTZMsg::actionGet() msg is invalid.");
			return;
		}

		this.change(msg);

	};

	change(data: kwMsg)
	{
		//console.log("kwTZMsg::change() consolealled.");

		if (!kwMsg.is(data))
		{
			console.error("kwTZMsg::change() data is invalid.");
			return
		}
		//console.info("kwTZMsg::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("kwTZMsg::clear() called.");
		this.data = null;
	};

	retrieve(): kwMsg
	{
		//console.log("kwTZMsg::retrieve() called.");
		return this.data;
	};

}
