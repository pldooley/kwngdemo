/**********************************************************************
 *
 * kw/main/tZ/kwTZState.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Component }			        from '@angular/core';
import { OnInit }				        from '@angular/core';
import { OnDestroy }			        from '@angular/core';
import { Subscription }	                from 'rxjs/Subscription';

import { kwApi }                        from "../../class/api/kwApi";
import { kwApisData }                   from '../apis/kwApisData';

import { kwTZApi }	                    from './kwTZApi';
// @formatter:off


const sSTATE: string = "language";


@Component({
	selector: 'hcn-lang-state',
	template: ``,
})
export class kwTZState implements OnInit, OnDestroy
{
	subApis: Subscription;

	constructor(    private srvcApi: kwTZApi,
	                private srvcApis: kwApisData    )
	{
		//console.log("kwTZState::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwTZState::ngOnInit() called");

		this.subApis = this.srvcApis.changed$.subscribe(apis =>
		{
			this.loadApi()
		});
	};

	ngOnDestroy()
	{
		//console.log("kwTZState::ngOnDestroy() called.");

		this.subApis.unsubscribe();
	}

	loadApi()
	{
		//console.log("kwTZState::loadApi() called");

		let api: kwApi = this.srvcApis.retrieveItem(sSTATE);
		if (!kwApi.is(api))
		{
			console.error("kwTZState::retrieveApi() api is invalid.");
			return;
		}
		//console.info("kwTZState::loadApi() api is ", api);

		this.srvcApi.change(api);
	};

}
