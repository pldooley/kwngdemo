/**********************************************************************
 *
 * kw/main/tZ/kwTZApi.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/
// @formatter:off
import {EventEmitter, Injectable}   from '@angular/core';

import { kwApi }                    from "../../class/api/kwApi";
// @formatter:on


@Injectable()
export class kwTZApi
{

	private broadcast: EventEmitter<kwApi>;

	data: kwApi;

	changed$: EventEmitter<kwApi>;

	constructor()
	{
		//console.log("kwTZApi::constructor() called.");
		this.broadcast = new EventEmitter<kwApi>();
		this.changed$ = this.broadcast;
	}

	change(data: kwApi): void
	{
		//console.log("kwTZApi::change() called.");

		if(!kwApi.is(data))
		{
			console.error("kwTZApi::change() data is invalid.");
			return
		}
		//console.info("kwTZApi::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("kwTZApi::clear() called.");
		this.data = null;
	};

	retrieve(): kwApi
	{
		//console.log("kwTZApi::retrieve() called.");
		return this.data;
	};

}
