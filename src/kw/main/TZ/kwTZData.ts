/**********************************************************************
 *
 * kw/main/tZ/kwTZData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }			from '@angular/core';
import { Injectable }			from '@angular/core';

import { kwTZSrvc }             from "../../class/TZ/kwTZSrvc";
import { kwTZType }             from "../../class/TZ/kwTZType";

// @formatter:off



@Injectable()
export class kwTZData
{

	private broadcast: EventEmitter<kwTZType>;

	data: kwTZType;

	changed$: EventEmitter<kwTZType>;

	constructor()
	{
		//console.log("kwTZData::constructor() called.");

		this.broadcast = new EventEmitter<kwTZType>();
		this.changed$=this.broadcast;
	}

	change(data: kwTZType)
	{
		//console.log("kwTZData::change() called.");

		if (!kwTZSrvc.isType(data)) {
			console.error("kwTZData::change() data is invalid.");
			return
		}
		//console.info("kwTZData::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("kwTZData::clear() called.");
		this.data = null;
	};

	retrieve() : kwTZType
	{
		//console.log("kwTZData::retrieve() called.");
		return this.data;
	};

}
