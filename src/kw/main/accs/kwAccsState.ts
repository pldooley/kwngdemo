/**********************************************************************
 *
 * kw/main/accs/kwAccState.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Component, OnInit, OnDestroy}       from '@angular/core';
import { Subscription}                       from 'rxjs/Subscription';

import { kwApi }                             from "../../class/api/kwApi";
import { kwApiSrvc}                          from '../../class/api/kwApiSrvc';
import { kwApisData}                         from '../apis/kwApisData'; 
// @formatter:on


const sSTATE: string = "account";


@Component({
	selector: 'kw-acc-state',
	template: ``,
})
export class kwAccState implements OnInit,
	OnDestroy
{
	subApis: Subscription;

	constructor(    private srvcApi: kwAccApi,
	                private srvcApis: kwApisData)
	{
		//console.log("kwAccState::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwAccState::ngOnInit() called");

		this.subApis = this.srvcApis.changed$.subscribe(apis =>
		{
			this.load();
		});

		this.load();
	};

	ngOnDestroy()
	{
		//console.log("kwAccState::ngOnDestroy() called.");

		this.subApis.unsubscribe();
	}

	load(): void
	{
		//console.log("kwAccState::load() called.");

		let api: kwApi = this.srvcApis.retrieveItem(sSTATE);
		if (!kwApi.is(api))
		{
			console.error("kwAccState::retrieveApi() api is invalid.");
			return;
		}
		console.info("kwAccState::loadApi() api is ", api);

		this.srvcApi.change(api);
	}

}
