/**********************************************************************
 *
 * kw/main/accs/kwAccsData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }             from '@angular/core';
import { Injectable }	            from '@angular/core';

import { kw }                       from "../../kw";
// @formatter:off



@Injectable()
export class kwAccsData
{

	// Observable email sources
	private broadcast: EventEmitter<object[]>;

	data: object[];

	// Observable string streams
	changed$ = this.broadcast;


	constructor()
	{
		//console.log("kwAccsData::constructor() called.");

		this.broadcast = new EventEmitter<object[]>();
		this.changed$=this.broadcast;
	}

	change(newData: object[])
	{
		//console.log("kwAccsData::change() called.");

		if (kw.isNull(newData)) {
			console.error("kwAccsData::change() data is invalid.");
			return
		}
		//console.info("kwAccsData::change() data is [", data, "]");

		let data = kwAccMdl.xImportRec(newData);
		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("hcnEmailsStateData::clear() called.");
		this.data = null
	};

	retrieve() : object[]
	{
		//console.log("hcnEmailsStateData::retrieve() called.");
		return this.data;
	};

	retrieveId(): number
	{
		//console.log("kwAccsData::retrieveId() called.");

		if (kw.isNull(data))
		{
			console.error("kwAccsData::retrieveId() data is invalid.");
			return undefined;
		}

		if (!data.hasOwnProperty("id"))
		{
			console.error("kwAccsData::retrieveId() id is missing.");
			return
		}

		let nId = data.id;
		if (!kw.isNumber(nId))
		{
			console.error("kwAccsData::retrieveId() nId is invalid.");
			return
		}

		return nId;
	};

}