/**********************************************************************
 *
 * /kw/main/accs/kwAccsMsg.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:on
import { EventEmitter }             from '@angular/core';
import { Injectable }	            from '@angular/core';
import { isArray }                  from "util";

import { kwApi }                    from "../../class/api/kwApi";
import { kwApisSrvc }               from "../../class/apis/kwApisSrvc";
import { kwApisType }               from "../../class/apis/kwApisType";
import { kwHttpAct }                from "../../http/kwHttpAct";
import { kwMsg }				    from '../../class/msg/kwMsg';
import { kwMsgSrvc }                from "../../class/msg/kwMsgSrvc";

import { kwAccsApi }                from "./kwAccsApi";
// @formatter:off


@Injectable()
export class kwAccsMsg
{

	private broadcast: EventEmitter<kwMsg>;

	data: kwMsg;

	changed$: EventEmitter<kwMsg>;

	constructor(private srvcApi: kwAccsApi)
	{
		//console.log("srvcHttpMetrixkwMsg::constructor() called.");

		this.broadcast = new EventEmitter<kwMsg>();
		this.changed$=this.broadcast;
	}

	actionAdd(data, params)
	{
		//console.log("kwAccsMsg::actionAdd() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApis.is(api))
		{
			console.error("kwAccsMsg::actionAdd() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.add(data, params, api)
		if (!kwMsg.is(msg))
		{
			console.error("kwAccsMsg::actionAdd() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionDelete(params)
	{
		//console.log("kwAccsMsg::actionDelete() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApis.is(api))
		{
			console.error("kwAccsMsg::actionDelete() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.delete(params, api)
		if (!kwMsg.is(msg))
		{
			console.error("kwAccsMsg::actionDelete() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionEdit(data, params)
	{
		//console.log("kwAccsMsg::actionEdit() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApis.is(api))
		{
			console.error("kwAccsMsg::actionEdit() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.edit(data, params, api)
		if (!kwMsg.is(msg))
		{
			console.error("kwAccsMsg::actionEdit() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionGet(params)
	{
		//console.log("kwAccsMsg::actionGet() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApis.is(api))
		{
			console.error("kwAccsMsg::actionGet() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.get( params, api)
		if (!kwMsg.is(msg))
		{
			console.error("kwAccsMsg::actionGet() msg is invalid.");
			return;
		}

		this.change(msg);

	};

	change(data: kwMsg)
	{
		//console.log("srvcHttpMetrixAction::change() consolealled.");

		if (!data)
		{
			console.error("srvcHttpMetrixAction::change() data is invalid.");
			return
		}
		//console.info("srvcHttpMetrixAction::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("srvcHttpMetrixAction::clear() called.");
		this.data = null;
	};

	retrieve(): kwMsg
	{
		//console.log("srvcHttpMetrixAction::retrieve() called.");

		return this.data;
	};

}
