/**********************************************************************
 *
 * kw/main/accs/kwAccsApi.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import {EventEmitter, Injectable}       from '@angular/core';

import { kwApi }                        from "../../class/api/kwApi";
import { kwApiSrvc }                    from "../../class/api/kwApiSrvc";
// @formatter:on


@Injectable()
export class kwAccsApi
{

	private broadcast: EventEmitter<kwApi>;

	data: kwApi;

	changed$: EventEmitter<kwApi>;

	constructor()
	{
		//console.log("kwAccsApi::constructor() called.");

		this.broadcast = new EventEmitter<kwApi>();
		this.changed$ = this.broadcast;

	}

	// Service AccsApi commands
	change(data: kwApi): void
	{
		//console.log("kwAccsApi::change() called.");

		if( !kwApi.is(data) )
		{
			console.error("kwAccsApi::change() data is invalid.");
			return
		}
		//console.info("kwAccsApi::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("kwAccsApi::clear() called.");
		this.data = null;
	};

	retrieve(): kwApi
	{
		//console.log("kwAccsApi::retrieve() called.");
		return this.data;
	};


}
