/**********************************************************************
 *
 * /kw/main/accs/kwAccsHttp.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Component }				from '@angular/core';
import { OnInit }					from '@angular/core';
import { OnDestroy }				from '@angular/core';
import { Subscription }             from 'rxjs/Subscription';

import { kw }		                from "../../kw";
import { kwHttpMsg }		        from "../../http/kwHttpMsg";
import { kwMsgSrvc }                from "../../class/msg/kwMsgSrvc";

import { kwAccsMsg }		        from "./kwAccsMsg";
import { kwAccsData }		        from "./kwAccsData";
// @formatter:on


@Component({
	selector: 'kw-accs-http',
	template: ``,
})
export class kwAccsHttp implements OnInit, OnDestroy
{

	sub: Subscription;

	constructor(private srvcMsg: kwAccsMsg,
	            private srvcData: kwAccsData   )
	{
		//console.log("kwAccsHttp::constructor() called");
	}

	ngOnInit(): void
	{
		//console.log("kwAccsHttp::ngOnInit() called");

		this.sub = this.srvcMsg.changed$.subscribe(msg =>
		{
			this.inspect(msg);
		});
	};

	execute(msg: any): void
	{
		//console.log("kwAccsHttp::execute() called");

		let promise = kwHttpMsg.multiple(msg);

		if (!kw.isNull(promise))
		{
			console.error("kwAccsHttp::execute() promise is invalid.");
			return;
		}

		promise.then(function (data)
		{
			//console.info("kwAccsHttp::execute::then() called.");

			if (!kw.isNull(data))
			{
				if (!msg.isDelete())
				{
					console.error("kwAccsHttp::execute::then() data is invalid");
					return;
				}
			}

			//console.info("kwAccsHttp::execute::then() data is ", data);

			this.srvcData.change(data);

		}), function(reason)
		{
			console.error("kwAccsHttp::execute::error() called");
			alert('Failed: ' + reason);
			this.srvcData.change(null);
		}
	};

	inspect(msg)
	{
		//console.log("kwAccsState::inspectAction() called");

		if (!kwMsg.is(msg))
		{
			console.error("kwAccsState::inspectAction() msg is invalid.");
			return;
		}
		//console.info("kwAccsState::inspectAction() msg is valid - esxecuting.");

		this.execute(msg);
	}

}
