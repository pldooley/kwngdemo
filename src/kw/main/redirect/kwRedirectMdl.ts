/**********************************************************************
 *
 * hcn/app/redirect/kwRedirectMdl.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }             from '@angular/core';
import { Injectable }	            from '@angular/core';

import { kw }			            from '../../kw';
import { kwModelSrvc }              from "../../class/model/kwModelSrvc";
// @formatter:off


@Injectable()
export class kwRedirectMdl
{

	// Observable email sources
	private broadcast: EventEmitter<object>;

	data: object;

	// Observable string streams
	changed$ = this.broadcast;


	constructor()
	{
		//console.log("kwRedirectMdl::constructor() called.");

		this.broadcast = new EventEmitter<object>();
		this.changed$=this.broadcast;
	}

	change(dataNew)
	{
		//console.log("kwRedirectMdl::changed() called.");

		if (kw.isNull(dataNew))
		{
			console.error("kwRedirectMdl::changed() dataNew is invalid.");
			return;
		}

		this.data = dataNew;
		//console.info("kwRedirectMdl::changed() data is ", data);

		this.broadcast.emit(this.data);
	};

	clear()
	{
		//console.log("kwRedirectMdl::clear() called.");
		this.data = null;
	};

	create()
	{
		//console.log("kwRedirectMdl::create() called.");

		if (kw.isNull(this.data))
		{
			console.error("kwRedirectMdl::create() data is invalid.");
			return false;
		}

		let rec = this.data.createRecord();
		if (kw.isNull(rec))
		{
			console.error("kwRedirectMdl::xImport() rec is invalid.");
			return;
		}

		return rec;
	};

	retrieve(): object
	{
		return this.data;
	};

	xExport(recs)
	{
		return kwModelSrvc.xExport(this.data, recs);
	};

	xExportRec(rec)
	{
		return kwModelSrvc.xExportRec(this.data, rec);
	};

	xImport(recs)
	{
		return kwModelSrvc.xImport(this.data, recs);
	};

	xImportRec(rec)
	{
		return kwModelSrvc.xImportRec(this.data, rec);
	};

}