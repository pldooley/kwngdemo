/**********************************************************************
 *
 * kw/util/redirect/kwRedirectApi.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/
// @formatter:off
import {EventEmitter, Injectable}   from '@angular/core';

import { kwApi }                    from "../../class/api/kwApi";
import { kwApiSrvc }                from "../../class/api/kwApiSrvc";

// @formatter:on


@Injectable()
export class kwRedirectApi
{

	private broadcast: EventEmitter<kwApi>;

	data: kwApi;

	changed$: EventEmitter<kwApi>;

	constructor()
	{
		//console.log("kwRedirectApi::constructor() called.");
		this.broadcast = new EventEmitter<kwApi>();
		this.changed$ = this.broadcast;
	}

	change(data: kwApi): void
	{
		//console.log("kwRedirectApi::change() called.");

		if(!kwApiSrvc.isType(data))
		{
			console.error("kwRedirectApi::change() data is invalid.");
			return
		}
		//console.info("kwRedirectApi::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("kwRedirectApi::clear() called.");
		this.data = null;
	};

	retrieve(): kwApi
	{
		//console.log("kwRedirectApi::retrieve() called.");
		return this.data;
	};

}
