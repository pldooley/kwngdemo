/**********************************************************************
 *
 * app/util/redirect/kwRedirectState.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Component }			        from '@angular/core';
import { OnInit }				        from '@angular/core';
import { OnDestroy }			        from '@angular/core';
import { Subscription }	                from 'rxjs/Subscription';

import { kwApiSrvc }                    from '../../class/api/kwApiSrvc';
import { kwApi }                        from "../../class/api/kwApi";
import { kwApisData }					from '../apis/kwApisData';

import { kwRedirectApi }	            from './kwRedirectApi';
// @formatter:off


const sSTATE: string = "redirect";


@Component({
	selector: 'hcn-redirect-state',
	template: ``,
})
export class kwRedirectState implements OnInit, OnDestroy
{
	subApis: Subscription;

	constructor(    private srvcApi: kwRedirectApi,
	                private srvcApis: kwApisData    )
	{
		//console.log("kwRedirectState::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwRedirectState::ngOnInit() called");

		this.subApis = this.srvcApis.changed$.subscribe(apis =>
		{
			this.loadApi()
		});
	};

	ngOnDestroy()
	{
		//console.log("kwRedirectState::ngOnDestroy() called.");

		this.subApis.unsubscribe();
	}

	loadApi()
	{
		//console.log("kwRedirectState::loadApi() called");

		let api: kwApi = this.srvcApis.retrieveItem(sSTATE);
		if (!kwApi.is(api))
		{
			console.error("kwRedirectState::retrieveApi() api is invalid.");
			return;
		}
		//console.info("kwRedirectState::loadApi() api is ", api);

		this.srvcApi.change(api);
	};

}
