/**********************************************************************
 *
 * app/util/redirect/kwRedirectData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }			 from '@angular/core';
import { Injectable }			 from '@angular/core';
// @formatter:off



@Injectable()
export class kwRedirectData
{

	private broadcast: EventEmitter<object>;

	data: object;

	changed$: EventEmitter<object>;

	constructor()
	{
		//console.log("kwRedirectData::constructor() called.");

		this.broadcast = new EventEmitter<object>();
		this.changed$=this.broadcast;
	}

	change(data: object)
	{
		//console.log("kwRedirectData::change() called.");

		if (kw.isNull(data)) {
			console.error("kwRedirectData::change() data is invalid.");
			return
		}
		//console.info("kwRedirectData::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("kwRedirectData::clear() called.");
		this.data = null;
	};

	retrieve() : object
	{
		//console.log("kwRedirectData::retrieve() called.");
		return this.data;
	};

}
