/**********************************************************************
 *
 * app/util/redirect/kwRedirectHttp.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Component }					from '@angular/core';
import { OnInit }						from '@angular/core';
import { OnDestroy }					from '@angular/core';
import { Subscription }					from 'rxjs/Subscription';

import { kw }					        from '../../kw';
import { kwHttpMsg }					from '../../http/kwHttpMsg';
import { kwMsg }                        from "../../class/msg/kwMsg";
import { kwMsgSrvc }                    from "../../class/msg/kwMsgSrvc";

import { kwRedirectMsg }	            from './kwRedirectMsg';
import { kwRedirectData }		        from './kwRedirectData';

// @formatter:on


const sSTATE: string = "redirect";

@Component({
	selector: 'hcn-redirect-http',
	template: ``,
})
export class kwRedirectHttp implements OnInit, OnDestroy
{

	subMsg: Subscription;

	constructor(    private srvcMsg: kwRedirectMsg,
	                private srvcData: kwRedirectData )
	{
		//console.log("kwRedirectHttp::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwRedirectHttp::ngOnInit() called");

		this.subMsg = this.srvcMsg.changed$.subscribe(msg =>
		{
			this.inspect(msg);
		});
	};

	ngOnDestroy()
	{
		//console.log("kwRedirectHttp::ngOnDestroy() called.");

		this.subMsg.unsubscribe();
	}

	execute(msg: kwMsg): void
	{
		//console.log("kwRedirectHttp::execute() called");

		let promise = kwHttpMsg.single(msg);

		if (!kw.isNull(promise))
		{
			console.error("kwRedirectHttp::execute() promise is invalid.");
			return;
		}

		promise.then(function (data)
		{
			//console.info("kwRedirectHttp::execute::then() called.");

			if (!kw.isNull(data))
			{
				if (!msg.isDelete())
				{
					console.error("kwRedirectHttp::execute::then() data is invalid");
					return;
				}
			}

			//console.info("kwRedirectHttp::execute::then() data is ", data);

			this.srvcData.change(data);

		}), function(reason)
		{
			console.error("kwRedirectHttp::execute::error() called");
			alert('Failed: ' + reason);
			this.srvcData.change(null);
		}
	};

	inspect(msg: kwMsg): void
	{
		//console.log("kwRedirectHttp::inspectAction() called");

		if (!kwMsg.is(msg))
		{
			console.error("kwRedirectHttp::inspectAction() msg is invalid.");
			return;
		}
		console.info("kwRedirectHttp::inspectAction() msg is valid - executing.");

		this.execute(msg);
	}

}
