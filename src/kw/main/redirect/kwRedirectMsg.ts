/**********************************************************************
 *
 * /kw/main/redirect/kwRedirectMsg.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/
// @formatter:on
import { EventEmitter, Injectable }     from '@angular/core';

import { kwApi }                        from "../../class/api/kwApi";
import { kwApiSrvc }                    from "../../class/api/kwApiSrvc";
import { kwHttpAct }                    from "../../http/kwHttpAct";
import { kwMsg }                        from "../../class/msg/kwMsg";
import { kwMsgSrvc }                    from "../../class/msg/kwMsgSrvc";

import { kwRedirectApi }                from "./kwRedirecApi";

// @formatter:off


@Injectable()
export class kwRedirectMsg
{

	private broadcast: EventEmitter<kwMsg>;

	data: kwMsg;

	changed$: EventEmitter<kwMsg>;

	constructor(private srvcApi: kwRedirectApi)
	{
		//console.log("kwRedirectMsg::constructor() called.");

		this.broadcast = new EventEmitter<kwMsg>();
		this.changed$=this.broadcast;
	}

	actionAdd(data, params)
	{
		//console.log("kwRedirectMsg::actionAdd() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwRedirectMsg::actionAdd() api is invalid.");
			return;
		}

		let msg: kwMsg	 = kwHttpAct.add(data, params, api)
		if (!kwMsg.is(msg))
		{
			console.error("kwRedirectMsg::actionAdd() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionDelete(params)
	{
		//console.log("kwRedirectMsg::actionDelete() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwRedirectMsg::actionDelete() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.delete(params, api);
		if (!kwMsg.is(msg))
		{
			console.error("kwRedirectMsg::actionDelete() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionEdit(data, params)
	{
		//console.log("kwRedirectMsg::actionEdit() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwRedirectMsg::actionEdit() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.edit(data, params, api)
		if (!kwMsg.is(msg))
		{
			console.error("kwRedirectMsg::actionEdit() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionGet(params)
	{
		//console.log("kwRedirectMsg::actionGet() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwRedirectMsg::actionGet() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.get(params, api);
		if (!kwMsg.is(msg))
		{
			console.error("kwRedirectMsg::actionGet() msg is invalid.");
			return;
		}

		this.change(msg);

	};

	change(data: kwMsg)
	{
		//console.log("kwRedirectMsg::change() consolealled.");

		if (!kwMsg.is(data))
		{
			console.error("kwRedirectMsg::change() data is invalid.");
			return
		}
		//console.info("kwRedirectMsg::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("kwRedirectMsg::clear() called.");
		this.data = null;
	};

	retrieve(): kwMsg
	{
		//console.log("kwRedirectMsg::retrieve() called.");
		return this.data;
	};

}
