/**********************************************************************
 *
 * kw/util/mectric/kwRedirectForm.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Injectable }	            from '@angular/core';

import { kw }			            from '../../kw';
import { kwFormEnum }               from "../../class/form/kwFormEnum";
import { kwFormSrvc }               from "../../class/form/kwFormSrvc";

import { kwRedirectMsg }            from "./kwRedirectMsg";
import { kwRedirectMdl }            from "./kwRedirectMdl";
// @formatter:off


@Injectable()
export class kwRedirectForm
{

	constructor(    private srvcMsg: kwRedirectMsg,
					private srvcMdl: kwRedirectMdl  )
	{
		//console.log("kwRedirectForm::constructor() called.");
	}

	create(nForm: kwFormEnum, obj: any)
	{
		//console.log("kwRedirectForm::create() called");
		return kwFormSrvc.create(nForm, obj, this, this.srvcMdl);
	}

	loadMeta(obj)
	{
		//console.log("kwRedirectForm::loadMeta() called.");

		if (kw.isNull(obj))
		{
			console.error("kwRedirectForm::loadMeta() obj is invalid");
			return;
		}

		return obj;

	};

	save(nForm: kwFormEnum, obj): boolean
	{
		//console.log("kwRedirectForm::save() called");
		return kwFormSrvc.save(nform, obj, this.srvcMdl, this.srvcMsg);
	}

}




