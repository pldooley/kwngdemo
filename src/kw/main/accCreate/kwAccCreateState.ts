/**********************************************************************
 *
 * kw/state/accCreate/kwAccCreateState.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/
// @formatter:off

import { Component }						from '@angular/core';
import { OnInit }							from '@angular/core';
import { OnDestroy }						from '@angular/core';
import { Subscription }						from 'rxjs/Subscription';

import { kw }				                from '../../kw';
import { kwAccCreateData}				from "./kwAccCreateData";

// @formatter:off


@Component({
	selector: 'kw-acc-create-state',
	template: ``,
})
export class kwAccCreateState implements OnInit, OnDestroy
{
	sub: Subscription;

	constructor(private srvcData: kwAccCreateData)
	{
		//console.log("kwAccCreateState::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwAccCreateState::ngOnInit() called");

		this.sub = this.srvcData.changed$.subscribe(info =>
		{
			this.inspect(data);
		});

	};

	ngOnDestroy()
	{
		//console.log("kwAccCreateState::ngOnDestroy() called");
		this.sub.unsubscribe();
	};

	inspect(data)
	{
		//console.log("kwAccCreateState::inspect() called");

		if (kw.isNull(data))
		{
			console.info("kwAccCreateState::inspect() data is invalid - doing nothing");
			return;
		}
		console.info("kwAccCreateState::inspect() data is ", data);

		console.info("kwAccCreateState::inspect() data is valid - doing nothing");
	};

}