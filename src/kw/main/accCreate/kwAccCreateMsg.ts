/**********************************************************************
 *
 * kw/main/accCreate/kwAccCreateAct.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:on
import { EventEmitter }             from '@angular/core';
import { Injectable }	            from '@angular/core';

import { kw }				        from '../../kw';
import { kwHttpAct }                from "../../http/kwHttpAct";
import { kwActType }				from '../../class/act/kwActType';
import { kwApisSrvc }               from "../../class/apis/kwApisSrvc";
import { kwApisType }               from "../../class/apis/kwApisType";
import { kwMsg }                    from "../../class/msg/kwMsg";
import { kwMsgSrvc }                from "../../class/msg/kwMsgSrvc";
import { kwMsgType }                from "../../class/msg/kwMsgType";

import { kwAccCreateHttpApi }       from "./kwAccCreateApi";
// @formatter:off


@Injectable()
export class kwAccCreateAct
{

	private broadcast: EventEmitter<kwMsg>;

	data: kwMsg;

	changed$: EventEmitter<kwMsg>;

	constructor()
	{
		//console.log("srvcHttpMetrixkwActType::constructor() called.");

		this.broadcast = new EventEmitter<kwMsg>();
		this.changed$=this.broadcast;
	}

	actionAdd(data, params): void
	{
		//console.log("kwAccCreateAct::actionAdd() called.");

		let apis: kwApisType = kwAccCreateHttpApi.get();
		if (!kwApisSrvc.isType(apis))
		{
			console.error("kwAccCreateAct::actionAdd() apis is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.add(data, params, apis)
		if (!kwMsg.is(msg))
		{
			console.error("kwAccCreateAct::actionAdd() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionDelete(params): void
	{
		//console.log("kwAccCreateAct::actionDelete() called.");

		let apis: kwApisType = kwAccCreateHttpApis.get();
		if (!kwApisSrvc.isType(apis))
		{
			console.error("kwAccCreateAct::actionDelete() apis is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.delete(data, params, apis)
		if (!kwMsg.is(msg))
		{
			console.error("kwAccCreateAct::actionDelete() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionEdit(data, params): void
	{
		//console.log("kwAccCreateAct::actionEdit() called.");

		let apis: kwApisType = kwAccCreateHttpApis.get();
		if (!kwApisSrvc.isType(apis))
		{
			console.error("kwAccCreateAct::actionEdit() apis is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.edit(data, params, apis)
		if (!kwMsg.is(msg))
		{
			console.error("kwAccCreateAct::actionEdit() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionGet(params): void
	{
		//console.log("kwAccCreateAct::actionGet() called.");

		let apis: kwApisType = kwAccCreateHttpApis.get();
		if (!kwApisSrvc.isType(apis))
		{
			console.error("kwAccCreateAct::actionGet() apis is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.get(data, params, apis)
		if (!kwMsg.is(msg))
		{
			console.error("kwAccCreateAct::actionGet() msg is invalid.");
			return;
		}

		this.change(msg);

	};

	change(data: kwMsg)
	{
		//console.log("srvcHttpMetrixAction::change() consolealled.");

		if (!kwMsg.is(data))
		{
			console.error("srvcHttpMetrixAction::change() data is invalid.");
			return
		}
		console.info("srvcHttpMetrixAction::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("srvcHttpMetrixAction::clear() called.");
		this.data = null;
	};

	retrieve(): kwMsg
	{
		//console.log("srvcHttpMetrixAction::retrieve() called.");

		return this.data;
	};

}
