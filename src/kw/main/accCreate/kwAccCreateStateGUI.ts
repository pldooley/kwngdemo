/**********************************************************************
 *
 * kw/main/acc/kwAccCreateStateGUI.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }             from '@angular/core';
import { Injectable }	            from '@angular/core';

import { kw }			            from '../../kw';
import { kwAccCreateAct }       from "./kwAccCreateMsg";
import { kwAccCreateStateMdl }		from './kwAccCreateStateMdl';
import { kwAccStateGUI }		    from '../acc/kwAccForm';
import { kwFormEnum }               from "../../class/form/kwFormEnum";
import { kwFormSrvc }               from "../../class/form/kwFormSrvc"; import { kwCur } from "../../class/curr/kwCurr"; import { kwCurSrvc } from "../../class/curr/kwCurrSrvc";
// @formatter:off



@Injectable()
export class kwAccCreateStateGUI
{

	constructor()
	{
		//console.log("kwAccCreateStateGUI::constructor() called.");
	}

	create(nForm: kwFormEnum, obj: any): any
	{
		//console.log("kwAccCreateStateGUI::create() called");

		let form = {};

		form.nMode = kwFormEnum.Add;

		form.bIsRoot = true;

		let acc = kwAccStateGUI.create(kwFormEnum.Add);
		if (kw.isNull(acc))
		{
			console.error("kwAccCreateStateGUI::create() acc is invalid");
			return;
		}
		console.info("kwAccCreateStateGUI::create() acc is ", acc);
		form.acc = acc;

		let sp = kwCustomStateGUI.create(kwFormEnum.Add);
		if (kw.isNull(sp))
		{
			console.error("kwAccCreateStateGUI::create() sp is invalid");
			return;
		}
		//console.info("kwAccCreateStateGUI::create() sp is ", sp);
		form.sp = sp;

		let user = kwUserStateGUI.create(kwFormEnum.Add);
		if (kw.isNull(user))
		{
			console.error("kwAccCreateStateGUI::create() user is invalid");
			return;
		}
		//console.info("kwAccCreateStateGUI::create() user is ", user);
		form.user = user;

		//console.info("kwAccCreateStateGUI::create() form is ", form);

		return form;
	}

	loadMeta(obj)
	{
		//console.log("kwAccCreateStateGUI::loadMeta() called.");

		if (kw.isNull(obj))
		{
			console.error("kwAccCreateStateGUI::loadMeta() obj is invalid");
			return;
		}

		let record = angular.copy(obj);

		let cur: kwCur = kwCurState.get();
		if (kwCur.is(cur))
		{
			console.error("kwAccCreateStateGUI::load() cur is invalid");
			return;
		}
		console.info("kwAccCreateStateGUI::formLoad() cur is", cur);
		record.currencies = cur;

		let lang: kwLang = kwLangState.get();
		if (kwLangSrvc.isl(lang))
		{
			console.error("kwAccCreateStateGUI::load() lang is invalid");
			return;
		}
		console.info("kwAccCreateStateGUI::formLoad() lang is", lang);
		record.languages = lang;

		let TZ: kwTZ = kwTZState.get();
		if (!kwTZ.is(TZ))
		{
			console.error("kwAccCreateStateGUI::load() TZ is invalid");
			return;
		}
		console.info("kwAccCreateDataGUI::load() TZ is", TZ);
		record.timeZones = TZ;

		return record;
	};

	save(nForm: kwFormEnum, obj)
	{
		//console.log("kwAccCreateStateGUI::save() called");

		if (!kwFormSrvc.in(nForm))
		{
			console.error("kwAccCreateStateGUI::save() nForm is invalid");
			return;
		}
		//console.info("kwAccCreateStateGUI::save() nForm is", nForm);

		if (kw.isNull(obj))
		{
			console.error("kwAccCreateStateGUI::save() obj is invalid");
			return;
		}

		let objX = kwAccCreateStateMdl.xExportRec(obj);
		if (kw.isNull(objX))
		{
			console.error("kwAccCreateStateGUI::save() objX is invalid");
			return;
		}

		switch(nForm)
		{
			case kwFormEnum.Add:
			{
				kwAccCreateAct.actionAdd(objX, []);

				break;
			}

			case kwFormEnum.Edit:
			{
				let nId = objX.id;
				if (!kw.isNumber(nId))
				{
					console.error("kwAccCreateStateGUI::save() nId is invalid");
					return;
				}

				kwAccCreateAct.actionEdit(objX, [nId]);

				break;
			}

			default:
			{
				console.error("kwAccCreateStateGUI::save() nForm is invalid");
			}
		}
	}

}




