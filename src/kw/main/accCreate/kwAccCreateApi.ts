/**********************************************************************
 *
 * kw/main/accCreate/kwAccCreateHttpApi.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import {EventEmitter, Injectable}       from '@angular/core';

import {kwBSType}                       from "../../util/BS/kwBSType";
import {kwHttpUrlSrvc}                  from "../../http/kwHttpUrlSrvc";
import {kwUrlInType}                    from "../../class/urlIn/kwUrlInType";
// @formatter:on


@Injectable()
export class kwAccCreateHttpApi
{

	private broadcast: EventEmitter<kwUrlInType>;

	data: kwUrlInType;

	changed$: EventEmitter<kwUrlInType>;

	constructor(private srvcApi: kwAccCreateApi)
	{
		//console.log("kwAccCreateHttpApi::constructor() called.");

		this.broadcast = new EventEmitter<kwUrlInType>();
		this.changed$ = this.broadcast;

	}

	// Service AccCreateApi commands
	change(data: kwUrlInType): void
	{
		//console.log("kwAccCreateHttpApi::change() called.");

		if( !data )
		{
			console.error("kwAccCreateHttpApi::change() data is invalid.");
			return
		}
		//console.info("kwAccCreateHttpApi::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("kwAccCreateHttpApi::clear() called.");

		this.data = null;
	};

	load(data: kwUrlInType): void
	{
		//console.log("kwAccCreateHttpApi::load() called.");

		if( !data )
		{
			console.error("kwAccCreateHttpApi::load() data is invalid.");
			return
		}
		//console.info("kwAccCreateHttpApi::load() data is [", data, "]");

		let url: kwUrlInType = this.srvcUrl.toUrl(data, sAPI, sMODE)
		if( !url )
		{
			console.error("kwAccCreateHttpApi::load() url is invalid.");
			return
		}
		//console.info("kwAccCreateHttpApi::load() url is [", url, "]");

		this.change(url);
	};

	retrieve(): kwUrlInType
	{
		//console.log("kwAccCreateHttpApi::retrieve() called.");

		return this.data;
	};


}
