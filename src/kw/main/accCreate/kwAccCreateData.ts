/**********************************************************************
 *
 * kw/main/acc/kwAccCreateData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }       from '@angular/core';
import { Injectable }         from '@angular/core';

// @formatter:on


@Injectable()
export class kwAccCreateData
{

	private broadcast: EventEmitter<any>;

	data: any;

	changed$: EventEmitter<any>;

	constructor()
	{
		//console.log("kwAccCreateData::constructor() called.");

		this.broadcast = new EventEmitter<any>();
		this.changed$ = this.broadcast;

	}

	change(data: any): void
	{
		//console.log("kwAccCreateData::change() called.");

		if(! data)
		{
			console.error("kwAccCreateData::change() data is invalid.");
			return
		}
		//console.info("kwAccCreateData::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("kwAccCreateData::clear() called.");

		this.data = null;
	};

	retrieve(): any
	{
		//console.log("kwAccCreateData::retrieve() called.");

		return this.data;
	};


}
