/**********************************************************************
 *
 * kw/main/accCreate/kwAccCreateHttp.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Component }				from '@angular/core';
import { OnInit }					from '@angular/core';
import { OnDestroy }				from '@angular/core';
import { Subscription }             from 'rxjs/Subscription';

import { kw }		                from "../../kw";
import { kwAccCreateHttpApi }		from "./kwAccCreateApi";
import { kwHttpMsg }		        from "../kwHttpMsg";
import { kwAccCreateAct }		    from "./kwAccCreateMsg";
import { kwAccCreateData }		from "./kwAccCreateData";
import { kwApiStateData }		    from "../../state/api/kwApiStateData";
import { kwApiType }		        from "../../class/api/kwApiType"; import { kwMsgSrvc } from "../../class/msg/kwMsgSrvc"; import { kwMsg } from "../../class/msg/kwMsg";
// @formatter:on


const sSTATE: string = "accountCreate";


@Component({
	selector: 'kw-acc-create-http',
	template: ``,
})
export class kwAccCreateHttp implements OnInit, OnDestroy
{

	subMsg: Subscription;
	subApi: Subscription;

	constructor(private srvcMsg: kwAccCreateAct,
	            private srvcApi: kwAccCreateHttpApi,
	            private srvcApi: kwApiStateData,
	            private srvcData: kwAccCreateData   )
	{
		//console.log("kwAccCreateHttp::constructor() called");
	}

	ngOnInit(): void
	{
		//console.log("kwAccCreateHttp::ngOnInit() called");

		this.subMsg = this.srvcMsg.changed$.subscribe(info =>
		{
			this.inspectAct(info);
		});
	};

	execute(msg: kwMsg): void
	{
		//console.log("kwAccCreateHttp::execute() called");

		let promise = kwHttpMsg.single(msg);

		if (!kw.isNull(promise))
		{
			console.error("kwAccCreateHttp::execute() promise is invalid.");
			return;
		}

		promise.then(function (data)
		{
			//console.info("kwAccCreateHttp::execute::then() called.");

			if (!kw.isNull(data))
			{
				if (!msg.isDelete())
				{
					console.error("kwAccCreateHttp::execute::then() data is invalid");
					return;
				}
			}

			//console.info("kwAccCreateHttp::execute::then() data is ", data);

			kwAccCreateData.changed(data);

		}), function(reason)
		{
			console.error("kwAccCreateHttp::execute::error() called");
			alert('Failed: ' + reason);
			kwAccCreateData.changed(null);
		}
	};

	inspectAct(msg: kwMsg): void
	{
		//console.log("kwAccCreateState::inspectAct() called");

		if (!kwMsg.is(msg))
		{
			console.error("kwAccCreateState::inspectAct() msg is invalid.");
			return;
		}
		//console.info("kwAccCreateState::inspectAct() msg is valid - esxecuting.");

		this.execute(msg);
	}


}
