(function () {

	"use strict";

	angular.module('fuse').service('kwStateAccCreateDataGUI',
		[
			'enumForm',
			'kwStateAccCreateDataAction',
			'srvcStateAccountGUI',
			'srvcStateCustomGUI',
			'srvcStateUserGUI',
			'srvcStateXFull',
			kwStateAccCreateDataGUI
		])

	function kwStateAccCreateDataGUI(
		enumForm,
		kwStateAccCreateDataAction,
		srvcStateAccountGUI,
		srvcStateCustomGUI,
		srvcStateUserGUI,
		srvcStateXFull
	)
	{
		//console.log("kwStateAccCreateDataGUI::constructor() called.");

		function create()
		{
			//console.log("kwStateAccCreateDataGUI::create() called");

			let form = {};

			form.nMode = enumForm.ADD;

			form.bIsRoot = true;

			let acc = srvcStateAccountGUI.create(enumForm.ADD);
			if (!isObject(acc))
			{
				console.error("kwStateAccCreateDataGUI::create() acc is invalid");
				return;
			}
			//console.debugI("kwStateAccCreateDataGUI::create() acc is ", acc);
			form.acc = acc;

			let sp = srvcStateCustomGUI.create(enumForm.ADD);
			if (!isObject(sp))
			{
				console.error("kwStateAccCreateDataGUI::create() sp is invalid");
				return;
			}
			//console.debugI("kwStateAccCreateDataGUI::create() sp is ", sp);
			form.sp = sp;

			let user = srvcStateUserGUI.create(enumForm.ADD);
			if (!isObject(user))
			{
				console.error("kwStateAccCreateDataGUI::create() user is invalid");
				return;
			}
			//console.debugI("kwStateAccCreateDataGUI::create() user is ", user);
			form.user = user;

			//console.debugI("kwStateAccCreateDataGUI::create() form is ", form);

			return form;
		}

		let isValid = function (form)
		{
			//console.log("kwStateAccCreateDataGUI::isValid() called.");

			if (!isObject(form))
			{
				console.error("kwStateAccCreateDataGUI::isValid() form is invalid.");
				return false;
			}

			return true;
		};

		function save(form)
		{
			//console.log("kwStateAccCreateDataGUI::save() called");

			if (!isValid(form))
			{
				console.error("kwStateAccCreateDataGUI::save() form is invalid");
				return;
			}

			let formX = xExport(form);
			if (!isObject(formX))
			{
				console.error("kwStateAccCreateDataGUI::save() formX is invalid");
				return;
			}

			kwStateAccCreateDataAction.actionAdd(formX, []);
		}

		let xExport = function (form)
		{
			//console.log("kwStateAccCreateDataGUI::xExport() called.");

			if (!isObject(form))
			{
				console.error("kwStateAccCreateDataGUI::xExport() form is invalid.");
				return
			}
			//console.debug("kwStateAccCreateDataGUI::xExport() form is ", form);

			let record = {};

			xExportAcc(form, record);
			xExportCustom(form, record);
			xExportUser(form, record);

			//console.debug("kwStateAccCreateDataGUI::xExport() record is ", record);

			return record;
		};

		let xExportAcc = function (form, record)
		{
			//console.log("kwStateAccCreateDataGUI::xExportAcc() called.");

			if (!isObject(form))
			{
				console.error("kwStateAccCreateDataGUI::xExportAcc() form is invalid.");
				return
			}
			//console.debug("kwStateAccCreateDataGUI::xExportAcc() form is ", form);

			if (!isObject(record))
			{
				console.error("kwStateAccCreateDataGUI::xExportAcc() record is invalid.");
				return
			}
			//console.debug("kwStateAccCreateDataGUI::xExportAcc() record is ", record);

			let acc = form.acc;
			if (!isObject(acc))
			{
				console.error("kwStateAccCreateDataGUI::xExportAcc() acc is invalid.");
				return
			}
			//console.debug("kwStateAccCreateDataGUI::xExportAcc() acc is ", acc);

			record.companyName 			= srvcStateXFull.getString({"record":acc,"sField":"sCompanyName"});
			record.currency 			= srvcStateXFull.getCurrencyCode({"record":acc,"sField":"currency"});
			record.isServiceProvider	= srvcStateXFull.getNumber({"record":acc,"sField":"bIsServiceProvider"});
			record.language 			= srvcStateXFull.getLanguageCode({"record":acc,"sField":"language"});
			record.timeZone 			= srvcStateXFull.getTimeZoneCode({"record":acc,"sField":"timeZone"});

			//console.debug("kwStateAccCreateDataGUI::xExportAcc() record is ", record)
		};

		let xExportCustom = function (form, record)
		{
			//console.log("kwStateAccCreateDataGUI::xExportCustom() called.");

			if (!isObject(form))
			{
				console.error("kwStateAccCreateDataGUI::xExportCustom() form is invalid.");
				return
			}
			//console.debug("kwStateAccCreateDataGUI::xExportCustom() form is ", form);

			if (!isObject(record))
			{
				console.error("kwStateAccCreateDataGUI::xExportCustom() record is invalid.");
				return
			}
			//console.debug("kwStateAccCreateDataGUI::xExportCustom() record is ", record);

			let custom = form.sp;
			if (!isObject(custom))
			{
				console.error("kwStateAccCreateDataGUI::xExportCustom() custom is invalid.");
				return
			}
			//console.debug("kwStateAccCreateDataGUI::xExportCustom() custom is ", custom);

			record.subdomain 	= srvcStateXFull.getString({"record":custom,"sField":"sSubdomain"});
			record.cname		= srvcStateXFull.getString({"record":custom,"sField":"sCName"});

			//console.debug("kwStateAccCreateDataGUI::xExportCustom() record is ", record)
		};

		let xExportUser = function (form, record)
		{
			//console.log("kwStateAccCreateDataGUI::xExportUser() called.");

			if (!isObject(form))
			{
				console.error("kwStateAccCreateDataGUI::xExportUser() form is invalid.");
				return
			}
			//console.debug("kwStateAccCreateDataGUI::xExportUser() form is ", form);

			if (!isObject(record))
			{
				console.error("kwStateAccCreateDataGUI::xExportUser() record is invalid.");
				return
			}
			//console.debug("kwStateAccCreateDataGUI::xExportUser() record is ", record);

			let user = form.user;
			if (!isObject(user))
			{
				console.error("kwStateAccCreateDataGUI::xExportUser() user is invalid.");
				return
			}
			//console.debug("kwStateAccCreateDataGUI::xExportUser() user is ", user);

			record.username 		= srvcStateXFull.getString({"record":user,"sField":"sUsername"});
			record.password 		= srvcStateXFull.getString({"record":user,"sField":"sPassword"});
			record.maxFailedLogins	= srvcStateXFull.getNumber({"record":user,"sField":"nMaxFailedLogins"});

			//console.debug("kwStateAccCreateDataGUI::xExportUser() record is ", record)
		};

		return {
			create: create,
			isValid: isValid,
			save: save
		};
	}

})();




