/**********************************************************************
 *
 * kw/main/acc/kwAccBSState.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/
// @formatter:off

import { Component }						from '@angular/core';
import { OnInit }							from '@angular/core';
import { OnDestroy }						from '@angular/core';
import { Subscription }						from 'rxjs/Subscription';

import { kw }                               from "../../kw";
import { kwAccBSStateData }					from "./kwAccBSStateData";
import { kwAccMdl }					from "../acc/kwAccMdl";
import { kwAppData }					from "../../state/app/kwAppData";

// @formatter:off


@Component({
	selector: 'kw-acc-bs-state',
	template: ``,
})
export class kwAccBSState implements OnInit, OnDestroy
{
	subApp: Subscription;
	subBS: Subscription;
	
	constructor(	private srvcAppData: kwAppData,
					private srvcBSData: kwAccBSStateData,
					private srvcMdl: kwAccMdl			 )
	{
		//console.log("kwAccBSState::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwAccBSState::ngOnInit() called");

		this.subApp = this.srvcAppData.changed$.subscribe(info =>
		{
			this.inspectApp(info);
		});

		this.subBS = this.srvcBSData.changed$.subscribe(info =>
		{
			this.inspectBS(info);
		});

	};

	ngOnDestroy()
	{
		//console.log("kwAccBSState::ngOnDestroy() called.");

		this.subApp.unsubscribe();
		this.subBS.unsubscribe();
	}

	inspectApp(data)
	{
		//console.log("kwAccBSState::inspectApp() called");

		if (this.srvcAppData.isLogout())
		{
			//console.info("kwAccBSState::inspectApp() logging out - clearing state");
			srvcStateAccountBS.changed(null);
			return;
		}

		//console.info("kwAccBSState::inspectApp() - doing nothing.");
	};

	inspectBS(data)
	{
		//console.log("kwAccBSState::inspectBS() called");

		if (kw.isNull(data))
		{
			//console.info("kwAccBSState::inspectBS() data is invalid - doing nothing.");
			return;
		}

		let account = data.account;
		if (kw.isNull(account))
		{
			console.error("kwAccBSState::inspectBootstrap() account is invalid");
			return;
		}
		console.info("kwAccBSState::inspectBootstrap() account is valid - storing State.");

		console.info("kwAccBSState::inspectBootstrap() account is ", account);

		kwAccBSStateData.change(account);
	};

}