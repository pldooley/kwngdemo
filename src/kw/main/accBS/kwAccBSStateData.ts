/**********************************************************************
 *
 * kw/main/acc/kwAccBSStateData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }             from '@angular/core';
import { Injectable }	            from '@angular/core';

import { kwAccMdl }			from '../acc/kwAccMdl';
// @formatter:off


@Injectable()
export class kwAccBSStateData
{

	// Observable email sources
	private broadcast: EventEmitter<any>;

	data: any;

	// Observable string streams
	changed$ = this.broadcast;


	constructor()
	{
		//console.log("hcnBSStateEmailsData::constructor() called.");

		this.broadcast = new EventEmitter<any[]>();
		this.changed$=this.broadcast;
	}

	change(newData: any[])
	{
		//console.log("hcnBSStateEmailsData::change() called.");

		if (!newData) {
			console.error("hcnBSStateEmailsData::change() data is invalid.");
			return
		}
		//console.info("hcnBSStateEmailsData::change() data is [", data, "]");

		let data = kwAccMdl.xImportRec(newData);
		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("hcnBSStateEmailsData::clear() called.");
		srvcCookieAccount.clear();
	};

	retrieve() : any[]
	{
		//console.log("hcnBSStateEmailsData::retrieve() called.");

		return this.data;
	};

	retrieveId()
	{
		//console.log("kwAccBSStateData::retrieveId() called.");

		if (!isValid(data))
		{
			console.error("kwAccBSStateData::retrieveId() data is invalid.");
			return undefined;
		}

		if (!data.hasOwnProperty("id"))
		{
			console.error("kwAccBSStateData::retrieveId() id is missing.");
			return
		}

		let nId = data.id;
		if (!kw.isNumber(nId))
		{
			console.error("kwAccBSStateData::retrieveId() nId is invalid.");
			return
		}

		return nId;
	};

}
