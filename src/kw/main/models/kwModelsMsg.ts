/**********************************************************************
 *
 * /kw/main/models/kwModelsMsg.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:on
import { EventEmitter }             from '@angular/core';
import { Injectable }	            from '@angular/core';

import { kwApi }                    from "../../class/api/kwApi";
import { kwHttpAct }                from "../../http/kwHttpAct";
import { kwMsg }                    from "../../class/msg/kwMsg";

import { kwModelsApi }              from "./kwModelsApi";
// @formatter:off


@Injectable()
export class kwModelsMsg
{

	private broadcast: EventEmitter<kwMsg>;

	data: kwMsg;

	changed$: EventEmitter<kwMsg>;

	constructor(private srvcApi: kwModelsApi)
	{
		//console.log("kwModelsMsg::constructor() called.");

		this.broadcast = new EventEmitter<kwMsg>();
		this.changed$=this.broadcast;
	}

	actionAdd(data, params)
	{
		//console.log("kwModelsMsg::actionAdd() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwModelsMsg::actionAdd() api is invalid.");
			return;
		}

		let msg: kwMsg	 = kwHttpAct.add(data, params, api);
		if (!kwMsg.is(msg))
		{
			console.error("kwModelsMsg::actionAdd() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionDelete(params)
	{
		//console.log("kwModelsMsg::actionDelete() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwModelsMsg::actionDelete() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.delete(params, api);
		if (!kwMsg.is(msg))
		{
			console.error("kwModelsMsg::actionDelete() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionEdit(data, params)
	{
		//console.log("kwModelsMsg::actionEdit() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwModelsMsg::actionEdit() url is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.edit(data, params, api)
		if (!kwMsg.is(msg))
		{
			console.error("kwModelsMsg::actionEdit() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionGet(params)
	{
		//console.log("kwModelsMsg::actionGet() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwModelsMsg::actionGet() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.get(params, api);
		if (!kwMsg.is(msg))
		{
			console.error("kwModelsMsg::actionGet() msg is invalid.");
			return;
		}

		this.change(msg);

	};

	change(data: kwMsg)
	{
		//console.log("kwModelsMsg::change() consolealled.");

		if (!kwMsg.is(data))
		{
			console.error("kwModelsMsg::change() data is invalid.");
			return
		}
		//console.info("kwModelsMsg::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("kwModelsMsg::clear() called.");
		this.data = null;
	};

	retrieve(): kwMsg
	{
		//console.log("kwModelsMsg::retrieve() called.");
		return this.data;
	};

}
