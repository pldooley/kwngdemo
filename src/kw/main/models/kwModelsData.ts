/**********************************************************************
 *
 * kw/main/models/kwModelsData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/
// @formatter:off
import { EventEmitter }			from '@angular/core';
import { Injectable }           from '@angular/core';

import { kw }                   from "../../kw";
import { kwModel }              from "../../class/model/kwModel";
import { kwModelEnum }          from "../../class/model/kwModelEnum";
import { kwModelSrvc }          from "../../class/model/kwModelSrvc";
import { kwModelSub }           from "../../class/model/kwModelSub";
import { kwModelType }          from "../../class/model/kwModelType";
// @formatter:on



@Injectable()
export class kwModelsData
{

	private broadcast: EventEmitter<object>;

	data: object;

	changed$: EventEmitter<object>;

	constructor()
	{
		//console.log("kwModelsData::constructor() called.");
		this.broadcast = new EventEmitter<object>();
		this.changed$ = this.broadcast;
	}

	change(data: object)
	{
		//console.log("kwModelsData::change() called.");

		if( kw.isNull(data) )
		{
			console.error("kwModelsData::change() data is invalid.");
			return
		}
		//console.info("kwModelsData::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("kwModelsData::clear() called.");

		this.data = null;
	};

	retrieve(): object
	{
		//console.log("kwModelsData::retrieve() called.");

		return this.data;
	};

	getModel(sModel): kwModel
	{
		//console.log("srvcStateModels::getModel() called.");
	
		if(kw.isNull(this.data))
		{
			console.error("srvcStateModels::getModel() data is invalid.");
			return;
		}
	
		if(!kw.isString(sModel))
		{
			console.error("srvcStateModels::getModel() sModel is invalid.");
			return;
		}
	
		let type: kwModelType = this.data[sModel];
		if(!kwModelSrvc.isType(type))
		{
			console.error("srvcStateModels::getModel() model for [" + sModel + "] is invalid.");
			return;
		}
	
		let sType = type.sType;
		if(!kw.isString(sType))
		{
			console.error("srvcStateModels::getModel() sType is invalid.");
			return;
		}

		let nType: kwModelEnum = kwModelSrvc.toEnum(sType);
		if (!kwModelSrvc.in(nType))
		{
			console.error("srvcStateModels::getModel() nType is invalid.");
			return;
		}
	
		let model: kwModel;
	
		switch (nType)
		{
			case kwModelEnum.Sub:
			{
				model = new kwModelSub(type);
				break;
			}

			case kwModelEnum.Full:
			{
				model = new kwModelSub(type);
				break;
			}

			default:
			{
				console.error("srvcStateModels::getModel() error creating model.");
				return;
			}
		}


		if(!kwModel.is(model))
		{
			console.error("srvcStateModels::getModel() error creating model..");
			return;
		}
	
		if (!model.init())
		{
			console.error("ctrlHttpBootstrap::getModel() error initializing model.");
			return;
		}
	
		return model;
	};


}