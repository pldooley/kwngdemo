/**********************************************************************
 *
 * kw/main/models/kwModelsState.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
import { Component }						from '@angular/core';
import { OnInit }							from '@angular/core';

import { kwModelsMsg }                      from "./kwModelsMsg";
// @formatter:on


@Component({
	selector: 'kw-models-state',
	template: ``,
})
export class kwModelsState implements OnInit
{
	constructor(private srvcMsg: kwModelsMsg)
	{
		//console.log("kwModelsState::constructor()called");
	}

	ngOnInit()
	{
		//console.log("kwModelsState::ngOnInit()called");
		this.retrieve();
	};

	retrieve()
	{
		//console.log("kwModelsState::retrieve() called.");
		this.srvcMsg.actionGet([])
	}

}
