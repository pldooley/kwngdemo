/**********************************************************************
 *
 * kw/main/models/kwModelsHttp.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Component, OnDestroy }             from '@angular/core';
import { OnInit }							from '@angular/core';
import { Subscription }						from 'rxjs/Subscription';

import { kw }		                        from "../../kw";
import { kwHttpMsg }                        from "../../http/kwHttpMsg";
import { kwMsg }                            from "../../class/msg/kwMsg";

import { kwModelsData }		                from './kwModelsData';
import { kwModelsMsg }		                from './kwModelsMsg';
// @formatter:on


const sTAG: string = "kwModelsHttp";


@Component({
	selector: 'kw-models-http',
	template: ``,
})
export class kwModelsHttp implements OnInit, OnDestroy
{

	subMsg: Subscription;

	constructor(private srvcData: kwModelsData,
	            private srvcMsg: kwModelsMsg    )
	{
		//console.log("kwModelsHttp::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwModelsHttp::ngOnInit() called");

		this.subMsg = this.srvcMsg.changed$.subscribe(msg =>
		{
			this.inspect(msg);
		});
	};

	ngOnDestroy()
	{
		//console.log("kwModelsHttp::ngOnDestroy() called.");
		this.subMsg.unsubscribe();
	}

	execute(msg: kwMsg): void
	{
		//console.log("kwMetricHttp::execute() called");

		let promise = kwHttpMsg.multiple(msg);

		if( !kw.isNull(promise) )
		{
			console.error("kwMetricHttp::execute() promise is invalid.");
			return;
		}

		promise.then(function(data)
		{
			//console.info("kwMetricHttp::execute::then() called.");

			if( !kw.isNull(data) )
			{
				if( !msg.isDelete() )
				{
					console.error("kwMetricHttp::execute::then() data is invalid");
					return;
				}
			}

			//console.info("kwMetricHttp::execute::then() data is ", data);

			this.srvcData.change(data);

		}), function(reason)
		{
			console.error("kwMetricHttp::execute::error() called");
			alert('Failed: ' + reason);
			this.srvcData.change(null);
		}
	}

	inspect(msg)
	{
		//console.log("kwApisHttp::inspect() called");

		if( !kwMsg.is(msg) )
		{
			console.error("kwApisHttp::inspect() msg is invalid.");
			return;
		}

		console.info("kwApisHttp::inspect() msg is valid - executing.");

		this.execute(msg);
	}

}
