/**********************************************************************
 *
 * kw/main/apis/kwModelsApi.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }                 from '@angular/core';
import { Injectable }                   from '@angular/core';

import { kwApi }                        from "../../class/api/kwApi";
import { kwApiSrvc }                    from "../../class/api/kwApiSrvc";
// @formatter:on



@Injectable()
export class kwModelsApi
{

	private broadcast: EventEmitter<kwApi>;

	data: kwApi;

	changed$: EventEmitter<kwApi>;

	constructor()
	{
		//console.log("kwModelsApi::constructor() called.");

		this.broadcast = new EventEmitter<kwApi>();
		this.changed$ = this.broadcast;
	}

	change(data: kwApi): void
	{
		//console.log("kwModelsApi::change() called.");

		if( !kwApiSrvc.isType(data) )
		{
			console.error("kwModelsApi::change() data is invalid.");
			return
		}
		//console.info("kwModelsApi::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("kwModelsApi::clear() called.");
		this.data = null;
	};

	retrieve(): kwApi
	{
		//console.log("kwModelsApi::retrieve() called.");
		return this.data;
	};


}
