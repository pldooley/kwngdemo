/**********************************************************************
 *
 * kw/kwAppBS.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Component }			from '@angular/core';
import { OnInit }				from '@angular/core';
//import { OpaqueToken }          from "@angular/core"
import { NgZone }               from "@angular/core";

import { kw }                   from "./kw";
// @formatter:on


//export let APP_CONFIG = new OpaqueToken("app.config");

//export interface IappConfig{
//	apiEndpoint: string;
//}

//export const AppConfig: IAppConfig = {
//	apiEndpoint: "http://localhost:15422/api/"
//}

const sURL:  string = "resources/config/bootstrap.json";


@Component({
	selector: 'kw-app-bootstrap',
	template: ``,
})
export class kwAppBS implements OnInit
{

	$http: any;
	bootstrap: any;
	bootstrapApi: any;
	credentials: any;
	hosts: any;
	services: any;

	sMode: string;
	sPath: string;
	sRedirect: string;
	sService: string;
	sToken: string;
	sURLBootstrap: string;

	bIsLive: boolean;


	constructor(private zone: NgZone)
	{
		console.log("kwAppBS::::constructor() called.");
	}

	ngOnInit()
	{
		console.log("kwAppBS::::ngOnInit() called.");

		this.zone.run(() =>
		{
			document.ready = () =>
			{
				this.loadBootstrap();
			}

		});
	}

	loadBootstrap(): void
	{
		console.log("kwAppBS::loadBootstrap() called.");

		let promise = this.retrieveBootstrap();
		if( kw.isNull(promise) )
		{
			console.error("kwAppBS::() promise is invalid.");
			return;
		}

		promise.then(function(response)
		{
			console.log("kwAppBS::loadBootstrap() called.");

			if( kw.isNull(response) )
			{
				console.error("kwAppBS::loadBootstrap() bootstrap is invalid.");
			}
			//console.info("document::ready::then() response is ", response);

			this.retrieve(response);
		})
	}

	buildURL()
	{
		console.log("kwAppBS::buildURL() called.");

		if( !kw.isString(this.sMode) )
		{
			console.error("kwAppBS::buildURL() sMode is invalid.");
			return;
		}
		//console.info("buildURL() sMode is ", sMode);

		if( kw.isNull(this.bootstrapApi) )
		{
			console.error("kwAppBS::buildURL() bootstrapApi is bootstrapApi.");
			return;
		}
		//console.info("buildURL() bootstrapApi is ", bootstrapApi);

		if( kw.isNull(this.hosts) )
		{
			console.error("kwAppBS::buildURL() hosts is hosts.");
			return;
		}
		//console.info("buildURL() hosts is ", hosts);

		let sService = this.bootstrapApi.service;
		if( !kw.isString(sService) )
		{
			console.error("kwAppBS::buildURL() sService is invalid.");
			return;
		}
		//console.info("buildURL() sService is ", sService);

		let sHost = this.hosts[ sService ];
		if( !kw.isString(sHost) )
		{
			console.error("kwAppBS::buildURL() sHost is invalid.");
			return;
			sHost = "";
		}
		console.info("buildURL() sHost is ", sHost);

		let sPath = this.bootstrapApi.path;
		if( !kw.isString(sPath) )
		{
			console.error("kwAppBS::buildURL() sPath is invalid.");
			return;
		}
		//console.info("buildURL() sPath is ", sPath);
		this.sPath = sPath;

		this.sURLBootstrap = sHost + "/" + sPath;

		console.info("kwAppBS::buildURL() sURLBootstrap is ", this.sURLBootstrap);
	}

	redirect()
	{
		console.log("kwAppBS::redirect() called.");

		if( !kw.isString(this.sRedirect) )
		{
			console.error("kwAppBS::redirect() sRedirect is invalid.");
			return;
		}
		//console.info("redirect() sRedirect is ", sRedirect);

		let sMsg = "Domain is not registerd with AirVM. "
			+ "Redirecting to "
			+ sRedirect
			+ "."

		//console.info("kwAppBS::redirect() sMsg is [" + sMsg + "].");
		//alert(sMsg);

		//window.location.href = sRedirect;
	}

	retrieve(response)
	{
		console.log("kwAppBS::retrieve() called.");

		if( kw.isNull(response) )
		{
			console.error("kwAppBS::retrieve() response is invalid.");
		}
		//console.info("retrieve() response is ", response);

		let bootstrap = response.data;
		if( kw.isNull(bootstrap) )
		{
			console.error("kwAppBS::retrieve() bootstrap is invalid.");
		}
		console.info("kwAppBS::retrieve() bootstrap is ", bootstrap);
		this.bootstrap = bootstrap;

		this.retrieveCredentials();
		this.retrieveMode();
		this.retrieveRedirect();
		this.retrieveServices();
		this.retrieveHosts();
		this.retrieveBootstrapApi();
		this.buildURL();

		this.promise = this.retrieveCustomInfo();
	}

	retrieveBootstrap()
	{
		console.log("kwAppBS::retrieveBootstrap() called.");

		if( !kw.isString(sMode) )
		{
			console.error("kwAppBS::retrieveBootstrap() sMode is invalid.");
			return;
		}
		//console.info("buildURL() sMode is ", sMode);
		console.info("kwAppBS::retrieveBootstrap() sURL is ", sURL);

		return $http.get(sURL);
	}

	retrieveBootstrapApi()
	{
		console.log("kwAppBS::retrieveBootstrapApi() called.");

		if( !kw.isString(sMode) )
		{
			console.error("kwAppBS::() sMode is invalid.");
			return;
		}
		//console.info("buildURL() sMode is ", sMode);

		if( kw.isNull(bootstrap) )
		{
			console.error("kwAppBS::retrieveBootstrapApi() bootstrap is invalid.");
			return;
		}
		//console.info("retrieveBootstrapApi() bootstrap is ", bootstrap);

		let apis = bootstrap.apis;
		if( kw.isNull(apis) )
		{
			console.error("kwAppBS::retrieveBootstrapApi() apis is invalid.");
			return;
		}
		//console.info("retrieveHosts() apis is ", apis);

		let bs = apis.bootstrap;
		if( kw.isNull(bs) )
		{
			console.error("kwAppBS::retrieveBootstrapApi() bs is invalid.");
			return;
		}
		//console.info("retrieveHosts() bs is ", bs);

		bootstrapApi = bs[ sMode ];
		if( kw.isNull(bootstrapApi) )
		{
			console.error("kwAppBS::retrieveBootstrapApi() bootstrapApi is invalid.");
			return;
		}
		//console.info("retrieveHosts() bootstrapApi is ", bootstrapApi);

	}

	retrieveCredentials(sName)
	{
		console.log("kwAppBS::retrieveCredentials() called.");

		if( kw.isNull(bootstrap) )
		{
			console.error("kwAppBS::retrieveHosts() bootstrap is invalid.");
			return;
		}
		//console.info("retrieveCredentials() bootstrap is ", services);

		credentials = bootstrap[ "credentials" ];
		if( kw.isNull(credentials) )
		{
			console.error("kwAppBS::retrieveHosts() credentials is invalid.");
			return;
		}
		//console.info("retrieveHosts() credentials is ", credentials);
	}

	retrieveHost(sName)
	{
		console.log("kwAppBS::retrieveHost() called.");

		if( !kw.isString(sName) )
		{
			console.error("kwAppBS::buildURL() sName is invalid.");
			return;
		}
		//console.info("buildURL() sName is ", sName);

		if( kw.isNull(services) )
		{
			console.error("kwAppBS::retrieveHosts() services is invalid.");
			return;
		}
		//console.info("retrieveHosts() services is ", services);

		let service = services[ sName ];
		if( kw.isNull(service) )
		{
			console.error("kwAppBS::retrieveHosts() service is invalid.");
			return;
		}
		//console.info("retrieveHosts() service is ", service);

		let sHost = service.host + ":" + service.port
		if( !kw.isString(sHost) )
		{
			console.error("kwAppBS::retrieveHosts() sHost is invalid.");
			return;
		}
		//console.info("retrieveHosts() sHost is ", sHost);

		return sHost;
	}

	retrieveHosts()
	{
		console.log("kwAppBS::retrieveHosts() called.");

		hosts = {};

		let host = undefined;

		host = retrieveHost("airSembly");
		if( !kw.isString(host) )
		{
			console.error("kwAppBS::retrieveHosts() hosts[airSembly] is invalid.");
			return;
		}
		hosts[ "airSembly" ] = host;
		//console.info("retrieveHosts() hosts[airSembly] is ", host["airSembly"]);

		host = retrieveHost("serviceProvider");
		if( !kw.isString(host) )
		{
			console.error("kwAppBS::retrieveHosts() hosts[serviceProvider] is invalid.");
			return;
		}
		hosts[ "serviceProvider" ] = host;
		//console.info("retrieveHosts() hosts[serviceProvider] is ", hosts["serviceProvider"]);

		host = retrieveHost("bootstrap");
		if( !kw.isString(host) )
		{
			console.error("kwAppBS::retrieveHosts() hosts[bootstrap] is invalid.");
			return;
		}
		hosts[ "bootstrap" ] = host;
		//console.info("retrieveHosts() hosts["bootstrap"] is ", hosts["bootstrap"]);

		host = retrieveHost("accountManagement");
		if( !kw.isString(host) )
		{
			console.error("kwAppBS::retrieveHosts() hosts[accountManagement] is invalid.");
			return;
		}
		hosts[ "accountManagement" ] = host;
		//console.info("retrieveHosts() host[accountManagement] is ", host["accountManagement"]);
	}

	retrieveMode()
	{
		console.log("kwAppBS::retrieveMode() called.");

		if( kw.isNull(bootstrap) )
		{
			console.error("kwAppBS::retrieveMode() bootstrap is invalid.");
			return;
		}
		//console.info("retrieveMode() bootstrap is ", bootstrap);

		sMode = bootstrap.mode;
		if( !kw.isString(sMode) )
		{
			console.error("kwAppBS::retrieveMode() sMode is invalid.");
			return;
		}
		//console.info("retrieveMode() sMode is ", sMode);

	}

	retrievePath()
	{
		console.log("kwAppBS::retrievePath() called.");

		if( kw.isNull(bootstrap) )
		{
			console.error("kwAppBS::retrievePath() bootstrap is invalid.");
			return;
		}
		//console.info("retrievePath() bootstrap is ", bootstrap);

		if( !kw.isString(sMode) )
		{
			console.error("kwAppBS::retrievePath() sMode is invalid.");
			return;
		}
		//console.info("retrievePath() sMode is ", sMode);

		let apis = bootstrap.apis;
		if( kw.isNull(apis) )
		{
			console.error("kwAppBS::retrievePath() apis is invalid.");
			return;
		}
		//console.info("retrievePath() apis is ", apis);

		let bootstrapApi = apis[ "bootstrap" ];
		if( kw.isNull(bootstrapApi) )
		{
			console.error("kwAppBS::retrievePath() bootstrapApi is invalid.");
			return;
		}
		//console.info("retrievePath() bootstrapApi is ", bootstrapApi);

		let api = bootstrapApi[ sMode ];
		if( kw.isNull(api) )
		{
			console.error("kwAppBS::retrievePath() api is invalid.");
			return;
		}
		//console.info("retrievePath() api is ", api);

		sPath = api.path;
		if( !kw.isString(sPath) )
		{
			console.error("kwAppBS::retrievePath() sPath is invalid.");
			return;
		}
		//console.info("retrievePath() sPath is ", sPath);

		sService = api.service;
		if( !kw.isString(sService) )
		{
			console.error("kwAppBS::retrievePath() sService is invalid.");
			return;
		}
		//console.info("retrievePath() sService is ", sService);

		sToken = api.token;
		if( !kw.isString(sToken) )
		{
			console.error("kwAppBS::retrievePath() sToken is invalid.");
			return;
		}
		//console.info("retrievePath() sToken is ", sToken);

	}

	retrieveRedirect()
	{
		console.log("kwAppBS::retrieveRedirect() called.");

		if( kw.isNull(bootstrap) )
		{
			console.error("kwAppBS::retrieveRedirect() bootstrap is invalid.");
			return;
		}
		//console.info("retrieveRedirect() bootstrap is ", bootstrap);

		sRedirect = bootstrap.redirect;
		if( !kw.isString(sRedirect) )
		{
			console.error("kwAppBS::retrieveRedirect() sRedirect is invalid.");
			return;
		}
		//console.info("retrieveRedirect() sRedirect is ", sRedirect);

	}

	retrieveCustomInfo()
	{
		console.log("kwAppBS::retrieveCustomInfo() called.");

		if( this.sMode === "debug" )
		{
			console.info("kwAppBS::retrieveCustomInfo() running debug - not retrieving custom.");
			startAngular();
			return;
		}

		if( !kw.isString(this.sURLBootstrap) )
		{
			console.error("kwAppBS::retrieveCustomInfo() sURLBootstrap is invalid.");
			return;
		}
		console.info("kwAppBS::retrieveCustomInfo() sURLBootstrap is ", this.sURLBootstrap);

		this.$http.get(sURLBootstrap).subscribe(data =>
		{
			//console.info("kwHttpSrvc::get()::next() called");
			//console.info("kwHttpSrvc::get()::next() data is [", data, "]");

			let info = data.json();
			if( !info )
			{
				console.error("kwAppBS::::get() info is invalid.");
				return;
			}

			if( info.length != 1 )
			{
				console.error("kwAppBS::::get() info is invalid.");
				return;
			}
			console.info("kwAppBS::::get() info is [", info, "]");


			store.change(info[ 0 ]);

		}, err =>
		{
			console.info("kwAppBS::::get()::err() called");
			console.info("kwAppBS::::get()::err() err is [", err, "]");

			//this.srvcErr.change(err);

		});
	}

	retrieveServices()
	{
		console.log("kwAppBS::retrieveServices() called.");

		if( kw.isNull(this.bootstrap) )
		{
			console.error("kwAppBS::retrieveService() bootstrap is invalid.");
			return;
		}
		console.info("kwAppBS::retrieveServices() bootstrap is ", this.bootstrap);

		let services = this.bootstrap.services;
		if( kw.isNull(services) )
		{
			console.error("kwAppBS::retrieveService() services is invalid.");
			return;
		}
		console.info("kwAppBS::retrieveServices() services is ", services);
		this.services = services;
	}

	startAngular(custom)
	{
		console.log("kwAppBS::startAngular() called.");

		if( kw.isNull(this.credentials) )
		{
			console.error("kwAppBS::startAngular() credentials is invalid.");
			return;
		}
		console.info("kwAppBS::startAngular() credentials is ", this.credentials);

		angular.module('fuse').constant('cnstCredentials', credentials);

		if( kw.isNull(custom) )
		{
			custom = {};
		}
		console.info("kwAppBS::startAngular() custom is ", custom);
		angular.module('fuse').constant('cnstCustom', custom);

		if( kw.isNull(this.hosts) )
		{
			console.error("kwAppBS::startAngular() hosts is invalid.");
			return;
		}
		console.info("kwAppBS::startAngular() hosts is ", this.hosts);
		angular.module('fuse').constant('cnstHosts', this.hosts);

		if( !kw.isString(this.sMode) )
		{
			console.error("kwAppBS::startAngular() sMode is invalid.");
			return;
		}
		//console.info("startAngular() sMode is ", this.sMode);
		angular.module('fuse').constant('cnstMode', this.sMode);

		angular.bootstrap(document, [ 'fuse' ]);
	}

}

