/**********************************************************************
 *
 * kw/state/errBody/kwErrData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }			 from '@angular/core';
import { Injectable }			 from '@angular/core';

import { kwErr }                from "../../class/err/kwErr";
import { kwErrBodyType }		 from './kwErrBodyType';
// @formatter:on


@Injectable()
export class kwErrData
{

	private broadcast: EventEmitter<kwErr>;

	data: kwErrBodyType;

	changed$: EventEmitter<kwErrBodyType>;

	constructor()
	{
		//console.log("kwErrData::constructor() called.");

		this.broadcast = new EventEmitter<kwErrBodyType>();
		this.changed$ = this.broadcast;
	}

	change(data: kwErrBodyType)
	{
		//console.log("kwErrData::change() called.");

		if( !kwErrBodySrvc.isType(data))
		{
			console.errBodyor("kwErrData::change() data is invalid.");
			return
		}
		//console.errBodyor("kwErrData::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("kwErrData::clear() called.");

		this.data = null;
	};

	isValid(data: kwErrBodyType)
	{
		//console.log("kwErrData::isValid() called.");

		return true;
	};

	retrieve(): kwErrBodyType
	{
		//console.log("kwErrData::retrieve() called.");

		return this.data;
	};


}
