/**********************************************************************
 *
 * kw/state/app/kwAppState.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
import { Component }				from '@angular/core';

import { kwAppData }                from './kwAppData';
// @formatter:on


@Component({
	selector: 'kw-app-state',
	template: ``,
})
export class kwAppState
{
	constructor(private srvc: kwAppData)
	{
		//console.log("kwAppState::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwAppState::ngOnInit() called");
	};

	ngOnDestroy()
	{
		//console.log("kwAppState::ngOnDestroy() called.");
	}

}
