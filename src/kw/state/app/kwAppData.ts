/**********************************************************************
 *
 * kw/state/app/kwAppData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/
// @formatter:off
import { EventEmitter }			from '@angular/core';
import { Injectable }           from '@angular/core';

import { kwApp }			    from '../../class/app/kwApp';
// @formatter:on



@Injectable()
export class kwAppData
{

	private broadcast: EventEmitter<kwApp>;

	data: kwApp;

	changed$: EventEmitter<kwApp>;

	constructor()
	{
		//console.log("kwAppData::constructor() called.");
		this.broadcast = new EventEmitter<kwApp>();
		this.changed$ = this.broadcast;
	}

	change(data: kwApp)
	{
		//console.log("kwAppData::change() called.");

		if( !kwApp.is(data) )
		{
			console.error("kwAppData::change() data is invalid.");
			return
		}
		//console.info("kwAppData::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("kwAppData::clear() called.");
		this.data = null;
	};

	retrieve(): kwApp
	{
		//console.log("kwAppData::retrieve() called.");
		return this.data;
	};

}
