/**********************************************************************
 *
 * kw/http/kwApiKey.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import {Component}					from '@angular/core';
import {OnInit}						from '@angular/core';

import { kw }		                from "../../kw";
import { kwKeySrvc }				from "../kwKeySrvc";
import { kwApiKeyData }			    from './kwApiKeyData';
// @formatter:on


const sKEY = "api";


@Component({
	selector: 'kw-api-key',
	template: ``,
})
export class kwApiKey implements OnInit
{

	constructor(private store: kwApiKeyData)
	{

		//console.log("kwApiKey::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwApiKey::ngOnInit() called");

		this.load();
	};

	load(): void
	{
		//console.log("kwApiKey::loadId() called.");

		let sApi: string = kwKeySrvc.retrieve(sKEY, this.store);
		if( !kw.isString(sApi))
		{
			console.error("kwApiKey::load() [", sKEY, "] is not present");
			return;
		}
		//console.info("kwApiKey::load() [", sKEY, "] is [", sApi, "]");
	}


}
