/**********************************************************************
 *
 * kw/key/api/kwApiKeyData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }             from '@angular/core';
import { Injectable }               from '@angular/core';

import { kw }		                from "../../kw";
// @formatter:on


@Injectable()
export class kwApiKeyData
{

	// Observable api sources
	broadcast: EventEmitter<string>;

	// Observable string streams
	changed$: EventEmitter<string>;

	data: string;

	constructor()
	{

		//console.log("kwApiKeyData::constructor() called.");

		this.broadcast = new EventEmitter<string>();
		this.changed$ = this.broadcast;
	}

	change(data: string): void
	{

		//console.log("kwApiKeyData::change() called.");

		if( !kw.isString(data) )
		{
			console.error("kwApiKeyData::change() data is invalid.");
			return
		}
		//console.info("kwApiKeyData::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("kwApiKeyData::clear() called.");
		this.data = null
	};

	retrieve(): string
	{
		//console.log("kwApiKeyData::retrieve() called.");
		return this.data;
	};


}
