/**********************************************************************
 *
 * kw/http/kwHostKey.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import {Component}					from '@angular/core';
import {OnInit}						from '@angular/core';

import { kw }		                from "../../kw";
import { kwKeySrvc }				from "../kwKeySrvc";
import { kwHostKeyData }			from './kwHostKeyData';
// @formatter:on


const sKEY = "host";


@Component({
	selector: 'kw-host-key',
	template: ``,
})
export class kwHostKey implements OnInit
{

	constructor(private store: kwHostKeyData)
	{

		//console.log("kwHostKey::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwHostKey::ngOnInit() called");

		this.load();
	};

	load(): void
	{
		//console.log("kwHostKey::loadId() called.");

		let sHost: string = kwKeySrvc.retrieve(sKEY, this.store);
		if( !kw.isString(sHost))
		{
			console.error("kwHostKey::load() [", sKEY, "] is not present");
			return;
		}
		//console.info("kwHostKey::load() [", sKEY, "] is [", sHost, "]");
	}


}
