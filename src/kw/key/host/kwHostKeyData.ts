/**********************************************************************
 *
 * kw/key/host/kwHostKeyData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }             from '@angular/core';
import { Injectable }               from '@angular/core';

import { kw }		                from "../../kw";
// @formatter:on


@Injectable()
export class kwHostKeyData
{

	// Observable host sources
	broadcast: EventEmitter<string>;

	// Observable string streams
	changed$: EventEmitter<string>;

	data: string;

	constructor()
	{

		//console.log("kwHostKeyData::constructor() called.");

		this.broadcast = new EventEmitter<string>();
		this.changed$ = this.broadcast;
	}

	change(data: string): void
	{

		//console.log("kwHostKeyData::change() called.");

		if( !kw.isString(data) )
		{
			console.error("kwHostKeyData::change() data is invalid.");
			return
		}
		//console.info("kwHostKeyData::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("kwHostKeyData::clear() called.");
		this.data = null
	};

	retrieve(): string
	{
		//console.log("kwHostKeyData::retrieve() called.");

		return this.data;
	};


}
