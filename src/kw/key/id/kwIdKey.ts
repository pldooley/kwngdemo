// @formatter:off
import { Component }						from '@angular/core';
import { OnInit }							from '@angular/core';

import { kw }                               from "../../kw";
import { kwKeySrvc }						from "../kwKeySrvc";
import { kwIdKeyData }						from './kwIdKeyData';
// @formatter:on


const sKEY = "id";

/**********************************************************************
 *
 * kw/http/kwHostKey.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/


@Component({
	selector: 'kw-id-key',
	template: ``,
})
export class kwIdKey implements OnInit
{

	constructor(private store: kwIdKeyData)
	{

		//console.log("kwIdKey::constructor() called");
	}

	ngOnInit(): void
	{
		//console.log("kwIdKey::ngOnInit() called");

		this.load();
	};

	load(): void
	{
		//console.log("kwIdKey::load() called.");

		let nId: number = kwKeySrvc.retrieve(sKEY, this.store);
		if( !kw.isNumber(nId ))
		{
			console.error("kwIdKey::load() [", sKEY, "] is not present");
			return;
		}
		//console.info("kwIdKey::load() [", sKEY, "] is [", nId, "]");
	}

}
