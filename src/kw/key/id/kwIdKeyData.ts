/**********************************************************************
 *
 * kw/key/id/kwIdKeyData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter } from '@angular/core';
import { Injectable } from '@angular/core';
// @formatter:on


@Injectable()
export class kwIdKeyData
{

	private broadcast: EventEmitter<number>;

	data: number;

	changed$: EventEmitter<number>;

	constructor()
	{
		//console.log("kwIdKeyData::constructor() called.");

		this.broadcast = new EventEmitter<number>();
		this.changed$ = this.broadcast;
	}

	change(data: number)
	{
		//console.log("kwIdKeyData::change() called.");

		if( !data )
		{
			console.error("kwIdKeyData::change() data is invalid.");
			return
		}
		//console.info( "kwIdKeyData::change() data is [", data, "]" );

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("kwIdKeyData::clear() called.");

		this.data = null;
	};

	isValid(data: number): boolean
	{
		//console.log("kwIdKeyData::isValid() called.");

		return true;
	};

	retrieve(): number
	{
		//console.log("kwIdKeyData::retrieve() called.");

		return this.data;
	};


}
