/**********************************************************************
 *
 * kw/key/kwKeySrvc.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }                       from "../kw";
// @formatter:on


export class kwKeySrvc
{

	static retrieve(sKey: string, srvc: object): any
	{

		//console.log("kwKeySrvc::change() called.");

		if( !kw.isString(sKey))
		{
			console.error("kwKeySrvc::retrieve() sKey is invalid.");
			return
		}
		//console.info("kwKeySrvc::change() data is [", data, "]");

		if( kw.isNull(srvc))
		{
			console.error("kwKeySrvc::retrieve() srvc is invalid.");
			return
		}

		let val: any = window[ sKey ];
		if( !val )
		{
			console.error("kwKeySrvc::retrieve() ", sKey, " is not present");
			return;
		}
		//console.info( "kwKeySrvc::retrieve() ", sKey, " is [", val, "]." );

		srvc.change(val);

		return val;
	}


}
