/**********************************************************************
 *
 * kw/key/port/kwPortKeyData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }         from '@angular/core';
import { Injectable }           from '@angular/core';

import { kw }                   from "../../kw";
// @formatter:on


@Injectable()
export class kwPortKeyData
{

	// Observable port sources
	broadcast: EventEmitter<number>;

	// Observable string streams
	changed$: EventEmitter<number>;

	data: number;

	constructor()
	{

		//console.log("kwPortKeyData::constructor() called.");

		this.broadcast = new EventEmitter<number>();
		this.changed$ = this.broadcast;
	}

	change(data: number): void
	{

		//console.log("kwPortKeyData::change() called.");

		if( !kw.isNumber(data) )
		{
			console.error("kwPortKeyData::change() data is invalid.");
			return
		}
		//console.info("kwPortKeyData::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("kwPortKeyData::clear() called.");
		this.data = null
	};

	retrieve(): number
	{
		//console.log("kwPortKeyData::retrieve() called.");

		return this.data;
	};


}
