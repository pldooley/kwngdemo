/**********************************************************************
 *
 * kw/key/port/kwPortKey.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import {Component}					from '@angular/core';
import {OnInit}						from '@angular/core';

import { kw }                       from "../../kw";
import {kwKeySrvc}					from "../kwKeySrvc";
import {kwPortKeyData}				from './kwPortKeyData';
// @formatter:on


const sKEY = "port";


@Component({
	selector: 'kw-port-key',
	template: ``,
})
export class kwPortKey implements OnInit
{

	constructor(private store: kwPortKeyData)
	{
		//console.log("kwPortKey::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwPortKey::ngOnInit() called");
		this.load();
	};

	load(): void
	{
		//console.log("kwPortKey::loadId() called.");

		let nPort: number = kwKeySrvc.retrieve(sKEY, this.store);
		if( !kw.isNumber(nPort))
		{
			console.error("kwPortKey::load() [", sKEY, "] is not present");
			return;
		}
		//console.info("kwPortKey::load() [", sKEY, "] is [", nPort, "]");
	}

}
