/**********************************************************************
 *
 * kw/enum/kwVarEnum.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

export enum kwVarEnum
{
	Arr		=  0,
	Bool,
	Curr,
	CurrCode,
	Lang,
	LangCode,
	Nulld,
	Num,
	Obj,
	Str,
	Time,
	TZ,
	TZCode,
}
