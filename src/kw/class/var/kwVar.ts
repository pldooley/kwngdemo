
/**********************************************************************
 *
 * kw/class/kwVar.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }			        from "../../kw";
import { kwVarEnum }		    from "./kwVarEnum";
import { kwVarSrvc }		    from "./kwVarSrvc";
// @formatter:on

export class kwVar
{

	constructor( private nType: kwVarEnum )
	{
		console.log("kwVar::constructor() is called.");
	}

	init(): boolean
	{
		//console.log("kwToken::init() called.");

		if (!kwVarSrvc.in(this.nType))
		{
			console.error("kwToken::init() nType is invalid.");
			return false;
		}
		console.info("kwVar::init() nType is [", this.nType, "]");

		return true;
	};

	isArr(): boolean
	{

		return (this.nType === kwVarEnum.Arr);
	}

	isBool(): boolean
	{

		return (this.nType === kwVarEnum.Bool);
	}

	isCur(): boolean
	{

		return (this.nType === kwVarEnum.Cur);
	}

	isCurCode(): boolean
	{

		return (this.nType === kwVarEnum.CurCode);
	}

	islang(): boolean
	{

		return (this.nType === kwVarEnum.Lang);
	}

	islangCode(): boolean
	{

		return (this.nType === kwVarEnum.LangCode);
	}

	isNulld(): boolean
	{

		return (this.nType === kwVarEnum.Nulld);
	}

	isNum(): boolean
	{

		return (this.nType === kwVarEnum.Num);
	}

	isObj(): boolean
	{

		return (this.nType === kwVarEnum.Obj);
	}

	isStr(): boolean
	{

		return (this.nType === kwVarEnum.Str);
	}

	isTime(): boolean
	{

		return (this.nType === kwVarEnum.Time);
	}

	isTZ(): boolean
	{

		return (this.nType === kwVarEnum.TZ);
	}

	isTZCode(): boolean
	{

		return (this.nType === kwVarEnum.TZCode);
	}

	toString(): string
	{
		return kw.toString(this.nType, kwVarEnum);
	};

	static is(obj: object): boolean
	{
		return kw.is(obj, kwVar)
	}

}
