/**********************************************************************
 *
 * kw/class/api/kwApi.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }		                from "../../kw";
import { kwActs }			        from "../acts/kwActs";
import { kwActsSrvc }               from "../acts/kwActsSrvc";
import { kwActsType }               from "../acts/kwActsType";

import { kwApiSrvc }                from "./kwApiSrvc";
import { kwApiType }                from "./kwApiType";
// @formatter:on


export class kwApi
{
	acts: kwActs;
	sMode: string;

	constructor(private type: kwApiType)
	{
		console.log("kwApi::constructor() called");
	};

	init(): boolean
	{
		console.log("kwApi::init() is called.");

		if (!kwApiSrvc.isType(this.type))
		{
			console.error("kwApi::init() type is invalid.");
			return false;
		}
		console.info("kwApi::init() type is ", this.type);

		let sMode: string = this.type.sMode;
		if(!kw.isString(sMode))
		{
			console.error("kwApi::init() sMode is invalid.");
			return false;
		}
		this.sMode = sMode;

		let acts: kwActsType = this.type.acts;
		if( !kwActsSrvc.isType(this.acts) )
		{
			console.error("kwApi::init() acts is invalid.");
			return false;
		}

		this.acts = new kwActs(acts);
		if( !this.acts.init() )
		{
			console.error("kwApi::init() error creating acts.");
			return false;
		}

		return true;
	}

	getActs(): kwActs
	{
		return this.acts;
	};

	getMode(): string
	{
		return this.sMode;
	};

	toString(): string
	{
		return this.constructor.name;
	};

	static is(obj: object): boolean
	{
		return kw.is(obj, kwApi)
	}


}

