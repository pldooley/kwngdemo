/**********************************************************************
 *
 * kw/class/api/kwApiType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kwActsType }        from "../acts/kwActsType";
// @formatter:on


export class kwApiType
{
	sMode: string;
	acts: kwActsType;
}
