/**********************************************************************
 *
 * kw/class/srvc/kwAjaxType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
// @formatter:on


export class kwAjaxType
{
	sService: string;
	sTemplate: string;
	sToken: string;
}
