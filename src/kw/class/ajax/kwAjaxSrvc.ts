/**********************************************************************
 *
 * kw/class/srvc/kwAjaxSrvc.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }		            from "../../kw";

import { kwAjaxType }			from "./kwAjaxType";
// @formatter:on


export class kwAjaxSrvc
{

	static isType(obj: object): boolean
	{
		return kw.is(obj, kwAjaxType)
	}

	static in(nVal: number): boolean
	{
		return false;
	}

	static toEnum(sVal: string): number
	{
		return -1;
	};
}

