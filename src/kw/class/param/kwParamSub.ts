/**********************************************************************
 *
 * kw/class/kwParamSub.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }		                from "../../kw";
import { kwXField }		            from "../xField/kwXField";
import { kwXFieldSrvc }             from "../xField/kwXFieldSrvc";
import { kwVarEnum }                from "../var/kwVarEnum";

import { kwParam }		            from "./kwParam";
import { kwParamEnum }		        from "./kwParamEnum";
import { kwParamType }		        from "./kwParamType";
import { kwParamSrvc }              from "./kwParamSrvc";
// @formatter:on

export class kwParamSub extends kwParam
{

	constructor(type: kwParamType)
	{
		super(kwParamEnum.Sub, type);
		console.log("kwParamSub::constructor() is called.");
	}

	loadSrvc(nType: kwParamEnum): kwXField
	{
		//console.log("classParamSub::loadSrvc() called.");

		if(!kwParamSrvc.in(nType))
		{
			console.error("classParamSub::loadSrvc() nType is not valid.");
			return;
		}
		//console.info("classParamSub::loadSrvc() nType is ", nType);

		let srvc: kwXField = kwXFieldSrvc.createSub(nType);
		if( !(!kwXField.is(srvc)) )
		{
			console.error("kwParamSub::loadSrvc() srvc is not created for sType ", this.toString());
			return;
		}
		console.info("kwParamSub::loadSrvc() srvc is ", srvc);

		return srvc;
	}

	processDefault(nVar: kwVarEnum): boolean
	{
		//console.log("classParamSub::processDefault() called.");
		return true;
	}

}