/**********************************************************************
*
* kw/class/kwParamFull.ts
*
* author: Patrick Dooley
*
*
**********************************************************************
*
* Copyright (c) 2017 iTKunst Corporation
*
**********************************************************************/

// @formatter:off
import { kw }		                from "../../kw";
import { kwCurrData }               from "../../main/curr/kwCurrData";
import { kwLangData }               from "../../main/lang/kwLangData";
import { kwTZData }                 from "../../main/TZ/kwTZData";
import { kwVarEnum }		        from "../var/kwVarEnum";
import { kwXField }                 from "../xField/kwXField";
import { kwXFieldSrvc }             from "../xField/kwXFieldSrvc";

import { kwParam }		            from "./kwParam";
import { kwParamEnum }		        from "./kwParamEnum";
import { kwParamType }		        from "./kwParamType";
import { kwParamSrvc }              from "./kwParamSrvc"; import { kwVarSrvc } from "../var/kwVarSrvc";
// @formatter:on

export class kwParamFull extends kwParam
{

	constructor(type: kwParamType)
	{
		super(kwParamEnum.Full, type);
		console.log("kwParamFull::constructor() is called.");
	}

	loadSrvc(nType: kwParamEnum): kwXField
	{
		//console.log("kwParamFull::loadSrvc() called.");

		if (!kwParamSrvc.in(nType))
		{
			console.error("kwParamFull::loadSrvc() nType is not valid.");
			return;
		}
		//console.info("kwParamFull::loadSrvc() nType is ", nType);

		let srvc: kwXField = kwXFieldSrvc.createFull(nType);
		if (!kwXField.is(srvc))
		{
			console.error("kwParamFull::loadSrvc() srvc is not created for sType ", this.toString());
			return;
		}
		//console.info("kwParamFull::loadSrvc() srvc is ", srvc);

		return srvc;
	}

	processDefault(nVar: kwVarEnum): boolean
	{
		//console.log("kwParamFull::processDefault() called.");

		if (!kwVarSrvc.in(nVar))
		{
			//console.info("kwParamFull::processDefault() no default value.");
			return false;
		}

		if (typeof this.default != "string")
		{
			//console.info("kwParamFull::processDefault() no default value.");
			return true;
		}

		switch (nVar)
		{
			case kwVarEnum.Curr:
			{
				let obj = kwCurrData.getByCode(this.default);
				if (!kw.isNull(obj))
				{
					console.error("kwParamFull::processDefault() obj is not valid.");
					return false;
				}
				//console.info("kwParamFull::processDefault() obj is ", obj);

				this.setVarDefault(obj);
				break;
			}

			case kwVarEnum.LANG:
			{
				let obj = kwLangData.getByCode(this.default);
				if (!kw.isNull(obj))
				{
					console.error("kwParamFull::processDefault() obj is not valid.");
					return false;
				}
				//console.info("kwParamFull::processDefault() obj is ", obj);

				this.setDefault(obj);
				break;
			}

			case kwVarEnum.TZ:
			{
				let obj = kwTZData.getByCode(this.default);
				if (!kw.isNull(obj))
				{
					console.error("kwParamFull::processDefault() obj is not valid.");
					return false;
				}
				//console.info("kwParamFull::processDefault() obj is ", obj);

				this.setDefault(obj);
				break;
			}

			default:
			{

			}
		}

		return true;
	}

}