
/**********************************************************************
 *
 * kw/class/lang/kwLang.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }		         from "../../kw";

import { kwLangSrvc }        from "./kwLangSrvc";
import { kwLangType }		 from "./kwLangType";
// @formatter:on

export class kwLang
{
	nId: number;
	sCode: string;

	constructor(private type: kwLangType)
	{
		console.log("kwLang::constructor() is called.");
	}

	init(): boolean
	{
		//console.log("kwLang::init() called.");

		if( !kwLangSrvc.isType(this.type) )
		{
			console.error("kwLang::init() type is not valid.");
			return false;
		}
		console.info("kwLang::init() type is ", this.type);

		let nId: number = this.type.nId;
		if( !kw.isNumber(nId) )
		{
			console.error("kwLang::init() nId is not valid.");
			return false;
		}
		console.info("kwLang::init() nId is ", nId);
		this.nId = nId;

		let sCode: string = this.type.sCode;
		if( !kw.isString(sCode) )
		{
			console.error("kwLang::init() sCode is not valid.");
			return false;
		}
		console.info("kwLang::init() sCode is ", sCode);
		this.sCode = sCode;

		return true;
	}

	getId()
	{
		return this.nId;
	}

	getCode()
	{
		return this.sCode;
	}

	toString(): string
	{
		return this.constructor.name;
	};

	static is(obj: object): boolean
	{
		return kw.is(obj, kwLang)
	}

}
