
/**********************************************************************
 *
 * kw/class/model/kwModelFull.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }		            from "../../kw";
import { kwParam }		        from "../param/kwParam";
import { kwParamFull }		    from "../param/kwParamFull";
import { kwParamSrvc }		    from "../param/kwParamSrvc";
import { kwParamType }          from "../param/kwParamType";

import { kwModel }		        from "./kwModel";
import { kwModelEnum }		    from "./kwModelEnum";
import { kwModelType }		    from "./kwModelType";
// @formatter:on

export class kwModelFull extends kwModel
{

	constructor(type: kwModelType)
	{
		super(kwModelEnum.Full, type);
		console.log("kwModelFull::constructor() is called.");
	}

	createParam(type: kwParamType): kwParam
	{
		//console.log("classModelFull::createParam() called.");
		if (!kwParamSrvc.isType(type))
		{
			console.error("classModelFull::createParam() type is invalid.");
			return;
		}
		console.info("classModelFull::createParam() type is [", type, "]");

		let param: kwParam = new kwParamFull(type);
		if (!param.init())
		{
			console.error("classModelFull::createParam() error creating param.");
			return;
		}

		return param;
	}

}