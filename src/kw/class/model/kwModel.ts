
/**********************************************************************
 *
 * kw/class/model/kwModel.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }		            from "../../kw";
import { kwParam }              from "../param/kwParam";
import { kwParamType }		    from "../param/kwParamType";

import { kwModelEnum }		    from "./kwModelEnum";
import { kwModelSrvc }		    from "./kwModelSrvc";
import { kwModelType }		    from "./kwModelType";
// @formatter:on

export abstract class kwModel
{

	params: kwParam[];

	constructor(    private nType: kwModelEnum,
	                private type: kwModelType   )
	{
		console.log("kwModel::constructor() is called.");
	}

	abstract createParam(param: kwParamType): kwParam

	init(): boolean
	{
		//console.log("kwModel::init() called.");

		if (!kwModelSrvc.in(this.nType))
		{
			console.error("kwModel::init() nType is not valid.");
			return false;
		}
		console.info("kwModel::init() nType is ", this.nType);

		if (!kwModelSrvc.isType(this.type))
		{
			console.error("kwModel::init() type is not valid.");
			return false;
		}
		console.info("kwModel::init() type is ", this.type);

		let params: kwParamType[] = this.type.params;
		if( kw.isNull(params))
		{
			console.error("kwModel::init() params is not valid.");
			return false;
		}
		console.info("kwModel::init() params is ", params);

		if (!this.createParams(params))
		{
			console.error("kwModel::init() error creating params.");
			return false;
		}

		return true;
	}

	createRecord()
	{
		//console.log("kwModel::createRecord() called.");
	
		if (!kw.isArray(this.params))
		{
			console.error("kwModel::createRecord() params is not valid.");
			return;
		}
		//console.info("kwModel::createRecord() params is ", this.params);
	
		let record: object;
	
		for (let i=0; i<this.params.length; i++)
		{
			let param = this.params[i];
			if (kw.isNull(param))
			{
				console.error("kwModel::createRecord() param is not valid.");
				return;
			}
			//console.info("kwModel::createRecord() param is ", param);
	
			param.addDefault(record);
		}
	
		//console.info("kwModel::createRecord() record is ", record);
	
		return record;
	}

	createParams(params): boolean
	{
		//console.log("kwModel::createParams() called.");
	
		if (!kw.isArray(params))
		{
			console.error("kwModel::createParams() params is not valid.");
			return;
		}
		//console.info("kwModel::createParams() params is ", params);

		for (let i=0; i<params.length; i++)
		{
			let param = params[i];
			if (kw.isNull(param))
			{
				console.error("kwModel::createParams() param is not valid.");
				return false;
			}
			//console.info("kwModel::createParams() param is ", param);
			//console.info("kwModel::createParams() params is ", this.params);
	
			let obj = this.createParam(param);
			if (kw.isNull(obj))
			{
				console.error("kwModel::createParams() obj is not valid.");
				return false;
			}
			//console.info("kwModel::createParams() obj is ", obj);
	
			this.params.push(obj);
		}
	
		console.info("kwModel::createParams() params is ", this.params);

		return true;
	}

	toString(): string
	{
		return kw.toString(this.nType, kwModelEnum);
	};

	static is(obj: object): boolean
	{
		return kw.is(obj, kwModel)
	}

}
