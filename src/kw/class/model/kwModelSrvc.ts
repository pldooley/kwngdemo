/**********************************************************************
 *
 * kw/class/api/kwModelSrvc.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }		            from "../../kw";

import { kwModelEnum }			from "./kwModelEnum";
import { kwModelType }			from "./kwModelType";
// @formatter:on


export class kwModelSrvc
{

	static isType(obj: object): boolean
	{
		return kw.is(obj, kwModelType)
	}

	static in(nVal: number): boolean
	{
		return kw.in(nVal, kwModelEnum)
	}

	static toEnum(sVal: string): number
	{
		return kw.toEnum(sVal, kwModelEnum);
	};

	static xExport(srvc, recs)
	{
		//console.log("kwModelSrvc::xExport() called.");

		if (kw.isNull(srvc))
		{
			console.error("kwModelSrvc::xExport() srvc is invalid.");
			return;
		}

		if (!kw.isArray(recs))
		{
			console.error("kwModelSrvc::xExport() recs is invalid.");
			return;
		}

		let recsX = srvc.xExport(recs);
		if (!kw.isArray(recsX))
		{
			console.error("kwModelSrvc::xExport() recsX is invalid.");
			return;
		}

		return recsX;
	};

	static xExportRec(srvc, record)
	{
		//console.log("kwModelSrvc::xExportRec() called.");

		if (kw.isNull(srvc))
		{
			console.error("kwModelSrvc::xExportRec() srvc is invalid.");
			return;
		}

		if (kw.isNull(record))
		{
			console.error("kwModelSrvc::xExportRec() record is invalid.");
			return;
		}

		let recX = srvc.xExportRec(record);
		if (kw.isNull(recX))
		{
			console.error("kwModelSrvc::xExportRec() recX is invalid.");
			return;
		}

		return recX;
	};

	static xImport(srvc, recs)
	{
		//console.log("kwModelSrvc::xImport() called.");

		if (kw.isNull(srvc))
		{
			console.error("kwModelSrvc::xImport() srvc is invalid.");
			return false;
		}

		if (!kw.isArray(recs))
		{
			console.error("kwModelSrvc::xImport() recs is invalid.");
			return false;
		}

		let recsX = srvc.xImport(recs);
		if (!kw.isArray(recsX))
		{
			console.error("kwModelSrvc::xImport() recsX is invalid.");
			return;
		}

		return recsX;
	};

	static xImportRec(srvc, rec)
	{
		//console.log("kwModelSrvc::xImportRec() called.");

		if (kw.isNull(srvc))
		{
			console.error("kwModelSrvc::xImportRec() srvc is invalid.");
			return false;
		}

		if (kw.isNull(rec))
		{
			console.error("kwModelSrvc::xImportRec() rec is invalid.");
			return false;
		}

		let recX = srvc.xImportRec(rec);
		if (kw.isNull(recX))
		{
			console.error("kwModelSrvc::xImportRec() recX is invalid.");
			return;
		}

		return recX;
	};


}

