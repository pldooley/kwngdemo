/**********************************************************************
 *
 * kw/class/model/kwModelType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kwParamType }         from "../param/kwParamType";
// @formatter:on


export class kwModelType
{
	sType: string;
	params: kwParamType[];
}
