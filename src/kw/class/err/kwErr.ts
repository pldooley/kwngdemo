/**********************************************************************
 *
 * kw/class/err/kwErr.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }                           from "../../kw";
import { kwAct }			            from "../act/kwAct";
import { kwActEnum }			        from "../act/kwActEnum";
import { kwErrrvc }			        from "../act/kwErrrvc";
import { kwActType }			        from "../act/kwActType";
import { kwActGet }                     from "../act/kwActGet";
import { kwActDel }                     from "../act/kwActDel";
import { kwActPost }                    from "../act/kwActPost";
import { kwActUpdate }                  from "../act/kwActUpdate";
import { kwActPatch }                   from "../act/kwActPatch";

import { kwErrSrvc }                   from "./kwErrSrvc";
import { kwErrType }			        from "./kwErrType";
// @formatter:on


export class kwErr
{

	body: kwErrBody;

	sStatusText: string;
	sUrl: string;

	nStatus: number;
	nType: number;

	bOk: boolean;

	constructor(private type: kwErrType)
	{
		console.log("kwErr::constructor() is called.");
	}

	init(): boolean
	{
		console.log("kwErr::init() is called.");

		if(!kwErrSrvc.isType(this.type))
		{
			console.error("kwErr::init() type is invalid.");
			return false;
		}

		let sStatusText: string = this.type.statusText;
		if (!kw.isString(sStatusText))
		{
			console.error("kwErr::init() sStatusText is invalid.");
			return false;
		}
		console.info("kwErr::init() sStatusText is [", sStatusText, "]");
		this.sStatusText = sStatusText;

		let sUrl: string = this.type.url;
		if (!kw.isString(sUrl))
		{
			console.error("kwErr::init() sUrl is invalid.");
			return false;
		}
		console.info("kwErr::init() sUrl is [", sUrl, "]");
		this.sUrl = sUrl;

		let nStatus: number = this.type.status;
		if (!kw.isNumber(nStatus))
		{
			console.error("kwErr::init() nStatus is invalid.");
			return false;
		}
		console.info("kwErr::init() nStatus is [", nStatus, "]");
		this.nStatus = nStatus;

		let nType: number = this.type.type;
		if (!kw.isNumber(nType))
		{
			console.error("kwErr::init() nType is invalid.");
			return false;
		}
		console.info("kwErr::init() nType is [", nType, "]");
		this.nType = nType;

		let bOk: boolean = this.type.ok;
		if (!kw.isBoolean(bOk))
		{
			console.error("kwErr::init() bOk is invalid.");
			return false;
		}
		console.info("kwErr::init() bOk is [", bOk, "]");
		this.bOk = bOk;

		if(!this.createBody())
		{
			console.error("kwErr::init() error creating body.");
			return false;
		}

		return true;
	}


	createBody(): boolean
	{
		console.log("kwErr::createBody() is called.");

		if(!kwErrSrvc.isType(this.type))
		{
			console.error("kwErr::createBody() type is invalid.");
			return false;
		}

		let type: kwErrBodyType = this.type._body;
		if (!kwErrBodySrvc.isType(type))
		{
			console.error("kwErr::createBody() type is invalid.");
			return false;
		}
		console.info("kwErr::createBody() type is [", type, "]");

		let body = new kwErrBody(type);
		if (!body.init())
		{
			console.error("kwErr::createBody() error creating body.");
			return false;
		}

		return true;
	}

	getBody(): string{
		return this.sUrl;
	}

	getUrl(): string{
		return this.sUrl;
	}

	getStatus(): number{
		return this.nStatus;
	}

	getType(): number{
		return this.nType;
	}

	isOk(): boolean{
		return this.bOk;
	}

	static is(obj: object): boolean
	{
		return kw.is(obj, kwErr)
	}


}

