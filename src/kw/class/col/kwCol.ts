/**********************************************************************
 *
 * kw/class/col/kwCol.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }		            from "../../kw";

import { kwColEnum }		    from "./kwColEnum";
import { kwColSrvc }		    from "./kwColSrvc";
// @formatter:on

export class kwCol
{

	constructor( private nType: kwColEnum )
	{
		console.log("kwCol::constructor() is called.");
	}

	init(): boolean
	{
		console.log("kwCol::init() is called.");

		if( !kwColSrvc.in(this.nType) )
		{
			console.error("kwCol::init() nType is invalid.");
			return false;
		}
		console.info("kwCol::init() nType is ", this.nType);

		return true;
	}

	isBool(): boolean
	{

		return (this.nType === kwColEnum.Bool);
	}

	isDel(): boolean
	{

		return (this.nType === kwColEnum.Del);
	}

	isEdit(): boolean
	{
		return (this.nType === kwColEnum.Edit);
	}

	isLang(): boolean
	{
		return (this.nType === kwColEnum.Lang);
	}

	isNum(): boolean
	{
		return (this.nType === kwColEnum.Num);
	}

	isStr(): boolean
	{
		return (this.nType === kwColEnum.Str);
	}

	isTZ(): boolean
	{
		return (this.nType === kwColEnum.TZ);
	}

	toString(): string
	{
		return kw.toString(this.nType, kwColEnum);
	};

	static is(obj: object): boolean
	{
		return kw.is(obj, kwCol)
	}

}
