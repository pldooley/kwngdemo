/**********************************************************************
 *
 * kw/class/col/kwColEnum.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

export enum kwColEnum
{
    Bool = 0,
    Del,
    Edit,
	Lang,
	Num,
	Str,
	TZ,
}