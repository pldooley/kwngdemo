/**********************************************************************
 *
 * kw/kw/kwXField.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }                       from "../../kw";
import { kwXFieldEnum }		        from "./kwXFieldEnum";
import { kwXFieldSrvc }		        from "./kwXFieldSrvc";
// @formatter:on

export abstract class kwXField
{

	constructor(private nType: kwXFieldEnum)
	{
		console.log("kwXField::constructor() is called.");
	}

	abstract transform(val: any, sName: string): any

	init(): boolean
	{
		console.log("kwAct::init() is called.");

		if( !kwXFieldSrvc.in(this.nType) )
		{
			console.error("kwAct::init() nType is invalid.");
			return false;
		}
		console.info("kwAct::init() nType is ", this.nType);
	}

	isArr(): boolean
	{
		return (this.nType === kwXFieldEnum.Arr);
	}

	isBool(): boolean
	{
		return (this.nType === kwXFieldEnum.Bool);
	}

	isCur(): boolean
	{
		return (this.nType === kwXFieldEnum.Cur);
	}

	islang(): boolean
	{
		return (this.nType === kwXFieldEnum.Lang);
	}

	isNum(): boolean
	{
		return (this.nType === kwXFieldEnum.Num);
	}

	isObj(): boolean
	{
		return (this.nType === kwXFieldEnum.Obj);
	}

	isStr(): boolean
	{
		return (this.nType === kwXFieldEnum.Str);
	}

	isTime(): boolean
	{
		return (this.nType === kwXFieldEnum.Time);
	}

	isTZ(): boolean
	{
		return (this.nType === kwXFieldEnum.TZ);
	}

	toString(): string
	{
		return kw.toString(this.nType, kwXFieldEnum);
	};

	static is(obj: object): boolean
	{
		return kw.is(obj, kwXField)
	}

}

