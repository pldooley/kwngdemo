/**********************************************************************
 *
 * kw/kw/kwXFieldSrvc.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }    		        from "../../kw";

import { kwXFieldArr }		    from "./kwXFieldArr";
import { kwXFieldBool }		    from "./kwXFieldBool";
import { kwXFieldCurr }		    from "./kwXFieldCurr";
import { kwXFieldEnum }		    from "./kwXFieldEnum";
import { kwXFieldLang }		    from "./kwXFieldLang";
import { kwXFieldNum }		    from "./kwXFieldNum";
import { kwXFieldObj }		    from "./kwXFieldObj";
import { kwXFieldStr }		    from "./kwXFieldStr";
import { kwXFieldTime }		    from "./kwXFieldTime";
import { kwXFieldTZ }		    from "./kwXFieldTZ";
// @formatter:on

export class kwXFieldSrvc
{

	static createFull(sType: string): Object
	{
		//console.log("kwXFieldSrvc::createFull() called.");

		if( !kw.isString(sType) )
		{
			console.error("kwXFieldSrvc::createFull() sType is undefined.");
			return;
		}
		//console.info("kwXFieldSrvc::createFull() sType is ", sType);

		let nType: kwXFieldEnum = kwXFieldSrvc.toEnum(sType);
		if(!kwXFieldSrvc.in(nType))
		{
			console.error("kwXFieldSrvc::createFull() nType is undefined.");
			return;
		}
		console.info("kwXFieldSrvc::create() nType is ", nType);

		let field: Object;

		switch( nType )
		{
			case kwXFieldEnum.Arr:
			{
				field = new kwXFieldArr();
				break;
			}

			case kwXFieldEnum.Bool:
			{
				field = new kwXFieldBool();
				break;
			}

			case kwXFieldEnum.Curr:
			{
				field = new kwXFieldCurr();
				break;
			}

			case kwXFieldEnum.Lang:
			{
				field = new kwXFieldLang();
				break;
			}

			case kwXFieldEnum.Num:
			{
				field = new kwXFieldNum();
				break;
			}

			case kwXFieldEnum.Obj:
			{
				field = new kwXFieldObj();
				break;
			}

			case kwXFieldEnum.Str:
			{
				field = new kwXFieldStr();
				break;
			}

			case kwXFieldEnum.Time:
			{
				field = new kwXFieldTime();
				break;
			}

			case kwXFieldEnum.TZ:
			{
				field = new kwXFieldTZ();
				break;
			}
		}

		if( kw.isNull(field) )
		{
			console.error("kwXFieldSrvc::createFull() field is not valid.");
			return;
		}
		//console.info("kwXFieldSrvc::createFull() field is ", field);

		return field;
	}

	static createSub(nType: kwXFieldEnum): Object
	{
		//console.log("kwXFieldSrvc::createSub() called.");

		if( !kwXFieldSrvc.in(nType) )
		{
			console.error("kwXFieldSrvc::createSub() nType is undefined.");
			return;
		}
		//console.info("kwXFieldSrvc::createSub() nType is ", nType);

		let xSrvc: Object;

		switch( nType )
		{
			case kwXFieldEnum.Arr:
			{
				xSrvc = new kwXFieldArr();
				break;
			}

			case kwXFieldEnum.Bool:
			{
				xSrvc = new kwXFieldBool();
				break;
			}

			case kwXFieldEnum.Num:
			{
				xSrvc = new kwXFieldNum();
				break;
			}

			case kwXFieldEnum.Str:
			{
				xSrvc = new kwXFieldStr();
				break;
			}

			case kwXFieldEnum.Time:
			{
				xSrvc = new kwXFieldTime();
				break;
			}
		}

		if( kw.isNull(xSrvc) )
		{
			console.error("kwXFieldSrvc::createSub() xSrvc is not valid.");
			return;
		}
		//console.info("kwXFieldSrvc::createSub() xSrvc is ", field);

		return xSrvc;
	}

	static isType(obj: object): boolean
	{
		return false;
	}

	static in(nVal: number): boolean
	{
		return kw.in(nVal, kwXFieldEnum);
	}

	static toEnum(sVal: string): number
	{
		return kw.toEnum(sVal, kwXFieldEnum);
	};

}

