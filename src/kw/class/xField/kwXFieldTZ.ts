/**********************************************************************
 *
 * kw/class/kwXFieldTZ.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }		                from "../../kw";
import { kwTZData }		            from "../../main/TZ/kwTZData";
import { kwXField }		            from "./kwXField";
import { kwXFieldEnum }		        from "./kwXFieldEnum";
// @formatter:on

export class kwXFieldTZ extends kwXField
{

	constructor()
	{
		super(kwXFieldEnum.TZ);
		console.log("kwXFieldTZ::constructor() is called.");
	}

	transform(val: any, sName: string): any
	{
		//console.log("kwXFieldTZ::transform() called.");

		if (!kw.isString(val))
		{
			console.error("kwXFieldTZ::transform() field [" + sName + "] is undefined.");
			return null;
		}

		let valNew: any = kwTZData.getByCode(sVal);
		if (kw.isNull(valNew))
		{
			console.error("kwXFieldTZ::transform() field [" + sName + "] is undefined.");
			return null;
		}

		return valNew;
	}
}

