/**********************************************************************
 *
 * kw/enum/kwXFieldEnum.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

export enum kwXFieldEnum
{
	Arr		=  0,
	Bool,
	Curr,
	Lang,
	Num,
	Obj,
	Str,
	Time,
	TZ,
}
