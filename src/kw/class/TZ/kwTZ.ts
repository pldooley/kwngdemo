
/**********************************************************************
 *
 * kw/class/TZ/kwTZ.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }		            from "../../kw";

import { kwTZSrvc }             from "./kwTZSrvc";
import { kwTZType }    		    from "./kwTZType";
// @formatter:on

export class kwTZ
{

	nId: number;
	sCode: string;

	constructor( private type: kwTZType )
	{
		console.log("kwTZ::constructor() is called.");
	}

	init(): boolean
	{
		//console.log("kwTZ::init() called.");

		if (!kwTZSrvc.isType(this.type))
		{
			console.error("kwTZ::init() type is invalid.");
			return false;
		}
		console.info("kwTZ::init() type is [", this.type, "]");

		let nId = this.type.nId;
		if (!kw.isNumber(nId))
		{
			console.error("kwTZ::init() nId is invalid");
			return false;
		}
		console.info("kwTZ::init() nId is [", nId, "]");
		this.nId = nId;

		let sCode = this.type.sCode;
		if (!kw.isString(sCode))
		{
			console.error("kwTZ::init() sCode is invalid");
			return false;
		}
		console.info("kwTZ::init() sCode is [", sCode, "]");
		this.sCode = sCode;

		return true;
	};

	getId(): number
	{
		return this.nId;
	}

	getCode(): string
	{
		return this.sCode;
	}
	
	toString(): string
	{
		return this.constructor.name;
	};

	static is(obj: object): boolean
	{
		return kw.is(obj, kwTZ)
	}

}
