/**********************************************************************
 *
 * kw/class/url/kwUrlType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
// @formatter:on


export class kwUrlType
{
	sDomain: string;
	sPath: string;
	sTemplate: string;
	sUrl: string;
}
