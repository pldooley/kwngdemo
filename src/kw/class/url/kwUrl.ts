/**********************************************************************
 *
 * kw/class/url/kwUrl.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }    		        from "../../kw";
import { kwActSrvc }            from "../act/kwActSrvc";
import { kwParams }             from "../params/kwParams";
import { kwActType }            from "../act/kwActType";
// @formatter:on

export class kwUrl
{

	sDomain: string;
	sPath: string;
	sTemplate: string;
	sUrl: string;

	constructor(    private type: kwActType,
	                private params              )
	{
		console.log("kwUrl::constructor() is called.");
	}

	init()
	{
		//console.log("kwUrl::init() called.");

		if( !(kwActSrvc.isType(this.type)) )
		{
			console.error("kwUrl::init() action is invalid");
			return false;
		}

		if( !kwParams.is(this.params) )
		{
			console.error("kwUrl::init() params is invalid");
			return false;
		}

		this.retrieveDomain(this.type);
		this.retrieveTemplate(this.type);
		this.createPath(this.params);
		this.create();
	};

	getUrl()
	{
		return this.sUrl;
	}

	create()
	{
		//console.log("kwUrl::create() called.");

		if( !kw.isString(this.sPath) )
		{
			console.error("kwUrl::create() sPath is invalid");
			return;
		}
		//console.info("kwUrl::create() sPath is ", this.sPath);

		let sUrl = undefined
		if( !kw.isString(this.sDomain) )
		{
			//console.info("kwUrl::create() sDomain is empty");

			sUrl = this.sPath;
		}
		else
		{
			sUrl = this.sDomain + "/" + this.sPath;
		}
		//console.info("kwUrl::create() sUrl is ", sUrl);

		this.setUrl(sUrl);
	}

	createPath(params)
	{
		//console.log("kwUrl::createPath() called.");

		if( !kw.isArray(params) )
		{
			console.error("kwUrl::createPath() params is invalid");
			return;
		}
		//console.info("kwUrl::createPath() params is ", params);

		if( !kw.isString(this.sTemplate) )
		{
			console.error("kwUrl::createPath() sTemplate is invalid");
			return;
		}
		//console.info("kwUrl::createPath() sTemplate is ", this.sTemplate);

		let nLength = params.length;
		if( length === 0 )
		{
			//console.info("kwUrl::createPath() params is empty");
		}

		let sPath = this.sTemplate;
		for( let i = 0; i < nLength; i++ )
		{
			let param = params[ i ];
			//console.info("kwUrl::createPath() param is ", param);

			sPath = sPath.replace('[' + i + ']', param);
		}

		//console.info("kwUrl::createPath() sPath is ", sPath);

		this.setPath(sPath);
	}

	retrieveDomain(action)
	{
		//console.log("kwUrl::retrieveDomain() called.");

		if( !kw.isNull(action) )
		{
			console.error("kwUrl::retrieveDomain() action is not valid.");
			return;
		}

		let sService = action.getService();
		if( !kw.isString(sService) )
		{
			//console.info("kwUrl::retrieveDomain() service is empty.");
			return;
		}

		let sDomain = cnstHosts[ sService ];
		if( !kw.isString(sDomain) )
		{
			console.error("kwUrl::retrieveService() sDomain is not valid.");
			return;
		}

		this.setDomain(sDomain);
	}
	;

	retrieveTemplate(action)
	{
		//console.log("kwUrl::Template() called.");

		let sTemplate = action.getTemplate();
		if( !kw.isString(sTemplate) )
		{
			console.error("kwUrl::init() sTemplate is invalid");
			return;
		}

		this.setTemplate(sTemplate);
	}

	toString(): string
	{
		return this.constructor.name;
	};

	static is(obj: object): boolean
	{
		return kw.is(obj, kwUrl)
	}

}
