
/**********************************************************************
 *
 * kw/class/data/kwData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }		                from "../../kw";

import { kwDataEnum }		        from "./kwDataEnum";
import { kwDataSrvc }		        from "./kwDataSrvc";
// @formatter:on

export class kwData
{

	constructor( private nType: kwDataEnum )
	{
		console.log("kwData::constructor() is called.");
	}

	init(): boolean
	{
		console.log("kwData::init() is called.");

		if( !kwDataSrvc.in(this.nType) )
		{
			console.error("kwCol::init() nType is invalid.");
			return false;
		}
		console.info("kwCol::init() nType is ", this.nType);

		return true;
	}

	isArray(): boolean
	{
		return (this.nType === kwDataEnum.Array);
	}

	isBoolean(): boolean
	{
		return (this.nType === kwDataEnum.Boolean);
	}

	isNull(): boolean
	{
		return (this.nType === kwDataEnum.Null);
	}

	isNumber(): boolean
	{
		return (this.nType === kwDataEnum.Number);
	}

	isString(): boolean
	{
		return (this.nType === kwDataEnum.String);
	}

	toString(): string
	{
		return kw.toString(this.nType, kwDataEnum);
	};

	static is(obj: object): boolean
	{
		return kw.is(obj, kwData)
	}

}
