/**********************************************************************
 *
 * kw/class/data/kwDataEnum.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

export enum kwDataEnum
{
	Array = 0,
	Null,
	Object,
	String,
	Number,
	Boolean,
}
