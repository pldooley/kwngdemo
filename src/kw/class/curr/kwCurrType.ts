/**********************************************************************
 *
 * kw/class/curr/kwCurrType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
// @formatter:on


export class kwCurrType
{
	nId: number;
	sCode: string;
	sName: string;
	sNamePlural: string;
}
