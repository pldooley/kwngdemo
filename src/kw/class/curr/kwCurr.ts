/**********************************************************************
 *
 * kw/class/curr/kwCurr.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }		         from "../../kw";

import { kwCurrSrvc }		 from "./kwCurrSrvc";
import { kwCurrType }		 from "./kwCurrType";
// @formatter:on

export class kwCurr
{
	nId: number;
	sCode: string;
	sName: string;
	sNamePlural: string;

	constructor(private type: kwCurrType)
	{
		console.log("kwCurr::constructor() is called.");
	}

	init(): boolean
	{
		console.log("kwCurr::init() is called.");

		if( !kwCurrSrvc.isType(this.type) )
		{
			console.error("kwCurr::init() type is invalid.");
			return false;
		}
		console.info("kwCurr::init() type is ", this.type);

		let nId: number = this.type.nId;
		if (!kw.isNumber(nId))
		{
			console.error("kwCurr::init() nId is invalid.");
			return false;
		}
		console.info("kwCurr::init() nId ", nId);
		this.nId = nId;

		let sCode: string = this.type.sCode;
		if (!kw.isString(sCode))
		{
			console.error("kwCurr::init() sCode is invalid.");
			return false;
		}
		console.info("kwCurr::init() sCode ", sCode);
		this.sCode = sCode;

		let sName: string = this.type.sName;
		if (!kw.isString(sName))
		{
			console.error("kwCurr::init() sName is invalid.");
			return false;
		}
		console.info("kwCurr::init() sName ", sName);
		this.sName = sName;

		let sNamePlural: string = this.type.sNamePlural;
		if (!kw.isString(sNamePlural))
		{
			console.error("kwCurr::init() sNamePlural is invalid.");
			return false;
		}
		console.info("kwCurr::init() sNamePlural ", sNamePlural);
		this.sNamePlural = sNamePlural;

		return true;
	}

	getId(): number
	{
		return this.type.nId;
	}

	getCode(): string
	{
		return this.type.sCode;
	}

	getName(): string
	{
		return this.type.sName;
	}

	getNamePlural(): string
	{
		return this.type.sNamePlural;
	}

	toString(): string
	{
		return this.constructor.name;
	};

	static is(obj: object): boolean
	{
		return kw.is(obj, kwCurr)
	}

}
