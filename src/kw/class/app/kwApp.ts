/**********************************************************************
 *
 * kw/class/app/kwApp.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }		         from "../../kw";

import { kwAppEnum }		 from "./kwAppEnum";
import { kwAppSrvc }		 from "./kwAppSrvc";
// @formatter:on

export class kwApp
{

	constructor( private nType: kwAppEnum )
	{
		console.log("kwApp::constructor() is called.");
	}

	init(): boolean
	{
		console.log("kwApp::init() is called.");

		if( !kwAppSrvc.in(this.nType) )
		{
			console.error("kwApp::init() nType is invalid.");
			return false;
		}
		console.info("kwApp::init() nType is ", this.nType);

		return true;

	}

	isConnect(): boolean
	{
		return (this.nType === kwAppEnum.Connect);
	}

	isCreate(): boolean
	{
		return (this.nType === kwAppEnum.Create);
	}

	isInit(): boolean
	{
		return (this.nType === kwAppEnum.Init);
	}

	isLoggedIn(): boolean
	{
		return (this.nType === kwAppEnum.LoggedIn);
	}

	isLoggedOut(): boolean
	{
		return (this.nType === kwAppEnum.LoggedOut);
	}

	isLogin(): boolean
	{
		return (this.nType === kwAppEnum.Login);
	}

	isReset(): boolean
	{
		return (this.nType === kwAppEnum.Reset);
	}

	isVerify(): boolean
	{
		return (this.nType === kwAppEnum.Verify);
	}

	toString(): string
	{
		return kw.toString(this.nType, kwAppEnum);
	};

	static is(obj: object): boolean
	{
		return kw.is(obj, kwApp)
	}


}
