/**********************************************************************
 *
 * kw/class/app/kwAppInit.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kwApp }			    from "./kwApp";
import { kwAppEnum }			from "./kwAppEnum";
// @formatter:on


export class kwAppInit extends kwApp
{
	constructor()
	{
		super(kwAppEnum.Init);
	}

}

