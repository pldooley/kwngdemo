/**********************************************************************
 *
 * kw/class/app/kwAppEnum.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

export enum kwAppEnum
{
	Connect = 0,
	Close,
	Init,
	Login,
	Reset,
	Create,
	LoggedIn,
	LoggedOut,
	Logout,
	Verify,
}
