/**********************************************************************
 *
 * kw/class/app/kwAppLogin.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kwApp }			    from "./kwApp";
import { kwAppEnum }			from "./kwAppEnum";
// @formatter:on


export class kwAppLogin extends kwApp
{
	constructor()
	{
		super(kwAppEnum.Login);
	}

}

