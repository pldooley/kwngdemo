/**********************************************************************
 *
 * kw/class/mode/kwMode.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }		            from "../../kw";
import { kwModeEnum }		    from "./kwModeEnum";
// @formatter:on


export class kwMode
{
	constructor( private nType: kwModeEnum )
	{
		console.log("kwMode::constructor() called");
	};

	get(): kwModeEnum
	{
		return this.nType;
	};

	isDebug(): boolean
	{

		return (this.nType === kwModeEnum.Debug);
	}

	isLive(): boolean
	{

		return (this.nType === kwModeEnum.Live);
	}

	toString(): string
	{
		return kw.toString(this.nType, kwModeEnum);
	};

	static is(obj: object): boolean
	{
		return kw.is(obj, kwMode)
	}
}

