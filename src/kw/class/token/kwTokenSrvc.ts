/**********************************************************************
 *
 * kw/class/token/kwTokenSrvc.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }		            from "../../kw";

import { kwTokenAuth0 }			from "./kwTokenAuth0";
import { kwTokenEnum }			from "./kwTokenEnum";
import { kwTokenOrg }			from "./kwTokenOrg";
// @formatter:on


export class kwTokenSrvc
{

	static create(sType: string, sToken: string): Object
	{
		//console.log("kwTokenSrvc::loadAuthorization() called.");

		if (!kw.isString(sType))
		{
			console.error("kwTokenSrvc::create() sType is invalid");
			return;
		}
		console.info("kwTokenSrvc::create() sType is [", sType, "]");

		if (!kw.isString(sToken))
		{
			console.error("kwTokenSrvc::create() sToken is invalid");
			return;
		}
		console.info("kwTokenSrvc::create() sToken is [", sToken, "]");

		let nType = kwTokenSrvc.toEnum(sType);
		if (!kwTokenSrvc.in(nType))
		{
			console.error("kwTokenSrvc::create() nType is invalid");
			return;
		}
		console.info("kwTokenSrvc::create() nType is [", nType, "]");


		let token : Object;

		switch(nType)
		{
			case kwTokenEnum.Auth0:
			{
				token = new kwTokenAuth0(sToken);
				break;
			}

			case kwTokenEnum.Org:
			{
				token = new kwTokenOrg(sToken);
				break;
			}

			default:
			{
				console.error("kwTokenSrvc::create() nType is invalid");
			}
		}

		if (kw.isNull(token))
		{
			console.error("kwOptions::createToken() token is not valid.");
			return;
		}

		if (!token.init())
		{
			console.error("kwOptions::createToken() error initializing token.");
			return;
		}

		return token;
	};

	static isType(obj: object): boolean
	{
		return false;
	}

	static in(nVal: number): boolean
	{
		return kw.in(nVal, kwTokenEnum)
	}

	static toEnum(sVal: string): number
	{
		return kw.toEnum(sVal, kwTokenEnum);
	};
}

