/**********************************************************************
 *
 * kw/class/token/kwTokenOrg.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kwToken }		            from "./kwToken";
import { kwTokenEnum }		        from "./kwTokenEnum";
// @formatter:on


export class kwTokenOrg extends kwToken
{
	constructor(protected sToken: string)
	{
		super(kwTokenEnum.Org, sToken);
		console.log("kwTokenOrg::constructor() is called.");
	}

}
