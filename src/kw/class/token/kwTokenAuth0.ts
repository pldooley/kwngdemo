/**********************************************************************
 *
 * kw/class/token/kwTokenAuth0.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kwToken }		            from "./kwToken";
import { kwTokenEnum }		        from "./kwTokenEnum";
// @formatter:on


export class kwTokenAuth0 extends kwToken
{
	constructor(protected sToken: string)
	{
		super(kwTokenEnum.Auth0, sToken);
		console.log("kwTokenAuth0::constructor() is called.");
	}

}
