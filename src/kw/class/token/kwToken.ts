
/**********************************************************************
 *
 * kw/class/token/kwToken.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off

import { kw }    		            from "../../kw";
import { kwTokenEnum }    		    from "./kwTokenEnum";
import { kwTokenSrvc }    		    from "./kwTokenSrvc";
// @formatter:on

export class kwToken
{

	sTokenFull: string;


	constructor(    private nType: kwTokenEnum,
	                protected sToken: string    )
	{
		console.log("kwToken::constructor() is called.");
	}

	init(): boolean
	{
		//console.log("kwToken::init() called.");

		if (!kwTokenSrvc.in(this.nType))
		{
			console.error("kwToken::createToken() nType is invalid.");
			return false;
		}
		console.info("kwToken::createToken() nType is ", this.nType);

		if (!kw.isString(this.sToken))
		{
			console.error("kwToken::createToken() sToken is invalid.");
			return false;
		}
		console.info("kwToken::createToken() sToken is ", this.sToken);

		return this.createToken();
	};

	createToken(): boolean
	{
		//console.log("kwToken::createToken() called.");

		if (!kwTokenSrvc.in(this.nType))
		{
			console.error("kwToken::createToken() nType is invalid.");
			return false;
		}
		console.info("kwToken::createToken() nType is ", this.nType);

		if (!kw.isString(this.sToken))
		{
			console.error("kwToken::createToken() sToken is invalid.");
			return false;
		}
		console.info("kwToken::createToken() sToken is ", this.sToken);

		let sType: string = this.toString();
		if (!kw.isString(sType))
		{
			console.error("kwToken::createToken() sType is invalid.");
			return false;
		}
		console.info("kwToken::createToken() sType is ", sType);

		let sTokenFull: string = sType + " " + this.sToken;
		console.info("kwToken::createToken() sTokenFull is ", sTokenFull);

		this.sTokenFull = sTokenFull;
	}

	getToken(): string
	{
		return this.sTokenFull;
	}

	isAuth0(): boolean
	{
		return (this.nType === kwTokenEnum.Auth0);
	}

	isOrg(): boolean
	{
		return (this.nType === kwTokenEnum.Org);
	}

	toString(): string
	{
		return kw.toString(this.nType, kwTokenEnum);
	};

	static is(obj: object): boolean
	{
		return kw.is(obj, kwToken)
	}

}
