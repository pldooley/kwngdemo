/**********************************************************************
 *
 * kw/class/errBody/kwErrBodyBodyType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
import {kwErrBodyBodyRepType}   from "./kwErrBodyBodyRepType";
// @formatter:on


export class kwErrBodyBodyType
{
	call: string;
	exception: string;
	report: kwErrBodyBodyRepType;
	success: boolean;
	text: string;
};
