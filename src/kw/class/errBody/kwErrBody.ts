/**********************************************************************
 *
 * kw/class/acts/kwActs.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }                           from "../../";
import { kwAct }			            from "../act/kwAct";
import { kwActEnum }			        from "../act/kwActEnum";
import { kwActSrvc }			        from "../act/kwActSrvc";
import { kwActType }			        from "../act/kwActType";
import { kwActGet }                     from "../act/kwActGet";
import { kwActDel }                     from "../act/kwActDel";
import { kwActPost }                    from "../act/kwActPost";
import { kwActUpdate }                  from "../act/kwActUpdate";
import { kwActPatch }                   from "../act/kwActPatch";

import { kwActsSrvc }                   from "./kwErrBodySrvc";
import { kwActsType }			        from "./kwErrType";
// @formatter:on


export class kwActs
{

	body: kwErrBody;

	sStatusText: string;
	sUrl: string;

	nStatus: number;
	nType: number;

	ok: boolean;

	constructor(private type: kwActsType)
	{
		console.log("kwActs::constructor() is called.");
	}

	init(): boolean
	{
		console.log("kwActs::init() is called.");

		if(!kwActsSrvc.isType(this.type))
		{
			console.error("kwActs::init() type is invalid.");
			return false;
		}

		let typeDel: kwActType = this.type.del;
		if (kwActSrvc.isType(typeDel))
		{
			let act: kwAct = new kwActDel(typeDel);
			if (!act.init())
			{
				console.error("kwActs::init() error creating del.");
				return false;
			}
			this.delete = act;
		}

		let typeGet: kwActType = this.type.get;
		if (kwActSrvc.isType(typeGet))
		{
			let act: kwAct = new kwActGet(typeGet);
			if (!act.init())
			{
				console.error("kwActs::init() error creating get.");
				return false;
			}
			this.get = act;
		}

		let typePatch: kwActType = this.type.patch;
		if (kwActSrvc.isType(typePatch))
		{
			let act: kwAct = new kwActPatch(typePatch);
			if (!act.init())
			{
				console.error("kwActs::init() error creating patch.");
				return false;
			}
			this.patch = act;
		}

		let typePost: kwActType = this.type.post;
		if (kwActSrvc.isType(typePost))
		{
			let act: kwAct = new kwActPost(typePost);
			if (!act.init())
			{
				console.error("kwActs::init() error creating post.");
				return false;
			}
			this.post = act;
		}

		let typeUpdate: kwActType = this.type.update;
		if (kwActSrvc.isType(typeUpdate))
		{
			let act: kwAct = new kwActUpdate(typeUpdate);
			if (!act.init())
			{
				console.error("kwActs::init() error creating update.");
				return false;
			}
			this.update = act;
		}

		return true;
	}

	getDelete(): kwAct {
		return this.delete;
	}

	getGet(): kwAct{
		return this.get;
	}

	getPatch(): kwAct{
		return this.patch;
	}

	getPost(): kwAct{
		return this.post;
	}

	getUpdate(): kwAct{
		return this.update;
	}

	static is(obj: object): boolean
	{
		return kw.is(obj, kwActs)
	}


}

