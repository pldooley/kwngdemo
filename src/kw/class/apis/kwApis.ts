/**********************************************************************
 *
 * kw/class/apis/kwApis.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }		                from "../../kw";
import { kwApi }                    from "../api/kwApi";

import { kwApisSrvc }               from "./kwApisSrvc";
import { kwApisType }               from "./kwApisType";
// @formatter:on

export class kwApis
{
	apis: object;

	constructor(private type: kwApisType)
	{
		console.log("kwApis::constructor() called");
	};

	init(): boolean
	{
		console.log("kwApis::init() called.");

		if( !kwApisSrvc.isType(this.type) )
		{
			console.error("kwApis::init() type is invalid.");
			return false;
		}
		console.info("kwApis::init() type ", this.type);

		let apis: object = this.type.apis;
		if (kw.isNull(apis))
		{
			console.error("kwApis::init() apis is invalid.");
			return false;
		}
		console.info("kwApis::init() apis ", apis);
		this.apis = apis;
	}

	getApi(sItem: string): kwApi
	{
		return kwApisSrvc.getItem(sItem, this.apis);
	};

	getApis(): object
	{
		return this.apis;
	};

	toString(): string
	{
		return this.constructor.name;
	};

	static is(obj: object): boolean
	{
		return kw.is(obj, kwApis)
	}


}

