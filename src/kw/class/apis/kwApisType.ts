/**********************************************************************
 *
 * kw/class/apis/kwApisType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
// @formatter:on


export class kwApisType
{
	apis: object;
}
