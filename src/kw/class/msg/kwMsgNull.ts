/**********************************************************************
 *
 * kw/class/kwMsgNull.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kwMsg }		        from "./kwMsg";
import { kwMsgEnum }		    from "./kwMsgEnum";
import { kwMsgType }            from "./kwMsgType";
// @formatter:on

export class kwMsgNull extends kwMsg
{
	constructor(type: kwMsgType)
	{
		super(kwMsgEnum.Null, type);
		console.log("kwMsgNull::constructor() is called.");
	}
}
