/**********************************************************************
 *
 * kw/class/kwMsgLogout.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kwMsg }                from "./kwMsg";
import { kwMsgEnum }		    from "./kwMsgEnum";
import { kwMsgType }            from "./kwMsgType";
// @formatter:on

export class kwMsgLogout extends kwMsg
{
	constructor(type: kwMsgType)
	{
		super(kwMsgEnum.Logout, type);
		console.log("kwMsgLogout::constructor() is called.");
	}
}
