/**********************************************************************
 *
 * kw/class/kwMsgAdd.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kwMsg }                from "./kwMsg";
import { kwMsgEnum }		    from "./kwMsgEnum";
import { kwMsgType }            from "./kwMsgType";
// @formatter:on

export class kwMsgAdd extends kwMsg
{
	constructor(type: kwMsgType)
	{
		super(kwMsgEnum.Add, type);
		console.log("kwMsgAdd::constructor() is called.");
	}

}
