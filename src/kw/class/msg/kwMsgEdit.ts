/**********************************************************************
 *
 * kw/class/kwMsgEdit.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kwMsg }                from "./kwMsg";
import { kwMsgEnum }		    from "./kwMsgEnum";
import { kwMsgType }            from "./kwMsgType";
// @formatter:on

export class kwMsgEdit extends kwMsg
{
	constructor(type: kwMsgType)
	{
		super(kwMsgEnum.Edit, type);
		console.log("kwMsgEdit::constructor() is called.");
	}

}
