/**********************************************************************
 *
 * kw/class/kwMsgDelete.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kwMsg }                from "./kwMsg";
import { kwMsgEnum }		    from "./kwMsgEnum";
import { kwMsgType }            from "./kwMsgType";
// @formatter:on

export class kwMsgDelete extends kwMsg
{
	constructor(type: kwMsgType)
	{
		super(kwMsgEnum.Delete, type);
		console.log("kwMsgDelete::constructor() is called.");
	}
}
