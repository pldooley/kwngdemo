/**********************************************************************
 *
 * kw/class/kwMsgGet.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kwMsg }                from "./kwMsg";
import { kwMsgEnum }		    from "./kwMsgEnum";
import { kwMsgType }            from "./kwMsgType";
// @formatter:on

export class kwMsgGet extends kwMsg
{
	constructor(type: kwMsgType)
	{
		super(kwMsgEnum.Get, type);
		console.log("kwMsgGet::constructor() is called.");
	}
}
