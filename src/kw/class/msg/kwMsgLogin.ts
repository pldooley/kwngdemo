/**********************************************************************
 *
 * kw/class/kwMsgLogin.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kwMsg }                from "./kwMsg";
import { kwMsgEnum }		    from "./kwMsgEnum";
import { kwMsgType }            from "./kwMsgType";
// @formatter:on

export class kwMsgLogin extends kwMsg
{
	constructor(type: kwMsgType)
	{
		super(kwMsgEnum.Login, type);
		console.log("kwMsgLogin::constructor() is called.");
	}
}
