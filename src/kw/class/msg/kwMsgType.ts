/**********************************************************************
 *
 * kw/msg/kwMsgType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kwAct }			    from "../act/kwAct";
import { kwData }               from "../data/kwData";
// @formatter:on


export class kwMsgType
{
	act: kwAct;
	data: kwData;
	params: object;
	sMode: string;
	sToken: string;
}

