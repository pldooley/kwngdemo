/**********************************************************************
 *
 * kw/class/msg/kwMsg.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }		                from "../../kw";
import { kwAct }		            from "../act/kwAct";
import { kwActSrvc }		        from "../act/kwActSrvc";
import { kwApi }                    from "../api/kwApi";
import { kwData }                   from "../data/kwData";
import { kwHttpHelper }             from "../../http/kwHttpHelper";
import { kwHttpHelperMock }         from "../../http/kwHttpHelperMock";
import { kwModeEnum }               from "../mode/kwModeEnum";
import { kwOptionsSrvc}		        from "../options/kwOptionsSrvc";
import { kwOptionsType}		        from "../options/kwOptionsType";
import { kwOptions }                from "../options/kwOptions";
import { kwParam }                  from "../param/kwParam";

import { kwMsgEnum }		        from "./kwMsgEnum";
import { kwMsgSrvc }		        from "./kwMsgSrvc";
import { kwMsgType }		        from "./kwMsgType"; import { kwModeSrvc } from "../mode/kwModeSrvc";
// @formatter:on



export abstract class kwMsg
{

	act: kwAct;
	data: kwData;
	helper: any;
	options: kwOptions;
	params: object;

	sMode: string;

	constructor(private nType: kwMsgEnum,
	            private type: kwMsgType,)
	{
		console.log("kwMsg::constructor() is called.");
	}

	init(): boolean
	{
		//console.log("kwMsg::init() called.");

		if( !kwMsgSrvc.in(this.nType) )
		{
			console.error("kwMsg::init() nType is invalid.");
			return false;
		}
		console.info("kwMsg::init() nType is ", this.nType);

		if( !kwMsgSrvc.isType(this.type) )
		{
			console.error("kwMsg::init() type is invalid.");
			return false;
		}
		console.info("kwMsg::init() type is ", this.type);

		let act: kwAct = this.type.act;
		if( !(kwAct.is(act)) )
		{
			console.error("kwMsg::init() act is invalid");
			return false;
		}
		console.info("kwMsg::init() act is ", act);
		this.act = act;

		let params: object = this.type.params;
		if(kw.isNull(params))
		{
			console.error("kwMsg::init() params is not valid.");
			return false;
		}
		console.info("kwMsg::init() params is ", params);
		this.params = params;

		let sMode = this.type.sMode;
		if( !kw.isString(sMode) )
		{
			console.error("kwMsg::init() sMode is invalid.");
			return false;
		}
		console.info("kwMsg::init() sMode is ", sMode);
		this.sMode = sMode;

		let data = this.type.data;
		if( kw.isNull(data) )
		{
			console.error("kwMsg::init() data is invalid.");
			return false;
		}
		console.info("kwMsg::init() data is ", data);
		this.data = data;

		this.createHelper();
		this.createOptions();

		return true
	}

	getData(): kwData
	{
		return this.data;
	};

	getAct(): kwAct
	{
		return this.act;
	};

	getHelper()
	{
		return this.helper;
	}

	getOptions(): kwOptionsType
	{
		if (!kwOptionsSrvc.isType(this.options))
		{
			console.error("kwMsg::getOptions() options is invalid");
			return;
		}

		return this.options.getOptions();
	}

	createHelper(): boolean
	{
		//console.log("kwMsg::createHelper() called ");

		if(!kw.isString(this.sMode))
		{
			console.error("kwMsg::createHelper() sMode is not valid.");
			return false;
		}

		let nMode: kwModeEnum = kwModeSrvc.toEnum(this.sMode);
		if (!kwModeSrvc.in(nMode))
		{
			console.error("kwMsg::createHelper() sMode is not valid.");
			return false;
		}

		this.helper = (nMode === kwModeEnum.Live) ? kwHttpHelper : kwHttpHelperMock;

		return true;
	}

	createOptions(): boolean
	{
		//console.log("kwMsg::createOptions() called ");

		if( !kwAct.is(this.act) )
		{
			console.error("kwMsg::createOptions() action is invalid");
			return false;
		}

		if( kw.isNull(this.params))
		{
			console.error("kwMsg::createOptions() params is invalid");
			return false;
		}

		let info: kwOptionsType = {
			action: this.act,
			params: this.params,
			data: this.data,
		}

		let options: kwOptions = new kwOptions(info);
		if( !options.init() )
		{
			console.error("kwMsg::createOptions() error creating options");
			return false;
		}

		this.options = options;
	}

	isAdd(): boolean
	{
		return (this.nType === kwMsgEnum.Add);
	}

	isDelete(): boolean
	{
		return (this.nType === kwMsgEnum.Delete);
	}

	isEdit(): boolean
	{
		return (this.nType === kwMsgEnum.Edit);
	}

	isGet(): boolean
	{
		return (this.nType === kwMsgEnum.Get);
	}

	isLogin(): boolean
	{
		return (this.nType === kwMsgEnum.Login);
	}

	isLogout(): boolean
	{
		return (this.nType === kwMsgEnum.Logout);
	}

	isNull(): boolean
	{
		return (this.nType === kwMsgEnum.Null);
	}

	toString(): string
	{
		return kw.toString(this.nType, kwMsgEnum);
	};

	static is(obj: object): boolean
	{
		return kw.is(obj, kwMsg)
	}

}