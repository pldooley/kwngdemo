/**********************************************************************
 *
 * kw/enum/kwMsgEnum.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

export enum kwMsgEnum
{
    Add = 0,
    Delete,
    Edit,
	Get,
	Login,
	Logout,
	Null
}
