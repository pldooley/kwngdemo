/**********************************************************************
 *
 * kw/class/api/kwMsgSrvc.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }		            from "../../kw";
import { kwApi }                from "../api/kwApi";
import { kwParam }              from "../param/kwParam";


import { kwMsgAdd }			    from "./kwMsgAdd";
import { kwMsgDelete }			from "./kwMsgDelete";
import { kwMsgEdit }			from "./kwMsgEdit";
import { kwMsgEnum }			from "./kwMsgEnum";
import { kwMsgGet }			    from "./kwMsgGet";
import { kwMsgType }			from "./kwMsgType";
// @formatter:on


export class kwMsgSrvc
{
	static add(data: object, params: kwParam[], api: kwApi): object
	{
		//console.log("kwMsgSrvc::add() called.");

		if( kw.isNull(data) )
		{
			console.error("kwMsgSrvc::add() data is invalid.");
			return;
		}

		if( kw.isNull(params) )
		{
			console.error("kwMsgSrvc::add() params is invalid.");
			return;
		}

		if( !kwApi.is(api) )
		{
			console.error("kwMsgSrvc::add() api is invalid.");
			return;
		}

		let sMode = api.getMode();
		if( !kw.isString(sMode) )
		{
			console.error("kwMsgSrvc::add() sMode is invalid.");
			return;
		}

		let type: kwMsgType = {
			"sMode": api,
			"data": data,
			"params": params
		};

		let msg = new kwMsgAdd(type);
		if( !msg.init() )
		{
			console.error("kwMsgSrvc::add() error creating msg.");
			return;
		}

		return msg;
	};

	static edit(data, params, api: kwApi): object
	{
		//console.log("kwMsgSrvc::edit() called.");

		if( kw.isNull(data) )
		{
			console.error("kwMsgSrvc::edit() data is invalid.");
			return;
		}

		if( kw.isNull(params) )
		{
			console.error("kwMsgSrvc::edit() params is invalid.");
			return;
		}

		if( !kwApi.is(api) )
		{
			console.error("kwMsgSrvc::edit() api is invalid.");
			return;
		}

		let sMode = api.getMode();
		if( !kw.isString(sMode) )
		{
			console.error("kwMsgSrvc::edit() sMode is invalid.");
			return;
		}

		let type: kwMsgType = {
			"sMode": api,
			"data": data,
			"params": params
		};

		let msg = new kwMsgEdit(type);
		if( !msg.init() )
		{
			console.error("kwMsgSrvc::edit() error creating msg.");
			return;
		}

		return msg;
	};

	static get(params, api: kwApi): object
	{
		//console.log("kwMsgSrvc::get() called.");

		if( kw.isNull(params) )
		{
			console.error("kwMsgSrvc::get() params is invalid.");
			return;
		}

		if( !kwApi.is(api) )
		{
			console.error("kwMsgSrvc::get() api is invalid.");
			return;
		}

		let sMode = api.getMode();
		if( !kw.isString(sMode) )
		{
			console.error("kwMsgSrvc::get() sMode is invalid.");
			return;
		}

		let type: kwMsgType = {
			"sMode": api,
			"data": {},
			"params": params
		};


		let msg = new kwMsgGet(type);
		if( !msg.init() )
		{
			console.error("kwMsgSrvc::get() error creating msg.");
			return;
		}

		return msg;
	};

	static remove(params, api: kwApi): object
	{
		//console.log("kwMsgSrvc::remove() called.");

		if( kw.isNull(params) )
		{
			console.error("kwMsgSrvc::remove() params is invalid.");
			return;
		}

		if( !kwApi.is(api) )
		{
			console.error("kwMsgSrvc::remove() api is invalid.");
			return;
		}

		let sMode = api.getMode();
		if( !kw.isString(sMode) )
		{
			console.error("kwMsgSrvc::remove() sMode is invalid.");
			return;
		}

		let type: kwMsgType = {
			"sMode": api,
			"data": {},
			"params": params
		};

		let msg = new kwMsgDelete(type);
		if( !msg.init() )
		{
			console.error("kwMsgSrvc::remove() error creating msg.");
			return;
		}

		return msg;
	};

	static isType(obj: object): boolean
	{
		return kw.is(obj, kwMsgType)
	}

	static in(nVal: number): boolean
	{
		return kw.in(nVal, kwMsgEnum)
	}

	static toEnum(sVal: string): number
	{
		return kw.toEnum(sVal, kwMsgEnum);
	};
}

