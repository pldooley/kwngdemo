/**********************************************************************
 *
 * kw/class/act/kwActType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kwAjaxType }			from "../ajax/kwAjaxType";
// @formatter:on


export class kwActType
{
	live: kwAjaxType;
	debug: kwAjaxType;
}
