/**********************************************************************
 *
 * kw/class/act/kwActEnum.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

export enum kwActEnum
{
	Delete,
	Get,
	Login,
	Logout,
	Patch,
	Post,
	Update,
	Null,
}

