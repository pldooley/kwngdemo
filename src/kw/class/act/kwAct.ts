/**********************************************************************
 *
 * kw/class/act/kwAct.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }		                from "../../kw";
import { kwActEnum }			    from "./kwActEnum";
import { kwActSrvc }			    from "./kwActSrvc";
import { kwActType }			    from "./kwActType";
import { kwAjaxSrvc }			    from "../ajax/kwAjaxSrvc";
import { kwAjaxType }			    from "../ajax/kwAjaxType";
// @formatter:on


export class kwAct
{

	debug: kwAjaxType;
	live: kwAjaxType;

	constructor(    private nType: kwActEnum,
					private type: kwActType  )
	{
		console.log("kwAct::constructor() is called.");
	}

	init(): boolean
	{
		console.log("kwAct::init() is called.");

		if (!kwActSrvc.in(this.nType))
		{
			console.error("kwAct::init() nType is invalid.");
			return false;
		}
		console.info("kwAct::init() nType is ", this.nType);

		if( !kwActSrvc.isType(this.type) )
		{
			console.error("kwAct::init() type is invalid.");
			return;
		}
		console.info("kwAct::init() type is ", this.type);

		let debug: kwAjaxType = this.type.debug;
		if( !kwAjaxSrvc.isType(debug))
		{
			console.error("kwAct::init() debug is invalid.");
			return;
		}
		this.debug = debug;

		let live: kwAjaxType = this.type.live;
		if( !kwAjaxSrvc.isType(live)  )
		{
			console.error("kwAct::init() live is invalid.");
			return;
		}
		this.live = live;

		return true;
	}

	getDebug()
	{
		return this.debug;
	}

	getLive()
	{
		return this.live;
	}

	isPost(): boolean
	{
		return (this.nType === kwActEnum.Post);
	}

	isDelete(): boolean
	{
		return (this.nType === kwActEnum.Delete);
	}

	isEdit(): boolean
	{
		return (this.nType === kwActEnum.Update);
	}

	isGet(): boolean
	{
		return (this.nType === kwActEnum.Get);
	}

	isLogin(): boolean
	{
		return (this.nType === kwActEnum.Login);
	}

	isLogout(): boolean
	{
		return (this.nType === kwActEnum.Logout);
	}

	toString(): string
	{
		return kw.toString(this.nType, kwActEnum);
	};

	static is(obj: object): boolean
	{
		return kw.is(obj, kwAct)
	}

}

