/**********************************************************************
 *
 * kw/enum/protocol/kwProtocolEnum.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

export enum kwProtocolEnum
{
	Http = 1,
	Https,
	Ftp,
	Sftp
}
