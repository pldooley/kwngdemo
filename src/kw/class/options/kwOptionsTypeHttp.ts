/**********************************************************************
 *
 * kw/options/kwOptionsTypeHttp.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }		                        from "../../kw";
import { kwOptionsTypeHttpHeader }          from "./kwOptionsTypeHttpHeader";
// @formatter:on


export class kwOptionsTypeHttp
{
	data: string;
	headers: kwOptionsTypeHttpHeader;
	method: string;
	token: string;
	url: string;
}
