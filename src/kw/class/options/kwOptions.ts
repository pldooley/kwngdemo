
/**********************************************************************
 *
 * kw/class/kwOptions.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }		                from "../../kw";
import { kwAct }                    from "../act/kwAct";
import { kwData }                   from "../data/kwData";
import { kwParam }                  from "../param/kwParam";;
import { kwToken }                  from "../token/kwToken";
import { kwUrl }		            from "../url/kwUrl";

import { kwOptionsSrvc }            from "./kwOptionsSrvc";
import { kwOptionsType }            from "./kwOptionsType";
import { kwOptionsTypeHttp }        from "./kwOptionsTypeHttp";


// @formatter:on


export class kwOptions
{

	optionsHttp: kwOptionsTypeHttp;

	act: kwAct;
	data: kwData;
	options: kwOptionsTypeHttp;
	params: kwParam[];
	token: kwToken;
	url: kwUrl;

	sMode: string;

	constructor( private type: kwOptionsType )
	{
		console.log("kwOptions::constructor() is called.");
	}

	init(): boolean
	{
		//console.log("kwOptions::init() called.");

		if (!kwOptionsSrvc.isType(this.type))
		{
			console.error("kwOptions::init() type is not valid.");
			return false;
		}
		console.info("kwOptions::init() type is ", this.type);

		let params = this.type.params;
		if (!kw.isNull(params))
		{
			console.error("kwOptions::init() params is not valid.");
			return false;
		}
		console.info("kwOptions::init() params is ", params);
		this.params = params;

		let act = this.type.act;
		if (!kwAct.is(act))
		{
			console.error("kwOptions::init() act is not valid.");
			return false;
		}
		console.info("kwOptions::init() act is ", act);
		this.act = act;

		let data: kwData = this.type.data;
		if (!kwData.is(data))
		{
			console.error("kwOptions::init() data is not valid.");
			return false;
		}
		console.info("kwOptions::init() data is ", data);
		this.data = data;

		let sMode: string = this.type.sMode;
		if (!kw.isString(sMode))
		{
			console.error("kwOptions::init() sMode is not valid.");
			return false;
		}
		console.info("kwOptions::init() sMode is ", sMode);
		this.sMode = sMode;

		let token: kwToken = this.type.token;
		if (!kwToken.is(token))
		{
			console.error("kwOptions::init() token is not valid.");
			return false;
		}
		console.info("kwOptions::init() token is ", token);
		this.token = token;

		return this.create();
	};

	getOptions()
	{
		//console.log("kwOptions::getOptions() called.");

		return this.type;
	}

	create(): boolean
	{
		//console.log("kwOptions::create() called.");

		let sToken: string;
		
		if (!kwToken.is(this.token))
		{
			console.error("kwOptions::create() token is not valid.");
			return;
		}
		console.info("kwOptions::create() token is ", this.token);

		sToken = this.token.toString();
		if (!kw.isString(sToken))
		{
			console.error("kwOptions::create() sToken is invalid");
			return;
		}
		console.info("kwOptions::create() sToken is ", sToken);

		let url: kwUrl = this.createUrl();
		if (!kwUrl.is(url))
		{
			console.error("kwOptions::create() url is not valid.");
			return;
		}
		console.info("kwOptions::create() url is ", this.url);

		let sUrl: string = url.toString();
        if (!kw.isString(sUrl))
        {
            console.error("kwOptions::create() sUrl is not valid.");
            return;
        }
        console.info("kwOptions::create() sUrl is ", sUrl);

		let sMethod:string = this.retrieveMethod();
		if (!kw.isString(sMethod))
		{
			console.error("kwOptions::create() sMethod is not valid.");
			return;
		}
		console.info("kwOptions::create() sMethod is ", sMethod);

		console.info("kwOptions::create() data is ", this.data);

		let options: kwOptionsTypeHttp = {
			headers: {
				authorization: sToken
			},
			url: sUrl,
			method: sMethod,
			data: this.data
		};

		console.info("kwOptions::create() options is ", options);

		this.options = options;
	};

	retrieveMethod(): string
	{
		//console.log("kwOptions::retrieveMethod() called.");

		if (!kwAct.is(this.act))
		{
			console.error("kwOptions::retrieveMethod() act is not valid.");
			return undefined;
		}

		let sMethod = act.toString();
		if (!kw.isString(sMethod))
		{
			console.error("kwOptions::retrieveMethod() sMethod is invalid.");
			return undefined;
		}
		console.info("kwOptions::retrieveMethod() sMethod is ", sMethod);

		return sMethod;
	};

	createUrl(): kwUrl
	{
		//console.log("kwOptions::createUrl() called.");

		if (!kw.isNull(this.act))
		{
			console.error("kwOptions::createUrl() action is not valid.");
			return undefined;
		}

		if (!kw.isNull(this.params))
		{
			console.error("kwOptions::createUrl() params is not valid.");
			return undefined;
		}

		let url: kwUrl = new kwUrl(this.act, this.params);
		if (!url.init())
		{
			console.error("kwOptions::createUrl() error creating url.");
			return undefined;
		}

        return url;
	};

	toString(): string
	{
		return this.constructor.name;
	};

	static is(obj: object): boolean
	{
		return kw.is(obj, kwOptions)
	}

}