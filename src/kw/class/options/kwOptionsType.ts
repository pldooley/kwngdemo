/**********************************************************************
 *
 * kw/options/kwOptionsType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kwAct }                from "../act/kwAct";
import { kwData }               from "../data/kwData";
import { kwModeEnum }           from "../mode/kwModeEnum";
import { kwParam }              from "../param/kwParam";
import { kwToken }              from "../token/kwToken";
import { kwUrl }                from "../url/kwUrl";
// @formatter:on


export class kwOptionsType
{
	act: kwAct;
	data: kwData;
	sMode: string;
	params: kwParam[];
	token: kwToken;
	url: kwUrl;
}
