/**********************************************************************
 *
 * kw/options/kwOptionsTypeHttpHeader.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
// @formatter:on


export class kwOptionsTypeHttpHeader
{
	authorization: string;
}
