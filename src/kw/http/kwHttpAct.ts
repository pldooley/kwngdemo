/**********************************************************************
 *
 * kw/state/kwHttpAct.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:on
import { Injectable }	            from '@angular/core';

import { kw }				        from '../kw';
import { kwApi }                    from "../class/api/kwApi";
import { kwMsg }                    from "../class/msg/kwMsg";
import { kwMsgSrvc }                from "../class/msg/kwMsgSrvc";
// @formatter:off


@Injectable()
export class kwHttpAct
{

	constructor()
	{
		//console.log("kwHttpAct::constructor() called.");
	}

	static add(data: object, params: object, api: kwApi): kwMsg
	{
		//console.log("kwHttpAct::Add() called.");

		if (kw.isNull(data))
		{
			console.error("kwAccAct::add() data is invalid.");
			return;
		}

		if (kw.isNull(params))
		{
			console.error("kwAccAct::add() params is invalid.");
			return;
		}

		if (!kwApi.is(api))
		{
			console.error("kwAccAct::add() api is invalid.");
			return;
		}

		let msg: kwMsg = kwMsgSrvc.add(data, params, api);
		if (!kwMsg.is(msg))
		{
			console.error("kwAccAct::add() msg is invalid.");
			return;
		}

		return msg;
	};

	static delete(params: object, api: kwApi): kwMsg
	{
		//console.log("kwHttpAct::delete() called.");

		if (kw.isNull(params))
		{
			console.error("kwHttpAct::delete() params is invalid.");
			return;
		}

		if (!kwApi.is(api))
		{
			console.error("kwHttpAct::delete() api is invalid.");
			return;
		}

		let msg: kwMsg = kwMsgSrvc.remove(params, api);
		if (!kwMsg.is(msg))
		{
			console.error("kwAccAct::delete() msg is invalid.");
			return;
		}

		return msg;
	};

	static edit(data: object, params: object, api: kwApi): kwMsg
	{
		//console.log("kwHttpAct::edit() called.");

		if (kw.isNull(data))
		{
			console.error("kwHttpAct::edit() params is invalid.");
			return;
		}


		if (kw.isNull(params))
		{
			console.error("kwHttpAct::edit() params is invalid.");
			return;
		}


		if (!kwApi.is(api))
		{
			console.error("kwHttpAct::edit() api is invalid.");
			return;
		}


		let msg: kwMsg = kwMsgSrvc.edit(data, params, api);
		if (!kwMsg.is(msg))
		{
			console.error("kwAccAct::edit() msg is invalid.");
			return;
		}

		return msg;
	};

	static get(params: object, api: kwApi): kwMsg
	{
		//console.log("kwHttpAct::get() called.");

		if (!kw.isArray(params))
		{
			console.error("kwHttpAct::get() params is invalid.");
			return;
		}

		if (kw.isNull(params))
		{
			console.error("kwHttpAct::delete() params is invalid.");
			return;
		}

		if (!kwApi.is(api))
		{
			console.error("kwHttpAct::delete() api is invalid.");
			return;
		}

		let msg: kwMsg = kwMsgSrvc.get(params, api);
		if (!kwMsg.is(msg))
		{
			console.error("kwAccAct::delete() msg is invalid.");
			return;
		}

		return msg;
	};


}
