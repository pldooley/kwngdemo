/**********************************************************************
 *
 * kw/http/kwHttpUrlSrvc.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Injectable }		from '@angular/core';

import { kwBSType }			from '../type/kwBSType';
import { kwUrlType }		from '../type/kwUrlType'; import { kw } from "../kw";
// @formatter:on


const sTEMPL = "{&1}://{&2}:{&3}/{&4}";


@Injectable()
export class kwHttpUrlSrvc
{

	static toString(url: kwUrlType): string
	{
		//console.log("kwHttpUrlSrvc::toString() called.");

		if( !url )
		{
			console.error("kwHttpUrlSrvc::toString() url is invalid.");
			return;
		}
		//console.info("kwHttpUrlSrvc::toString() url is [", url, "]");

		let sUrl = sTEMPL
			.replace("{&1}", url.sMode)
			.replace("{&2}", url.sHost)
			.replace("{&3}", url.nPort.toString())
			.replace("{&4}", url.sAPI);

		console.info("kwHttpUrlSrvc::toString() sUrl is [" + sUrl + "]");

		return sUrl;
	}

	static toUrl(data: kwBSType, sAPI: string, sMode: string): kwUrlType
	{

		//console.log("kwUrlStateData::toUrl() called.");

		if( !kwBSSrvc.isType(data) )
		{
			console.error("kwUrlStateData::toUrl() data is invalid.");
			return;
		}
		console.info( "kwUrlStateData::toUrl() data is [", data, "]");

		if( !kw.isString(sAPI))
		{
			console.error("kwUrlStateData::toUrl() sAPI is invalid.");
			return;
		}
		//console.info( "kwUrlStateData::toUrl() sAPI is [", sAPI, "]");

		if( !kw.isString(sMode))
		{
			console.error("kwUrlStateData::toUrl() sMode is invalid.");
			return;
		}
		//console.info( "kwUrlStateData::toUrl() sMode is [", sMode, "]");

		let nPort = data.nPort;
		if( !kw.isNumber(nPort))
		{
			console.error("kwUrlStateData::toUrl() nPort is invalid.");
			return;
		}
		//console.info( "kwUrlStateData::toUrl() nPort is [", nPort, "]");

		let sHost = data.sHost;
		if( !kw.isString(sHost))
		{
			console.error("kwUrlStateData::toUrl() sHost is invalid.");
			return;
		}
		//console.info( "kwUrlStateData::toUrl() sHost is [", sHost, "]");

		let url: kwUrlType = {
			sAPI: sAPI,
			sHost: sHost,
			sMode: sMode,
			nPort: nPort,
		};
		//console.info( "kwUrlStateData::toUrl() url is [", url, "]");

		return url;
	};

}
