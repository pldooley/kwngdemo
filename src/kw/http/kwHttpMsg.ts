/**********************************************************************
 *
 * kw/http/kwHttpMsg.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }		                from "../kw";
// @formatter:on

export class kwHttpMsg
{

	constructor()
	{
		//console.log("kwHttpMsg::constructor() called");
	}

	static multiple(msg)
	{
		//console.log("kwHttpMsg::multiple() called.");

		if( !kw.isNull(msg) )
		{
			console.error("kwHttpMsg::multiple() msg is invalid");
			return;
		}
		//console.info("kwHttpMsg::multiple() msg is ", msg);

		let helper = msg.getHelper();
		if( !kw.isNull(helper) )
		{
			console.error("kwHttpMsg:multiple() helper is invalid");
			return;
		}

		let options = msg.getOptions();
		if( !kw.isNull(options) )
		{
			console.error("kwHttpMsg:multiple() options is invalid");
			return;
		}

		console.info("kwHttpMsg::multiple() options is ", options);

		let deferred = $q.defer();

		$http(options)
			.then(function(data, status, headers, config)
				{
					//console.log("kwHttpMsg:multiple::then() called");

					if( !kw.isNull(data) )
					{
						console.error("kwHttpMsg::multiple::then() data is invalid");
						deferred.resolve(null);
					}
					//console.info("kwHttpMsg:getUnique::then() data is ", data);

					let records = helper.extractRecords(data);
					if( !isArray(records) )
					{
						console.error("srvcHttpMsg::multiple::then() records is invalid");
						deferred.resolve(null);
					}
					//console.info("kwHttpMsg:multiple::then() records is ", records);

					deferred.resolve(records);

				},
				function(data, status, headers, config)
				{
					//console.error("kwHttpMsg:multiple::error() called");

					srvcStateHttp.changed(data);

					helper.traceError(data);

					deferred.reject(data);
				}
			)

			[ 'finally' ](function()
		{
		});

		return deferred.promise;
	};

	static single(msg)
	{
		//console.log("srvcHttpMsg::single() called.");

		if( !kw.isNull(msg) )
		{
			console.error("srvcHttpMsg::single() msg is invalid");
			return;
		}
		//console.info("srvcHttpMsg::single() msg is ", msg);

		let helper = msg.getHelper();
		if( !kw.isNull(helper) )
		{
			console.error("srvcHttpMsg:single() helper is invalid");
			return;
		}

		let options = msg.getOptions();
		if( !kw.isNull(options) )
		{
			console.error("srvcHttpMsg:single() options is invalid");
			return;
		}

		//console.info("srvcHttpMsg::single() options is ", options);

		let deferred = $q.defer();

		$http(options)
			.then(
				function(data, status, headers, config)
				{
					if( !kw.isNull(data) )
					{
						console.error("srvcHttpMsg::single::then() data is invalid");
						deferred.resolve(null);
					}
					//console.info("srvcHttpMsg:single::then() data is ", data);

					let record = helper.extractRecordArray(data);
					if( !kw.isNull(record) )
					{
						//console.warn("srvcHttpMsg::getUnique::then() record is invalid");
					}
					//console.info("srvcHttpMsg:getUnique::then() record is ", record);

					deferred.resolve(record);
				},
				function(data, status, headers, config)
				{
					console.error("srvcHttpMsg:single::error() called");

					srvcStateHttp.changed(data);

					helper.traceError(data);

					deferred.reject(data);

				})
			[ 'finally' ](function()
			{
				//console.log("srvcHttpMsg:single::finally() called");
			}
		);

		return deferred.promise;
	};
}