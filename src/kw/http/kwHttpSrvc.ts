/**********************************************************************
 *
 * kw/http/kwHttpSrvc.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Headers }					    from "@angular/http";
import { Http }						    from "@angular/http";
import { Injectable }				    from '@angular/core';
import { RequestOptions }			    from "@angular/http";

import { kw }				            from '../kw';
import { kwActEnum }					from "../class/act/kwActEnum";
import { kwErrData }				    from '../state/err/kwErrData';
import { kwHttpUrlSrvc }				from './kwHttpUrlSrvc';
// @formatter:on

@Injectable()
export class kwHttpSrvc
{

/*	info: any;

	constructor(    private http: Http,
					private srvcUrl: kwHttpUrlSrvc,
					private srvcErr: kwErrData)
	{
		//console.log("kwHttpSrvc::constructor() called.");
	};

	public get(url: kwUrlType, data: any, store: any)
	{
		//console.log("kwHttpSrvc::resp() called.");

		if( !data )
		{
			console.error("kwHttpSrvc::get() data is invalid.");
			return;
		}
		//console.info( "kwHttpSrvc::get() data is [", data, "]" );

		if( !url )
		{
			console.error("kwHttpSrvc::get() url is invalid.");
			return;
		}
		//console.info("kwHttpSrvc::get() url is [", url, "]");

		if( !kw.isString(store))
		{
			console.error("kwHttpSrvc::get() store is invalid.");
			return;
		}
		//console.info("kwHttpSrvc::get() store is [", store, "]");

		let nId = data.nId;
		if( !kw.isNumber(nId))
		{
			console.error("kwHttpSrvc::get() nId is invalid.");
			return;
		}
		//console.info( "kwHttpSrvc::get() nId is [", nId, "]" );

		let sUrlBase: string = this.srvcUrl.process(url);
		if( !kw.isString(sUrlBase))
		{
			console.error("kwHttpSrvc::get() sUrlBase is invalid.");
			return;
		}
		//console.info( "kwHttpSrvc::get() sUrlBase is [", sUrlBase, "]" );

		let sUrl = sUrlBase + "?id=" + nId;
		//console.info( "kwHttpSrvc::get() sUrl is [", sUrl, "]" );

		let headers = new Headers({
			'Content-Type': 'application/json; charset=utf-8'
		});

		let options = new RequestOptions({headers: headers});
		let body = {}

		this.http.get(sUrl).subscribe(data =>
		{
			//console.info("kwHttpSrvc::get()::next() called");
			//console.info("kwHttpSrvc::get()::next() data is [", data, "]");

			let info = data.json();
			if( !info )
			{
				console.error("kwHttpSrvc::get() info is invalid.");
				return;
			}

			if( info.length != 1 )
			{
				console.error("kwHttpSrvc::get() info is invalid.");
				return;
			}
			console.info("kwHttpSrvc::get() info is [", info, "]");


			store.change(info[ 0 ]);

		}, err =>
		{
			//console.info("kwHttpSrvc::get()::err() called");
			//console.info("kwHttpSrvc::get()::err() err is [", err, "]");

			this.srvcErr.change(err);

		});

	}

	public process(url: kwUrlType, action: Action, srvcStore: any)
	{
		//console.log("kwHttpSrvc::resp() called.");

		if( !action )
		{
			console.error("kwHttpSrvc::process() action is invalid.");
			return;
		}
		//console.info("kwHttpSrvc::process() action is [", action, "]");

		if( !url )
		{
			console.error("kwHttpSrvc::process() url is invalid.");
			return;
		}
		//console.info("kwHttpSrvc::process() url is [", url, "]");

		if( !kw.isString(srvcStore))
		{
			console.error("kwHttpSrvc::process() srvcStore is invalid.");
			return;
		}

		let data = action.data;
		if( !data )
		{
			console.error("kwHttpSrvc::process() data is invalid.");
			return;
		}
		//console.info("kwHttpSrvc::process() data is [", data, "]");

		switch( action.nMethod )
		{

			case kwActEnum.Post:
			{
				this.post(url, data, srvcStore);
				return;
			}

			case kwActEnum.Get:
			{
				this.get(url, data, srvcStore);
				return;
			}
		}

	}


	public post(url: kwUrlType, data: any, srvcStore: any)
	{
		//console.log("kwHttpSrvc::post() called.");

		if( !data )
		{
			console.error("kwHttpSrvc::post() data is invalid.");
			return;
		}
		//console.info("kwHttpSrvc::post() data is [", data, "]");

		if( !url )
		{
			console.error("kwHttpSrvc::post() url is invalid.");
			return;
		}
		//console.info("kwHttpSrvc::post() url is [", url, "]");

		if( !kw.isString(srvcStore))
		{
			console.error("kwHttpSrvc::post() srvcStore is invalid.");
			return;
		}

		let sUrl: string = this.srvcUrl.process(url);
		if( !kw.isString(sUrl))
		{
			console.error("kwHttpSrvc::post() sUrl is invalid.");
			return;
		}
		//console.info("kwHttpSrvc::post() sUrl is [", sUrl, "]");

		let headers = new Headers({
			'Content-Type': 'application/json; charset=utf-8'
		});

		let options = new RequestOptions({headers: headers});
		let body = JSON.stringify(data);

		this.http.post(sUrl, body, options).subscribe(data =>
		{
			//console.info("kwHttpSrvc::post()::next() called");
			//console.info("kwHttpSrvc::post()::next() data is [", data, "]");

			srvcStore.change(data.json());
		}, err =>
		{
			//console.info("kwHttpSrvc::post()::err() called");
			//console.info("kwHttpSrvc::post()::err() err is [", err, "]");

			this.srvcErr.change(err)
		});

	}
*/
}
