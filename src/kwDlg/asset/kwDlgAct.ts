// @formatter:off
import { Component }				from '@angular/core';
import { Input }					from '@angular/core';

import { kwDlgActType }             from "../type/kwDlgActType";
// @formatter:on


@Component({
	selector: 'kw-dlg-act',
	templateUrl: './kwDlgAct.html',
})
export class kwDlgAct
{

	@Input() info: kwDlgActType;

	constructor()
	{
		//console.log("kwDlgAct::constructor() called.");
	}
}
