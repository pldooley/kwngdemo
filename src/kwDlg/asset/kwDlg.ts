// @formatter:off
import { ChangeDetectorRef }	        from '@angular/core';
import { Component }					from '@angular/core';
import { Location }					    from '@angular/common';
import { OnDestroy }					from '@angular/core';
import { OnInit }						from '@angular/core';
import { Router }						from '@angular/router';
import { Subscription }			        from 'rxjs/Subscription';

import { kwDlgType }					from "../type/kwDlgType";
import { kwDlgStateData }				from '../state/kwDlgStateData';
// @formatter:on


const sHOME = "/dashboard";

@Component({
	selector: 'kw-dlg',
	templateUrl: './kwDlg.html',
})
export class kwDlg implements OnDestroy,
	OnInit
{

	info: kwDlgType;

	subscription: Subscription;

	constructor(private cd: ChangeDetectorRef,
							private location: Location,
							private router: Router,
							private srvcDlg: kwDlgStateData)
	{
		//console.log("kwDlg::constructor() called.");
	}

	ngOnInit(): void
	{
		//console.log("kwDlg::ngOnInit() called.");

		this.subscribe();
		this.retrieveDlg();
	}

	ngOnDestroy()
	{
		//console.log("kwDlg::ngOnDestroy() called.");

		this.subscription.unsubscribe();
	}

	onClick()
	{
		//console.log("kwDlg::onClick() called.");

		let sRoute = this.router.url;
		if( !(sRoute) )
		{
			console.error("kwDlg::onClick() sRoute is invalid.");
			return;
		}
		console.info("kwDlg::onClick() sRoute is [", sRoute, "]");

		if( sRoute === sHOME )
		{
			return;
		}

		this.location.back();
	}

	retrieveDlg(): void
	{
		//console.log("kwDlg::retrieveDlg() called.");

		let info = this.srvcDlg.retrieve();
		if( !info )
		{
			console.error("kwDlg::retrieveDlg() info invalid.");
			return;
		}
		//console.info("kwDlg::retrieveDlg() info is [", info, "]");

		this.info = info;
	}

	subscribe()
	{
		//console.log("kwDlg::subscribe() called.");

		this.subscription = this.srvcDlg.changed$.subscribe(
			info =>
			{
				//console.log("kwDlg::srvcDlg::change() called.");

				if( !info )
				{
					console.error("kwDlg::srvcDlg::change() info is invalid.");
					return;
				}
				//console.info("kwDlg::srvcDlg::change() info is [", info, "]");

				this.retrieveDlg();
			});
	}


}


