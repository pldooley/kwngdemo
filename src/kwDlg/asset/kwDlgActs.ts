// @formatter:off
import { Component }				from '@angular/core';
import { Input }					from '@angular/core';

import { kwDlgActType }             from "../type/kwDlgActType";
import { kwDlgActsType }	        from '../type/kwDlgActsType';
// @formatter:on


@Component({
	selector: 'kw-dlg-acts',
	templateUrl: './kwDlgActs.html',
})
export class kwDlgActs
{

	@Input() info: kwDlgActsType;

	infos: kwDlgActType[];

	constructor()
	{
		//console.log("kwDlgActs::constructor() called.");
	}
}
