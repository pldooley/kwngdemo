// @formatter:off
import { Component }					from '@angular/core';
import { Input }						from '@angular/core';
import { OnInit }						from '@angular/core';

import { kwDlgTitleType }				from '../type/kwDlgTitleType';
// @formatter:on


@Component({
	selector: 'kw-dlg-title',
	templateUrl: './kwDlgTitle.html',
})
export class kwDlgTitle implements OnInit
{

	@Input() info: kwDlgTitle;


	constructor()
	{
		//console.log("kwDlgTitle::constructor() called.");
	}

	ngOnInit(): void
	{
		//console.log("kwDlgTitle::ngOnInit() called.");

		console.info("kwDlgTitle::ngOnInit() info is [", this.info, "]");
	}


}
