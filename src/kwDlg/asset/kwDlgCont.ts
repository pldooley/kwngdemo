// @formatter:off
import { Component }					from '@angular/core';
import { Input }						from '@angular/core';

import { kwDlgContType }		        from '../type/kwDlgContType';
// @formatter:on


@Component({
	selector: 'kw-dlg-content',
	templateUrl: './kwDlgCont.html',
})
export class kwDlgCont
{

	@Input() info: kwDlgContType;

	constructor()
	{
		//console.log("kwDlgCont::constructor() called.");
	}

}
