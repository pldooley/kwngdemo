// @formatter:off
import { NgModule }								 from '@angular/core';

import { kwModule }							    from '../kw/kwModule';

import { kwDlg }								from './asset/kwDlg';
import { kwDlgAct }					            from './asset/kwDlgAct';
import { kwDlgActs }					        from './asset/kwDlgActs';
import { kwDlgCont }					        from './asset/kwDlgCont';
import { kwDlgTitle }						    from './asset/kwDlgTitle';

import { kwDlgType }							from './type/kwDlgType';
import { kwDlgActType }						    from './type/kwDlgActType';
import { kwDlgActsType }						from './type/kwDlgActsType';
import { kwDlgContType }						from './type/kwDlgContType';
import { kwDlgTitleType }						from './type/kwDlgTitleType';

import { kwDlgState }	                        from "./state/kwDlgState";
import { kwDlgStateData }						from "./state/kwDlgStateData";
// @formatter:on

@NgModule(
	{
		imports:
			[],
		declarations:
			[
				kwDlg,
				kwDlgAct,
				kwDlgActs,
				kwDlgCont,
				kwDlgTitle,
				kwDlgState,
			],
		providers:
			[
				kwDlgStateData,
				kwDlgType,
				kwDlgActType,
				kwDlgActsType,
				kwDlgContType,
				kwDlgTitleType
			],
		exports:
			[
				kwDlg,
				kwDlgActs,
				kwDlgCont,
				kwDlgTitle,
				kwDlgState,
				kwDlgStateData,
				kwDlgType,
				kwDlgActType,
				kwDlgActsType,
				kwDlgContType,
				kwDlgTitleType
			]
	})
export class kwDlgModule {}
