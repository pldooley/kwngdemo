// @formatter:off
import { kwDlgActType }			from './kwDlgActType';
// @formatter:on


export class kwDlgActsType
{
	actions: kwDlgActType[];
}
