// @formatter:off
import { kwLinkType }			 from '../../kw/type/kwLinkType';
import { kwTitleType }			 from '../../kw/type/kwTitleType';
// @formatter:on


export class kwDlgActType
{
	title: kwTitleType;
	link: kwLinkType;
}
