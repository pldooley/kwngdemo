// @formatter:off
import { kwDlgTitleType }			from './kwDlgTitleType';
import { kwDlgActsType }			from './kwDlgActsType';
import { kwDlgContType }			from './kwDlgContType';
// @formatter:on


export class kwDlgType
{
	actions?: kwDlgActsType;
	content?: kwDlgContType;
	title: kwDlgTitleType;
	sImage?: string;
	bHasClose?: boolean;
	sUrl?: string;
}
