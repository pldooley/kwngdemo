// @formatter:off
import { kwLogoType }			 from '../../kw/type/kwLogoType';
// @formatter:on


export class kwDlgContType
{
	sTitle: string;
	sSubTitle?: string;
	nHeight?: number;
}
