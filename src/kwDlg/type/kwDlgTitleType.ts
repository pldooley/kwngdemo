// @formatter:off
import { kwLogoType }			 from '../../kw/type/kwLogoType';
import { kwTitleType }			 from '../../kw/type/kwTitleType';
// @formatter:on


export class kwDlgTitleType
{
	title: kwTitleType;
	logo: kwLogoType;
	sImage: string;
	sUrl: string;
	bIsXVisible: boolean;
}