// @formatter:off
import { Component }			from '@angular/core';
import { OnInit }				from '@angular/core';
import { OnDestroy }			from '@angular/core';
import { Subscription }	        from 'rxjs/Subscription';


import { kwDlgStateData }	    from './kwDlgStateData';
// @formatter:on


@Component({
	selector: 'kw-dlg-state',
	template: ``,
})
export class kwDlgState implements OnInit,
	OnDestroy
{
	sub: Subscription;

	constructor(private srvcData: kwDlgStateData)
	{

		//console.log("kwDlgState::constructor() called");
	}

	ngOnInit()
	{
		//console.log("kwDlgState::ngOnInit() called");

		this.sub = this.srvcData.changed$.subscribe(dlg =>
		{
			//console.log("kwDlgState::subscribe()::changed$() called.");
		});
	};

	ngOnDestroy()
	{
		//console.log("kwDlgState::ngOnDestroy() called.");

		this.sub.unsubscribe();
	}

	retrieve()
	{
		//console.log("kwDlgState::retrieve() called");

		let dlg = this.srvcData.retrieve();
		if( dlg == null )
		{
			console.error("kwDlgState::retrieve() dlg is invalid.");
			return;
		}
		//console.info("kwDlgState::retrieve() dlg is ", dlg);

	};


}
