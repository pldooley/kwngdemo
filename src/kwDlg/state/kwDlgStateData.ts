// @formatter:off
import { EventEmitter }		        from '@angular/core';
import { Injectable }			    from '@angular/core';

import { kwDlgType }				from '../type/kwDlgType';
import { kwDlgActType }		        from '../type/kwDlgActType';
import { kwDlgActsType }		    from '../type/kwDlgActsType';
import { kwDlgContType }		    from '../type/kwDlgContType';
import { kwDlgTitleType }			from '../type/kwDlgTitleType';
// @formatter:on


const TITLE: kwDlgTitleType = {
	title: {
		sMain: "THANK YOU FOR STAYING WITH US",
		sSub: "How is your stay going?",
		nHeight: 75,
	},
	sImage: "/fairmont.jpg",
	logo: {
		sUrl: "/Fairmont_Logo.gif"
	},
	bIsXVisible: true,
	sUrl: "/dashboard"

};

const CONTENT: kwDlgContType = {
	sTitle: "Please take a moment. We appreciate your feedback.",
	nHeight: 100
};

const GREAT: kwDlgActType = {
	title: {
		sMain: "Great!",
		sSub: "My stay is going well!",
		sThird: "",
		nHeight: 50
	},
	link: {
		sUrl: "/great",
		sIcon: "check_circle",
	},
};

const POOR: kwDlgActType = {
	title: {
		sMain: "Hmm...",
		sSub: "My stay could be better.",
		sThird: "",
		nHeight: 30
	},
	link: {
		sUrl: "/terrible",
		sIcon: "do_not_disturb_on"
	},
};

const ACTIONS: kwDlgActsType = {
	actions: [ GREAT, POOR ]
}

const DLG: kwDlgType = {
	content: CONTENT,
	title: TITLE,
	actions: ACTIONS
};

@Injectable()
export class kwDlgStateData
{

	private broadcast: EventEmitter<kwDlgType>;

	data: kwDlgType = DLG;

	changed$: EventEmitter<kwDlgType>;

	constructor()
	{
		//console.log("kwDlgStateData::constructor() called.");

		this.broadcast = new EventEmitter<kwDlgType>();
		this.changed$ = this.broadcast;
	}

	change(data: kwDlgType)
	{
		//console.log("kwDlgStateData::change() called.");

		if( !data )
		{
			console.error("kwDlgStateData::change() data is invalid.");
			return
		}
		//console.info("kwDlgStateData::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("kwDlgStateData::clear() called.");

		this.data = null;
	};

	retrieve(): kwDlgType
	{
		//console.log("kwDlgStateData::retrieve() called.");

		return this.data;
	};


}
