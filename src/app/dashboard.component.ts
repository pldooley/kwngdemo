/**********************************************************************
 *
 * app/class/acc/hcnHttpAcc.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { Component }					from '@angular/core';
import { MatDialogRef	}				from "@angular/material";
import { OnInit }						from '@angular/core';

import { kwDlg }						from '../kwDlg/asset/kwDlg';
import { kwDlgType }					from '../kwDlg/type/kwDlgType';
import { kwDlgActType }                 from '../kwDlg/type/kwDlgActType';
import { kwDlgActsType }				from '../kwDlg/type/kwDlgActsType';
import { kwDlgContType }				from '../kwDlg/type/kwDlgContType';
import { kwDlgTitleType }				from '../kwDlg/type/kwDlgTitleType';
import { kwDlgStateData }				from '../kwDlg/state/kwDlgStateData';
// @formatter:on


const TITLE: kwDlgTitleType = {
	title: {
		sMain: "THANK YOU FOR STAYING WITH US",
		sSub: "How is your stay going?",
		nHeight: 75,
	},
	sImage: "/fairmont.jpg",
	logo: {
		sUrl: "/Fairmont_Logo.gif"
	},
	bIsXVisible: true,
	sUrl: "/dashboard"

};

const CONTENT: kwDlgContType = {
	sTitle: "Please take a moment. We appreciate your feedback.",
	nHeight: 100
};


const GREAT: kwDlgActType = {
	title: {
		sMain: "Great!",
		sSub: "My stay is going well!",
		sThird: "",
		nHeight: 50
	},
	link: {
		sUrl: "/great",
		sIcon: "check_circle",
	},
};

const POOR: kwDlgActType = {
	title: {
		sMain: "Hmm...",
		sSub: "My stay could be better.",
		sThird: "",
		nHeight: 30
	},
	link: {
		sUrl: "/terrible",
		sIcon: "do_not_disturb_on"
	},
};

const ACTIONS: kwDlgActsType = {
	actions: [ GREAT, POOR ]
}

const DLG: kwDlgType = {
	content: CONTENT,
	title: TITLE,
	actions: ACTIONS
};

@Component({
	selector: 'hcn-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit
{

	info: kwDlgType = DLG;

	constructor(private srvcDlg: kwDlgStateData,
							private dlgRef: MatDialogRef<kwDlg>)
	{

		//console.log("DashboardComponent::constructor() called.");

	}

	ngOnInit(): void
	{

		//console.log("DashboardComponent::ngOnInit() called.");

		//console.info("DashboardComponent::ngOnInit() info is [" , this.info, "]");

		this.srvcDlg.change(this.info);
	}

	onCancel(): void
	{

		console.log("DashboardComponent::onCancel() called.");

		this.dlgRef.close();
	}

}
