/**********************************************************************
 *
 * app/class/acc/hcnHttpAcc.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { Component }					from '@angular/core';
import { MatDialog }					from '@angular/material';
import { OnDestroy }					from '@angular/core';
import { OnInit }						from '@angular/core';
import { Router }						from "@angular/router";
import { Subscription }			        from 'rxjs/Subscription';

import { kwErrData }				    from '../kw/state/err/kwErrData';
import { kwDlg }						from "../kwDlg/asset/kwDlg";
// @formatter:on


const DLG_CONFIG = {
	width: '751px',
	height: '250px',
	id: "critiqueDlg",
	panelClass: "hcn-panel",
	disableClose: true,
	hasBackdrop: true,
	backdropClass: "hcn-dlg-backdrop",
}

@Component({
	selector: 'hcn-app',
	templateUrl: './app.component.html',
	styleUrls: [ './app.component.css' ]
})
export class AppComponent implements OnDestroy,
	OnInit
{

	sub: Subscription;

	constructor(    public dlg: MatDialog,
					public router: Router,
					private srvcErr: kwErrData)
	{
		//console.log("AppComponent::constructor() called.");
	}

	ngOnDestroy()
	{
		//console.log("AppComponent::ngOnDestroy() called.");

		this.sub.unsubscribe();
	}

	ngOnInit(): void
	{
		//console.log("AppComponent::ngOnInit() called.");

		this.subscribe();
	}

	onOpenDlg(): void
	{
		//console.log("AppComponent::onOpenDlg() called.");

		this.router.navigate([ "" ]);
		this.openDlg();
	}

	openDlg(): void
	{
		//console.log("AppComponent::openDlg() called.");

		let dlgRef = this.dlg.open(kwDlg, DLG_CONFIG);

		dlgRef.afterClosed().subscribe(result =>
		{
			//console.log('The dlg was closed');

			this.router.navigate([ "" ]);
		});

	}

	subscribe(): void
	{
		//console.log("AppComponent::subscribe() called.");

		this.sub = this.srvcErr.changed$.subscribe(
			info =>
			{
				//console.log("AppComponent::srvcPage::change() called.");

				if( !info )
				{
					console.error("AppComponent::srvcErr::change() info is invalid.");
					return;
				}
				//console.info("TitleComponent::srvcPage::change() info is [", info, "]");

				//alert(info);
			});
	}


}


