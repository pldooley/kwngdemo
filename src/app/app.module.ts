/**********************************************************************
 *
 * app/module.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { NgModule }							from '@angular/core';

import { BrowserModule }					from '@angular/platform-browser';
import { BrowserAnimationsModule }		    from '@angular/platform-browser/animations';
import { HttpModule }						from '@angular/http';
import { HttpClientModule }					from '@angular/common/http';

import { SharedModule }						from './core/modules/shared.module';
import { kwModule }							from '../kw/kwModule';
import { kwDlgModule }						from '../kwDlg/kwDlgModule';
import { kwFormModule }						from '../kwForm/kwFormModule';

import { AppRoutingModule }					from './app-routing.module';

import { AppComponent }						from './app.component';
import { DashboardComponent }				from './dashboard.component';
import { GreatComponent }					from './great.component';
import { TerribleComponent }				from './terrible.component';
import { ThanksComponent }					from './thanks.component';


import { hcnCritiqueState }				    from "./util/critique/hcnCritiqueState";
import { hcnCritiqueData }				    from "./util/critique/hcnCritiqueData";
import { hcnEmailsKey }					    from "./key/emails/hcnEmailsKey";
import { hcnEmailsKeyData }				    from "./key/emails/hcnEmailsKeyData";
import { hcnGuestHttp }						from "./util/guest/hcnGuestHttp";
import { hcnGuestState }					from "./util/guest/hcnGuestState";
import { hcnGuestData }				        from "./util/guest/hcnGuestData";
import { hcnOptionsData }				    from "./util/options/hcnOptionsData";
import { hcnRespApi }				        from "./util/resp/hcnRespApi";
import { hcnRespHttp }					    from "./util/resp/hcnRespHttp";
import { hcnRespMsg }			            from "./util/resp/hcnRespMsg";
import { hcnRespState }					    from "./util/resp/hcnRespState";
import { hcnTripAdvisorState }			    from "./util/tripAdvisor/hcnTripAdvisorState";
import { hcnTripAdvisorData }			    from "./util/tripAdvisor/hcnTripAdvisorData";
// @formatter:on

@NgModule(
	{
		imports:
			[
				AppRoutingModule,
				BrowserModule,
				BrowserAnimationsModule,
				HttpModule,
				HttpClientModule,
				SharedModule,
				kwModule,
				kwDlgModule,
				kwFormModule
			],
		declarations:
			[
				AppComponent,
				DashboardComponent,
				GreatComponent,
				TerribleComponent,
				ThanksComponent,
				hcnCritiqueState,
				hcnEmailsKey,
				hcnGuestHttp,
				hcnGuestState,
				hcnRespHttp,
				hcnRespState,
				hcnTripAdvisorState,
			],
		entryComponents:
			[],
		providers:
			[
				hcnCritiqueData,
				hcnEmailsKeyData,
				hcnGuestData,
				hcnOptionsData,
				hcnRespMsg,
				hcnRespApi,
				hcnTripAdvisorData,
			],
		bootstrap: [ AppComponent ]
	})
export class AppModule {}
