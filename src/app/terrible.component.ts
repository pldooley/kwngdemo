/**********************************************************************
 *
 * app/class/acc/hcnHttpAcc.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { Component }						from '@angular/core';
import { FormControl }						from '@angular/forms';
import { FormGroup }						from '@angular/forms';
import { MatDialogRef }						from "@angular/material";
import { OnInit }							from '@angular/core';

import { kwDlg }							from "../kwDlg/asset/kwDlg";
import { kwDlgType }					    from '../kwDlg/type/kwDlgType';
import { kwDlgTitleType }				    from '../kwDlg/type/kwDlgTitleType';
import { kwDlgStateData }                   from "../kwDlg/state/kwDlgStateData";
import { kwMetricMsg }		                from '../kw/main/metric/kwMetricMsg';
import { kwMetricType }                     from "../kw/class/metric/kwMetricType";

import { hcnCritiqueType }					from "./util/critique/hcnCritiqueType";
import { hcnCritiqueData }				    from './util/critique/hcnCritiqueData';
import { hcnGuestData }				        from './util/guest/hcnGuestData';
import { hcnRespMsg }                       from "./util/resp/hcnRespMsg";
import { hcnRespType }                      from "./util/resp/hcnRespType";
// @formatter:on


const DLG_TITLE: kwDlgTitleType = {
	title: {
		sMain: "How can we make your stay more delightful?",
		sSub: "",
		sThird: "",
		nHeight: 50
	},
	sImage: "/assets/fairmont.jpg",
	logo: {
		sUrl: "/assets/Fairmont_Logo.gif"
	},
	bIsXVisible: true,
	sUrl: "/terrible"
};

const DLG: kwDlgType = {
	title: DLG_TITLE
}

const sMETRIC = "STEP_ONE_BAD";

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;


@Component({
	selector: 'hcn-terrible',
	templateUrl: './terrible.component.html',
	styleUrls: [ './terrible.component.css' ]
})
export class TerribleComponent implements OnInit
{

	critique: hcnCritiqueType;
	guest: object;
	isLinear: boolean = false;
	page: kwDlgType = DLG;

	contactGroup: FormGroup;
	optionGroup: FormGroup;
	textGroup: FormGroup;

	constructor(    private dlgRef: MatDialogRef<kwDlg>,
	                private srvcCritique: hcnCritiqueData,
					private srvcDlg: kwDlgStateData,
					private srvcGuest: hcnGuestData,
					private srvcMetrixMsg: kwMetricMsg,
					private srvcRespMsg: hcnRespMsg)
	{
		//console.log("TerribleComponent::constructor() called.");
	}

	ngOnInit(): void
	{
		//console.log("TerribleComponent::ngOnInit() called.");

		//console.info("TerribleComponent::ngOnInit() page is[", this.page, "].");

		this.srvcDlg.change(this.page);
		this.send();

		this.retrieveGuest();
		this.retrieveCritique();

		this.optionGroup = new FormGroup({
			id: new FormControl()
		});

		this.textGroup = new FormGroup({
			issue: new FormControl()
		});

		this.contactGroup = new FormGroup({
			phone: new FormControl(),
			email: new FormControl(),
		});

	}

	onCancel(): void
	{
		console.log("TerribleComponent::onCancel() called.");

		let data: kwMetricType =
		{
			sButton: sMETRIC
		}

		this.srvcMetrixMsg.actionAdd(data, []);

		this.dlgRef.close();
	}

	onSubmit(): void
	{
		console.log("TerribleComponent::onSubmit() called.");

		let value = this.textGroup.value;
		if( !value )
		{
			console.error("TerribleComponent::onSubmit() value is invalid.");
			return;
		}
		console.info("TerribleComponent::onSubmit() value is [", value, "]");

		let sIssue = value.issue;
		if( !sIssue )
		{
			console.error("TerribleComponent::onSubmit() sIssue is invalid.");
			return;
		}
		console.info("TerribleComponent::onSubmit() sIssue is [", sIssue, "]");

		debugger;
		let resp: hcnRespType = {
			GuestResp: sIssue,
/*			GuestPhone: this.guest["sPhone"],
			GuestEmail: this.guest["sEmail"],
			GuestRoom: this.guest["sRoom"],
			GuestFirst: this.guest["sNameFirst"],
			GuestLast: this.guest["sNameLast"],
*/			TargetEmail: "",
			TimeStamp: 35
		};

		console.info("TerribleComponent::onSubmit() resp is [", resp, "]");

		this.srvcRespMsg.actionAdd(resp, []);

		this.dlgRef.close();
	}

	retrieveCritique(): void
	{
		//console.log("TerribleComponent::retrieveCritique() called.");

		let critique = this.srvcCritique.retrieve();
		if( !critique )
		{
			console.error("TerribleComponent::retrieveCritique() critique is invalid.");
			return;
		}
		console.info("TerribleComponent::retrieveCritique() critique is [", critique, "]");

		this.critique = critique;
	}

	retrieveGuest(): void
	{
		//console.log("TerribleComponent::retrieveGuest() called.");

		let guest = this.srvcGuest.retrieve();
		if( !guest )
		{
			console.error("TerribleComponent::retrieveGuest() guest is invalid.");
			return;
		}
		console.info("TerribleComponent::retrieveGuest() guest is [", guest, "]");

		this.guest = guest;
	}

	send(): void
	{
		//console.log("TerribleComponent::send() called.");

		let data: kwMetricType = {
			sButton: sMETRIC
		};

		this.srvcMetrixMsg.actionAdd(data, []);
	}


}
