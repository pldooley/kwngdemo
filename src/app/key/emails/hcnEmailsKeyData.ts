/**********************************************************************
 *
 * kw/key/host/hcnEmailsKeyData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }             from '@angular/core';
import { Injectable }               from '@angular/core';

import { kw }		                from "../../../kw/kw";

import { hcnEmailType }             from "../../class/email/hcnEmailType";
// @formatter:on


@Injectable()
export class hcnEmailsKeyData
{

	// Observable host sources
	broadcast: EventEmitter<hcnEmailType[]>;

	// Observable string streams
	changed$: EventEmitter<hcnEmailType[]>;

	data: hcnEmailType[];

	constructor()
	{

		//console.log("hcnEmailsKeyData::constructor() called.");

		this.broadcast = new EventEmitter<hcnEmailType[]>();
		this.changed$ = this.broadcast;
	}

	change(data: hcnEmailType[]): void
	{

		//console.log("hcnEmailsKeyData::change() called.");

		if( kw.isNull(data) )
		{
			console.error("hcnEmailsKeyData::change() data is invalid.");
			return
		}
		//console.info("hcnEmailsKeyData::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("hcnEmailsKeyData::clear() called.");
		this.data = null
	};

	retrieve(): hcnEmailType[]
	{
		//console.log("hcnEmailsKeyData::retrieve() called.");

		return this.data;
	};

}
