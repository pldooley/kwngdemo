/**********************************************************************
 *
 * kw/http/hcnEmailsKey.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import {Component}					from '@angular/core';
import {OnInit}						from '@angular/core';

import { kw }		                from "../../../kw/kw";
import { kwKeySrvc }				from "../../../kw/key/kwKeySrvc";

import { hcnEmailSrvc }             from "../../class/email/hcnEmailSrvc";
import { hcnEmailType }             from "../../class/email/hcnEmailType";
import { hcnEmailsKeyData }			from './hcnEmailsKeyData';
// @formatter:on


const sKEY = "emails";


@Component({
	selector: 'hcn-emails-key',
	template: ``,
})
export class hcnEmailsKey implements OnInit
{

	constructor(private store: hcnEmailsKeyData)
	{

		//console.log("hcnEmailsKey::constructor() called");
	}

	ngOnInit()
	{
		//console.log("hcnEmailsKey::ngOnInit() called");

		this.load();
	};

	load(): void
	{
		//console.log("hcnEmailsKey::loadId() called.");

		let emails: hcnEmailType[] = kwKeySrvc.retrieve(sKEY, this.store);
		if( !hcnEmailSrvc.isType(emails))
		{
			console.error("hcnEmailsKey::load() [", sKEY, "] is not present");
			return;
		}
		console.info("hcnEmailsKey::load() [", sKEY, "] is [", emails, "]");
	}

}
