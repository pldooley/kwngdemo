/**********************************************************************
 *
 * app/class/acc/hcnHttpAcc.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { NgModule }					from '@angular/core';
import { RouterModule }				from '@angular/router';
import { Routes }					from '@angular/router';

import { DashboardComponent }       from "./dashboard.component";
import { GreatComponent }			from './great.component';
import { TerribleComponent }		from './terrible.component';
import { ThanksComponent }			from './thanks.component';
// @formatter:on


const routes: Routes =
[
	{
		path: '',
		redirectTo: '/dashboard',
		pathMatch: 'full'
	},
	{
		path: 'great',
		component: GreatComponent
	},
	{
		path: 'dashboard',
		component: DashboardComponent,
	},
	{
		path: 'terrible',
		component: TerribleComponent,
	},
	{
		path: 'thanks',
		component: ThanksComponent,
	}
];

@NgModule(
{
	imports:
	[
		RouterModule.forRoot(
			routes,
			{enableTracing: false}),
	],
	exports:
	[
		RouterModule,
	]
})
export class AppRoutingModule {}
