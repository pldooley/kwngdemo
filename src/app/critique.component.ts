/**********************************************************************
 *
 * app/class/acc/hcnHttpAcc.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

/*

import { ActivatedRoute }					 from "@angular/router";
import { Component }								from '@angular/core';
import { Location }								 from "@angular/common";
import { OnDestroy }								from '@angular/core';
import { OnInit }									 from '@angular/core';
import { Router }									 from '@angular/router';
import { Subscription }						 from "rxjs/Subscription";

import { Critique }								 from "./state/critique/critique";
import { kwActEnum }						 from "../kw/enum/kwActEnum";
import { hcnGRMetricEnum }							 from "./enum/hcnGRMetricEnum";
import { OptionChoice }						 from './types/option-choice';
import { hcnCritiqueData		 }		from './state/critique/hcnCritiqueData';
import { kwMetricMsg }		from './state/metric/kwMetricMsg';
import { hcnOptionsState }				 from './state/hcnOptionsState';
import { srvcStatePage }						from './state/page/srvcStatePage';
import { kwTitleType }										from './types/title';


const PAGE =
{
	title:
	{
		sMain: "WE'D LIKE TO IMPROVE.",
		sSub: "What wasn't amazing?",
		nHeight: 35,
	},
	sImage: "/fairmont.jpg",
	logo:
	{
		sUrl: "/Fairmont_Logo.gif"
	},
	bIsXVisible: true,
	sUrl: "/terrible/critique"
};

const CRITIQUE: Critique = {
	sPhone: "",
	sEmail: "",
	sCritique: "",
};

const TITLE: kwTitleType =
{
 sMain: "What could be improved?",
 sSub: "We'd love any feed back."
};


const sTHANKS = "/thanks";

const eMETRIC = hcnGRMetricEnum.STEP_TWO_BAD_CANCELLED;


@Component(
{
	selector: 'hcn-critique',
	templateUrl: './critique.component.html',
	styleUrls: [ './critique.component.css' ]
})
export class CritiqueComponent implements OnDestroy, OnInit
{

	info: OptionChoice;
	data: kwTitleType = TITLE;
	sub: Subscription;

	critique: Critique = CRITIQUE;

	constructor(	private location: Location,
								private route: ActivatedRoute,
								private router: Router,
								private srvcCritique: hcnCritiqueData,
								private srvcMetAct: kwMetricMsg,
								private srvcOptions: hcnOptionsState,
								private srvcPage: srvcStatePage )
	{
		//console.log("CritiqueComponent::constructor() called.");
	}

	ngOnInit(): void
	{
		//console.log("CritiqueComponent::ngOnInit() called.");

		this.sub = this.route.params.subscribe(params =>
		{
			//console.log("CritiqueComponent::route::changed() called.");

			let nId = +params['id']

			this.retrieveOption(nId);

		})

		this.srvcPage.change(PAGE);

	}

	ngOnDestroy()
	{
		//console.log("CritiqueComponent::ngOnDestroy() called.");

		this.sub.unsubscribe();
	}

	onCancel()
	{
		//console.log("CritiqueComponent::onCancel() called.");

		let data = {
			nMethod: kwActEnum.Post,
			data: {
				"Button": hcnGRMetricEnum[eMETRIC]
			 }
		};

		this.srvcMetAct.change(data);

		this.location.back();
	}

	onSubmit()
	{
		//console.log("CritiqueComponent::onSubmit() called.");

		if (!this.critique)
		{
			console.error("CritiqueComponent::onSubmit() critique is invalid.");
			return;
		}
		//console.info("CritiqueComponent::onSubmit() critique is [", this.critique, "]");

		if (!this.critique.sCritique)
		{
			//console.info("CritiqueComponent::onSubmit() sCritique is invalid.");
			return;
		}
		//console.info("CritiqueComponent::onSubmit() sCritique is [", this.critique.sCritique, "]");

		this.srvcCritique.change(this.critique);

		this.router.navigate([sTHANKS]);
	}

	retrieveOption(nId: number): void
	{
		//console.log("CritiqueComponent::retrieveOption() called.");

		if (nId < 0)
		{
			console.error("CritiqueComponent::retrieveOption() nId is invalid.");
			return;
		}
		//console.info("CritiqueComponent::retrieveOption() nId is [", nId, "]");

		this.info = this.srvcOptions.getById(nId);
		if (!this.info)
		{
			console.error("CritiqueComponent::retrieveOption() info is invalid.");
			return;
		}
		//console.info("CritiqueComponent::retrieveOption() info is [", this.info, "]");
	}


}

 */
