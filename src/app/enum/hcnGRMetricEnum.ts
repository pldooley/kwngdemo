/**********************************************************************
 *
 * app/enum/hcnGRMetricEnum.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

export enum hcnGRMetricEnum
{
	STEP_ONE_BAD = 0,
	STEP_ONE_BAD_CANCELLED,
	STEP_ONE_GOOD,
	STEP_ONE_GOOD_CANCELLED,
	STEP_TWO_BAD_CANCELLED,
}
