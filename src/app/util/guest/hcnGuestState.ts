/**********************************************************************
 *
 * app/util/guest/hcnGuestState.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { Component }			        from '@angular/core';
import { OnInit }				        from '@angular/core';
import { OnDestroy }			        from '@angular/core';
import { Subscription }	                from 'rxjs/Subscription';

import { kwApi }                        from "../../../kw/class/api/kwApi";
import { kwApisData }                   from '../../../kw/main/apis/kwApisData';

import { hcnGuestApi }	                from './hcnGuestApi';
// @formatter:off


const sSTATE: string = "guest";


@Component({
	selector: 'hcn-guest-state',
	template: ``,
})
export class hcnGuestState implements OnInit, OnDestroy
{
	subApis: Subscription;

	constructor(    private srvcApi: hcnGuestApi,
	                private srvcApis: kwApisData    )
	{
		//console.log("hcnGuestState::constructor() called");
	}

	ngOnInit()
	{
		//console.log("hcnGuestState::ngOnInit() called");

		this.subApis = this.srvcApis.changed$.subscribe(apis =>
		{
			this.loadUrl();
		});
	};

	ngOnDestroy()
	{
		//console.log("hcnGuestState::ngOnDestroy() called.");

		this.subApis.unsubscribe();
	}

	loadUrl()
	{
		//console.log("hcnGuestState::loadApi() called");

		let api: kwApi = this.srvcApis.retrieveItem(sSTATE);
		if (!kwApi.is(api))
		{
			console.error("hcnGuestState::retrieveApi() api is invalid.");
			return;
		}
		//console.info("hcnGuestState::loadApi() api is ", api);

		this.srvcApi.change(api);
	};

}
