/**********************************************************************
 *
 * app/util/guest/hcnGuestForm.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { Injectable }	            from '@angular/core';

import { kw }			            from '../../../kw/kw';
import { kwForm }                   from "../../../kw/class/form/kwForm";
import { kwFormEnum }               from "../../../kw/class/form/kwFormEnum";
import { kwFormSrvc }               from "../../../kw/class/form/kwFormSrvc";

import { hcnGuestMsg }              from "./hcnGuestMsg";
import { hcnGuestMdl }              from "./hcnGuestMdl";

// @formatter:off


@Injectable()
export class hcnGuestForm
{

	constructor(    private srvcMsg: hcnGuestMsg,
					private srvcMdl: hcnGuestMdl  )
	{
		//console.log("hcnGuestForm::constructor() called.");
	}

	createRecord(nForm: kwFormEnum, data: object): object
	{
		//console.log("hcnGuestForm::createRecord() called");

		if (!kwFormSrvc.in(nForm))
		{
			console.error("hcnGuestForm::createRecord() nForm is invalid");
			return;
		}

		let record: object;

		switch(nForm)
		{
			case kwFormEnum.Add:
			{
				let form: kwForm = this.srvcMdl.create();
				if (!kwForm.is(form))
				{
					console.error("hcnGuestForm::createRecord() form is invalid");
					return;
				}
				//console.info("hcnGuestForm::createRecord() form is ", form)

				record = this.loadMeta(form);
				break;
			}

			case kwFormEnum.Edit:
			{
				record = this.loadMeta(data);
				break;
			}

			case kwFormEnum.View:
			{
				record = data;
				break;
			}

			default:
			{
				console.error("hcnGuestForm::createRecord() nForm is invalid");
			}
		}

		if (kw.isNull(record))
		{
			console.error("hcnGuestForm::createRecord() error creating record.");
			return;
		}

		return record;
	}

	loadMeta(obj)
	{
		//console.log("hcnGuestForm::loadMeta() called.");

		if (kw.isNull(obj))
		{
			console.error("hcnGuestForm::loadMeta() obj is invalid");
			return;
		}

		return obj;
	};

	save(nForm: kwFormEnum, obj: object): boolean
	{
		//console.log("hcnGuestForm::save() called");

		if (!kwFormSrvc.in(nForm))
		{
			console.error("hcnGuestForm::save() nForm is invalid");
			return false;
		}

		if (kw.isNull(obj))
		{
			console.error("hcnGuestForm::save() obj is invalid");
			return false;
		}

		let objX = this.srvcMdl.xExportRec(obj);
		if (kw.isNull(objX))
		{
			console.error("hcnGuestForm::save() objX is invalid");
			return false;
		}

		switch(nForm)
		{
			case kwFormEnum.Add:
			{
				this.srvcMsg.actionAdd(objX, []);
				break;
			}

			case kwFormEnum.Edit:
			{
				let nId = objX.id;
				if (!kw.isNumber(nId))
				{
					console.error("hcnGuestForm::save() nId is invalid");
					return;
				}

				this.srvcMsg.actionEdit(objX, [nId]);

				break;
			}

			default:
			{
				console.error("hcnGuestForm::save() nForm is invalid");
				return false;
			}
		}
	}

	xExport(rec: object): object
	{
		return this.srvcMdl.xExportRec(rec);
	}

}




