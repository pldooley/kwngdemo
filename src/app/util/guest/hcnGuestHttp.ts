/**********************************************************************
 *
 * app/util/guest/hcnGuestHttp.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { Component }					from '@angular/core';
import { OnInit }						from '@angular/core';
import { OnDestroy }					from '@angular/core';
import { Subscription }					from 'rxjs/Subscription';

import { kw }					        from '../../../kw/kw';
import { kwHttpMsg }					from '../../../kw/http/kwHttpMsg';
import { kwMsg }                        from "../../../kw/class/msg/kwMsg";

import { hcnGuestMsg }	                from './hcnGuestMsg';
import { hcnGuestData }		            from './hcnGuestData';
// @formatter:on


const sSTATE: string = "guest";

@Component({
	selector: 'hcn-guest-http',
	template: ``,
})
export class hcnGuestHttp implements OnInit, OnDestroy
{

	subMsg: Subscription;

	constructor(    private srvcMsg: hcnGuestMsg,
	                private srvcData: hcnGuestData )
	{
		//console.log("hcnGuestHttp::constructor() called");
	}

	ngOnInit()
	{
		//console.log("hcnGuestHttp::ngOnInit() called");

		this.subMsg = this.srvcMsg.changed$.subscribe(info =>
		{
			this.inspect(info);
		});

	};

	ngOnDestroy()
	{
		//console.log("hcnGuestHttp::ngOnDestroy() called.");

		this.subMsg.unsubscribe();
	}

	execute(msg: any): void
	{
		//console.log("hcnGuestHttp::execute() called");

		let promise = kwHttpMsg.single(msg);

		if (!kw.isNull(promise))
		{
			console.error("hcnGuestHttp::execute() promise is invalid.");
			return;
		}

		promise.then(function (data)
		{
			//console.info("hcnGuestHttp::execute::then() called.");

			if (!kw.isNull(data))
			{
				if (!msg.isDelete())
				{
					console.error("hcnGuestHttp::execute::then() data is invalid");
					return;
				}
			}

			//console.info("hcnGuestHttp::execute::then() data is ", data);

			this.srvcData.change(data);

		}), function(reason)
		{
			console.error("hcnGuestHttp::execute::error() called");
			alert('Failed: ' + reason);
			this.srvcData.change(null);
		}
	};

	inspect(msg)
	{
		//console.log("hcnGuestHttp::inspectAction() called");

		if (!kwMsg.is(msg))
		{
			console.error("hcnGuestHttp::inspectAction() msg is invalid.");
			return;
		}

		//console.info("hcnGuestHttp::inspectAction() msg is valid - esxecuting.");

		this.execute(msg);
	}


}
