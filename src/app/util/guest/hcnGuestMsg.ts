/**********************************************************************
 *
 * /app/util/guest/hcnGuestMsg.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:on
import { EventEmitter }             from '@angular/core';
import { Injectable }	            from '@angular/core';

import { kwApi }                    from "../../../kw/class/api/kwApi";
import { kwHttpAct }                from "../../../kw/http/kwHttpAct";
import { kwMsg }                    from "../../../kw/class/msg/kwMsg";

import { hcnGuestApi }              from "./hcnGuestApi";
// @formatter:off



@Injectable()
export class hcnGuestMsg
{

	private broadcast: EventEmitter<kwMsg>;

	data: kwMsg;

	changed$: EventEmitter<kwMsg>;

	constructor(private srvcApi: hcnGuestApi)
	{
		//console.log("hcnGuestMsg::constructor() called.");

		this.broadcast = new EventEmitter<kwMsg>();
		this.changed$=this.broadcast;
	}

	actionAdd(data, params)
	{
		//console.log("hcnGuestMsg::actionAdd() called.");


		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("hcnGuestMsg::actionAdd() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.add(data, params, api);
		if (!kwMsg.is(msg))
		{
			console.error("hcnGuestMsg::actionAdd() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionDelete(params)
	{
		//console.log("hcnGuestMsg::actionDelete() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("hcnGuestMsg::actionDelete() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.delete(params, api);
		if (!kwMsg.is(msg))
		{
			console.error("hcnGuestMsg::actionDelete() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionEdit(data, params)
	{
		//console.log("hcnGuestMsg::actionEdit() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("hcnGuestMsg::actionEdit() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.edit(data, params, api)
		if (!kwMsg.is(msg))
		{
			console.error("hcnGuestMsg::actionEdit() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionGet(params)
	{
		//console.log("hcnGuestMsg::actionGet() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("hcnGuestMsg::actionGet() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.get(params, api);
		if (!kwMsg.is(msg))
		{
			console.error("hcnGuestMsg::actionGet() msg is invalid.");
			return;
		}

		this.change(msg);

	};

	change(data: kwMsg)
	{
		//console.log("hcnGuestMsg::change() consolealled.");

		if (!kwMsg.is(data))
		{
			console.error("hcnGuestMsg::change() data is invalid.");
			return
		}
		//console.info("hcnGuestMsg::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear()
	{
		//console.log("hcnGuestMsg::clear() called.");
		this.data = null;
	};

	retrieve(): kwMsg
	{
		//console.log("hcnGuestMsg::retrieve() called.");
		return this.data;
	};

}
