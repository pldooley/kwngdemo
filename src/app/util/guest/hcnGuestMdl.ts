/**********************************************************************
 *
 * app/util/guest/hcnGuestMdl.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }             from '@angular/core';
import { Injectable }	            from '@angular/core';

import { kw }			            from '../../../kw/kw'; 
import { kwModelSrvc }              from "../../../kw/class/model/kwModelSrvc";
// @formatter:off



@Injectable()
export class hcnGuestMdl
{

	// Observable email sources
	private broadcast: EventEmitter<any>;

	data: any;

	// Observable string streams
	changed$ = this.broadcast;

	constructor()
	{
		//console.log("hcnGuestMdl::constructor() called.");

		this.broadcast = new EventEmitter<any>();
		this.changed$=this.broadcast;
	}

	change(dataNew)
	{
		//console.log("hcnGuestMdl::changed() called.");

		if (kw.isNull(dataNew))
		{
			console.error("hcnGuestMdl::changed() dataNew is invalid.");
			return;
		}

		this.data = dataNew;
		//console.info("hcnGuestMdl::changed() data is ", data);

		this.broadcast.emit(this.data);
	};

	clear()
	{
		this.data = null;
	};

	create()
	{
		//console.log("hcnGuestMdl::create() called.");

		if (kw.isNull(this.data))
		{
			console.error("hcnGuestMdl::create() data is invalid.");
			return false;
		}

		let rec = this.data.createRecord();
		if (kw.isNull(rec))
		{
			console.error("hcnGuestMdl::xImport() rec is invalid.");
			return;
		}

		return rec;
	};

	retrieve()
	{
		return this.data;
	};

	xExport(recs)
	{
		return kwModelSrvc.xExport(this.data, recs);
	};

	xExportRec(rec)
	{
		return kwModelSrvc.xExport(this.data, rec);
	};

	xImport(recs)
	{
		return kwModelSrvc.xImport(this.data, recs);
	};

	xImportRec(rec)
	{
		return kwModelSrvc.xImport(this.data, rec);
	};

}