/**********************************************************************
 *
 * app/util/guest/hcnGuestData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }		    from '@angular/core';
import { Injectable }			from '@angular/core';

import { kw }                   from "../../../kw/kw";
// @formatter:off


@Injectable()
export class hcnGuestData
{

	private broadcast: EventEmitter<object>;

	data: object;

	changed$: EventEmitter<object>;

	constructor()
	{
		//console.log("hcnGuestData::constructor() called.");

		this.broadcast = new EventEmitter<object>();
		this.changed$=this.broadcast;
	}

	change(data: object) : void
	{
		//console.log("hcnGuestData::change() called.");

		if (kw.isNull(data))
		{
			console.error("hcnGuestData::change() data is invalid.");
			return
		}
		//console.info("hcnGuestData::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear() : void
	{
		//console.log("hcnGuestData::clear() called.");
		this.data = null;
	};

	retrieve() : object
	{
		//console.log("hcnGuestData::retrieve() called.");
		return this.data;
	};

}
