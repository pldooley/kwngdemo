/**********************************************************************
 *
 * hcn/http/guest/hcnGuestApi.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import {EventEmitter, Injectable}       from '@angular/core';

import { kwApi }                        from "../../../kw/class/api/kwApi";
// @formatter:on


@Injectable()
export class hcnGuestApi
{

	private broadcast: EventEmitter<kwApi>;

	data: kwApi;

	changed$: EventEmitter<kwApi>;

	constructor()
	{
		//console.log("hcnGuestApi::constructor() called.");

		this.broadcast = new EventEmitter<kwApi>();
		this.changed$ = this.broadcast;
	}

	change(data: kwApi): void
	{
		//console.log("hcnGuestApi::change() called.");

		if( !kwApi.is(data) )
		{
			console.error("hcnGuestApi::change() data is invalid.");
			return
		}
		//console.info("hcnGuestApi::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("hcnGuestApi::clear() called.");
		this.data = null;
	};

	retrieve(): kwApi
	{
		//console.log("hcnGuestApi::retrieve() called.");
		return this.data;
	};

}
