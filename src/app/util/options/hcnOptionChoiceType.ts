/**********************************************************************
 *
 * app/util/options/hcnOptionChoiceType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { kwLinkType }			from '../../../kw/type/kwLinkType';
import { kwTitleType }			from '../../../kw/type/kwTitleType';
// @formatter:off

export class hcnOptionChoiceType {
	nId: number;
	sName: string;
	title: kwTitleType;
	link: kwLinkType;
}
