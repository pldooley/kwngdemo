/**********************************************************************
 *
 * app/util/options/hcnOptionsData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter } from '@angular/core';
import { Injectable } from '@angular/core';

import { hcnOptionChoiceType }						 from './hcnOptionChoiceType';
// @formatter:off

const OPTIONS: hcnOptionChoiceType[] = [
	{
		nId: 0,
		sName: "checkin",
		title: {
			sMain: "Check-In/Out",
			sSub: "Speed, friendliness, or ease of checking in and out",
			sThird: "",
			nHeight: 50
		},
		link: {
			sUrl: "/checkin",
			//sIcon: "/assets/ic_local_offer_black_48dp.png"
			sIcon: "vpn_key"
		},
	},
	{
		nId: 1,
		sName: "room",
		title: {
			sMain: "Room",
			sSub: "Quality of the room, condition of the amenites",
			sThird: "",
			nHeight: 50
		},
		link: {
			sUrl: "/room",
			//sIcon: "/assets/ic_hotel_black_48dp.png"
			sIcon: "hotel"
		},
	},
	{
		nId: 2,
		sName: "service",
		title: {
			sMain: "Service",
			sSub: "Speed or friendliness of the service or staff.",
			sThird: "",
			nHeight: 50
		},
		link: {
			sUrl: "/service",
			//sIcon: "/assets/ic_room_service_black_48dp.png"
			sIcon: "room_service"
		},
	},
	{
		nId: 3,
		sName: "food",
		title: {
			sMain: "Food",
			sSub: "Quality, presentation, or spead of the food or drinks.",
			sThird: "",
			nHeight: 50
		},
		link: {
			sUrl: "/food",
			//sIcon: "/assets/ic_restaurant_black_48dp.png"
			sIcon: "restaurant"
		},
	},
	{
		nId: 4,
		sName: "other",
		title: {
			sMain: "Other",
			sSub: "Quality, presentation, or spead of the food or drinks.",
			sThird: "",
			nHeight: 50
		},
		link: {
			sUrl: "/other",
			//sIcon: "/assets/ic_restaurant_black_48dp.png"
			sIcon: "restaurant"
		},
	},
	{
		nId: 5,
		sName: "massage",
		title: {
			sMain: "Massage",
			sSub: "Quality, presentation, or spead of the food or drinks.",
			sThird: "",
			nHeight: 50
		},
		link: {
			sUrl: "/massage",
			//sIcon: "/assets/ic_restaurant_black_48dp.png"
			sIcon: "restaurant"
		},
	}
];

@Injectable()
export class hcnOptionsData{

	private broadcast: EventEmitter<hcnOptionChoiceType[]>;

	data: hcnOptionChoiceType[] = OPTIONS;

	changed$ = this.broadcast;

	constructor()
	{
		//console.log("hcnOptionsData::constructor() called.");

		this.broadcast = new EventEmitter<hcnOptionChoiceType[]>();
		this.changed$=this.broadcast;
	}

	change(options: hcnOptionChoiceType[])
	{
		//console.log("hcnOptionsData::change() called.");

		this.data = options;

		this.broadcast.emit(options);
	}

	clear()
	{
		//console.log("hcnOptionsData::clear() called.");
	};

	retrieve() : hcnOptionChoiceType[]
	{
		//console.log("hcnOptionsData::retrieve() called.");

		return this.data;
	};

	getById(nId) : hcnOptionChoiceType
	{
		//console.log("hcnOptionsData::getById() called.");

		return this.data[nId];
	};

}
