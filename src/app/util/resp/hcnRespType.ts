/**********************************************************************
 *
 * app/util/resp/hcnRespType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
// @formatter:off

export class hcnRespType {
	GuestResp?: string;
	GuestPhone?: string;
	GuestEmail?: string;
	GuestRoom?: string;
	GuestFirst?: string;
	GuestLast?: string;
	TargetEmail: string;
	TimeStamp: number;
}
