/**********************************************************************
 *
 * app/util/resp/hcnRespMsg.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }			    from '@angular/core';
import { Injectable }			    from '@angular/core';

import { kwApi }                    from "../../../kw/class/api/kwApi";
import { kwHttpAct }                from "../../../kw/http/kwHttpAct";
import { kwMsg }                    from "../../../kw/class/msg/kwMsg";

import { hcnRespApi }               from "./hcnRespApi";
// @formatter:off


@Injectable()
export class hcnRespMsg
{

	private broadcast: EventEmitter<kwMsg>;

	data: kwMsg;

	changed$: EventEmitter<kwMsg>;

	
	constructor(private srvcApi: hcnRespApi)
	{
		//console.log("hcnRespMsg::constructor() called.");

		this.broadcast = new EventEmitter<kwMsg>();
		this.changed$=this.broadcast;
	}

	actionAdd(data, params): void
	{
		//console.log("kwAccAct::actionAdd() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwAccAct::actionAdd() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.add(data, params, api)
		if (!kwMsg.is(msg))
		{
			console.error("kwAccAct::actionAdd() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionDelete(params): void
	{
		//console.log("kwAccAct::actionDelete() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwAccAct::actionDelete() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.delete(params, api)
		if (!kwMsg.is(msg))
		{
			console.error("kwAccAct::actionDelete() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionEdit(data, params): void
	{
		//console.log("kwAccAct::actionEdit() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwAccAct::actionEdit() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.edit(data, params, api)
		if (!kwMsg.is(msg))
		{
			console.error("kwAccAct::actionEdit() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	actionGet(params): void
	{
		//console.log("kwAccAct::actionGet() called.");

		let api: kwApi = this.srvcApi.retrieve();
		if (!kwApi.is(api))
		{
			console.error("kwAccAct::actionGet() api is invalid.");
			return;
		}

		let msg: kwMsg = kwHttpAct.get(params, api)
		if (!kwMsg.is(msg))
		{
			console.error("kwAccAct::actionGet() msg is invalid.");
			return;
		}

		this.change(msg);
	};

	change(data: kwMsg): void
	{
		//console.log("hcnRespMsg::change() called.");

		if (!kwMsg.is(data))
		{
			console.error("hcnRespMsg::change() data is invalid.");
			return
		}
		//console.info("hcnRespMsg::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("hcnRespMsg::clear() called.");
		this.data = null;
	};

	retrieve() : kwMsg
	{
		//console.log("hcnRespMsg::retrieve() called.");
		return this.data;
	};

}
