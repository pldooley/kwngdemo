/**********************************************************************
 *
 * app/util/resp/hcnRespApi.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import {EventEmitter, Injectable}   from '@angular/core';

import { kwApi }                    from "../../../kw/class/api/kwApi"; 
import { kwApiSrvc }                from "../../../kw/class/api/kwApiSrvc";

// @formatter:on


@Injectable()
export class hcnRespApi
{

	private broadcast: EventEmitter<kwApi>;

	data: kwApi;

	changed$: EventEmitter<kwApi>;

	constructor()
	{
		//console.log("hcnRespApi::constructor() called.");
		this.broadcast = new EventEmitter<kwApi>();
		this.changed$ = this.broadcast;
	}

	change(data: kwApi): void
	{
		//console.log("hcnRespApi::change() called.");

		if(!kwApiSrvc.isType(data))
		{
			console.error("hcnRespApi::change() data is invalid.");
			return
		}
		//console.info("hcnRespApi::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("hcnRespApi::clear() called.");
		this.data = null;
	};

	retrieve(): kwApi
	{
		//console.log("hcnRespApi::retrieve() called.");
		return this.data;
	};

}
