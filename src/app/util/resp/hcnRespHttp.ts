/**********************************************************************
 *
 * app/util/resp/hcnRespHttp.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { Component }					from '@angular/core';
import { OnInit }						from '@angular/core';
import { OnDestroy }					from '@angular/core';
import { Subscription }					from 'rxjs/Subscription';

import { kw }					        from '../../../kw/kw';
import { kwHttpMsg }					from '../../../kw/http/kwHttpMsg';
import { kwMsg }                        from "../../../kw/class/msg/kwMsg";

import { hcnRespData }                  from "./hcnRespData";
import { hcnRespMsg }	                from './hcnRespMsg';
// @formatter:on


@Component({
	selector: 'hcn-resp-http',
	template: ``,
})
export class hcnRespHttp implements OnInit, OnDestroy
{

	subMsg: Subscription;

	constructor(    private srvcMsg: hcnRespMsg,
	                private srvcData: hcnRespData )
	{
		//console.log("hcnRespHttp::constructor() called");
	}

	ngOnInit()
	{
		//console.log("hcnRespHttp::ngOnInit() called");

		this.subMsg = this.srvcMsg.changed$.subscribe(msg =>
		{
			this.inspect(msg);
		});
	};

	ngOnDestroy()
	{
		//console.log("hcnRespHttp::ngOnDestroy() called.");

		this.subMsg.unsubscribe();
	}

	execute(msg: any): void
	{
		//console.log("hcnRespHttp::execute() called");

		let promise = kwHttpMsg.single(msg);

		if (!kw.isNull(promise))
		{
			console.error("hcnRespHttp::execute() promise is invalid.");
			return;
		}

		promise.then(function (data)
		{
			//console.info("hcnRespHttp::execute::then() called.");

			if (!kw.isNull(data))
			{
				if (!msg.isDelete())
				{
					console.error("hcnRespHttp::execute::then() data is invalid");
					return;
				}
			}

			//console.info("hcnRespHttp::execute::then() data is ", data);

			this.srvcData.change(data);

		}), function(reason)
		{
			console.error("hcnRespHttp::execute::error() called");
			alert('Failed: ' + reason);
			this.srvcData.change(null);
		}
	};

	inspect(msg)
	{
		//console.log("hcnRespHttp::inspectAction() called");

		if (!kwMsg.is(msg))
		{
			console.error("hcnRespHttp::inspectAction() msg is invalid.");
			return;
		}

		this.execute(msg);
	}

}
