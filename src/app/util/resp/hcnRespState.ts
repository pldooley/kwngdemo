/**********************************************************************
 *
 * app/util/resp/hcnRespState.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { Component }			        from '@angular/core';
import { OnInit }				        from '@angular/core';
import { OnDestroy }			        from '@angular/core';
import { Subscription }	                from 'rxjs/Subscription';

import { kwApi }                        from "../../../kw/class/api/kwApi";
import { kwApisData }                   from "../../../kw/main/apis/kwApisData";

import { hcnRespApi }	                from './hcnRespApi';
// @formatter:off


const sSTATE: string = "response";


@Component({
	selector: 'hcn-resp-state',
	template: ``,
})
export class hcnRespState implements OnInit, OnDestroy
{
	subApis: Subscription;

	constructor(    private srvcApi: hcnRespApi,
	                private srvcApis: kwApisData    )
	{
		//console.log("hcnRespState::constructor() called");
	}

	ngOnInit()
	{
		//console.log("hcnRespState::ngOnInit() called");

		this.subApis = this.srvcApis.changed$.subscribe(apis =>
		{
			this.load();
		});
	};

	ngOnDestroy()
	{
		//console.log("hcnRespState::ngOnDestroy() called.");

		this.subApis.unsubscribe();
	}

	load()
	{
		//console.log("hcnRespState::load() called");

		let api: kwApi = this.srvcApis.retrieveItem(sSTATE);
		if (!kwApi.is(api))
		{
			console.error("hcnRespState::load() api is invalid.");
			return;
		}
		//console.info("hcnRespState::load() api is ", api);

		this.srvcApi.change(api);
	};

}
