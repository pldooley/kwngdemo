/**********************************************************************
 *
 * app/util/resp/hcnRespData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }			    from '@angular/core';
import { Injectable }				from '@angular/core';

import { kw }                       from "../../../kw/kw";
// @formatter:off


@Injectable()
export class hcnRespData
{

	private broadcast: EventEmitter<object>;

	data: object;

	changed$: EventEmitter<object>;

	constructor()
	{
		//console.log("hcnRespData::constructor() called.");

		this.broadcast = new EventEmitter<object>();
		this.changed$=this.broadcast;
	}

	// Service resp commands
	change(data: object)
	{
		//console.log("hcnRespData::change() called.");

		if (kw.isNull(data))
		{
			console.error("hcnRespData::change() data is invalid.");
			return
		}
		console.error("hcnRespData::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("hcnRespData::clear() called.");
		this.data = null;
	};

	retrieve() : object
	{
		//console.log("hcnRespData::retrieve() called.");
		return this.data;
	};

}
