/**********************************************************************
 *
 * app/util/critique/hcnCritiqueType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

export class hcnCritiqueType
{
	sPhone: string;
	sEmail: string;
	sCritique: string;
}
