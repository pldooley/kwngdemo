/**********************************************************************
 *
 * app/util/critique/hcnCritiqueState.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { Component }						from '@angular/core';
import { OnInit }							from '@angular/core';
import { OnDestroy }						from '@angular/core';
import { Subscription }						from 'rxjs/Subscription';

import { kw }                               from "../../../kw/kw";

import { hcnCritiqueData }				    from './hcnCritiqueData';
import { hcnCritiqueType }					from "./hcnCritiqueType";
import { hcnEmailsKeyData }				    from '../../key/emails/hcnEmailsKeyData';
import { hcnEmailType }                     from "../../type/hcnEmailType";
import { hcnGuestData }				        from '../guest/hcnGuestData';

import { hcnRespMsg }                       from "../resp/hcnRespMsg";
import { hcnRespType }						from '../resp/hcnRespType';
// @formatter:off


@Component({
	selector: 'hcn-critique-state',
	template: ``,
})
export class hcnCritiqueState implements OnInit, OnDestroy
{
	sub: Subscription;

	critique: hcnCritiqueType;
	guest: object;
	emails: hcnEmailType[];

	constructor(	private srvcData: hcnCritiqueData,
					private srvcEmails: hcnEmailsKeyData,
					private srvcGuest: hcnGuestData,
					private srvcMsg: hcnRespMsg		)
	{
		//console.log("hcnCritiqueState::constructor() called");
	}

	ngOnInit()
	{
		//console.log("hcnCritiqueState::ngOnInit() called");

		this.sub = this.srvcData.changed$.subscribe(info =>
		{
			this.send(info);
		});

		this.retrieveEmails();
		this.retrieveGuest();
	};

	ngOnDestroy()
	{
		//console.log("hcnCritiqueState::ngOnDestroy() called.");

		this.sub.unsubscribe();
	}

	retrieveGuest()
	{
		//console.log("hcnCritiqueState::retrieveGuest() called");

		let guest: object = this.srvcGuest.retrieve();
		if (kw.isNull(guest))
		{
			console.error("hcnCritiqueState::retrieveGuest() guest is invalid.");
			return;
		}
		//console.info("hcnCritiqueState::retrieveGuest() guest is ", guest);

		this.guest = guest;
	};

	retrieveEmails(): void
	{
		//console.log("hcnCritiqueState::retrieveEmails() called.");

		let emails: hcnEmailType[] = this.srvcEmails.retrieve();
		if (!emails || emails.length === 0)
		{
			console.error("hcnCritiqueState::retrieveEmails() emails is invalid.");
			return;
		}
		//console.info("hcnCritiqueState::retrieveEmails() emails is [", emails, "]");

		this.emails = emails;
	};

	send(data: hcnCritiqueType): void
	{
		//console.log("hcnCritiqueState::send() called.");

		if (!data)
		{
			console.error("hcnCritiqueState::send() data is invalid.");
			return;
		}
		//console.info("hcnCritiqueState::send() data is [", data, "]");

		if (!this.emails || this.emails.length === 0)
		{
			console.error("hcnCritiqueState::send() emails is invalid.");
			return;
		}
		//console.info("hcnCritiqueState::send() emails is [", this.emails, "]");

		if (!this.guest)
		{
			console.error("hcnCritiqueState::send() guest is invalid.");
			return;
		}
		//console.info("hcnCritiqueState::send() guest is [", this.guest, "]");

		let date = new Date();
		//console.info("hcnCritiqueState::send() date is [", date, "]");

		let nTime = date.getTime();
		//console.info("hcnCritiqueState::send() nTime is [", nTime, "]");

		let resp: hcnRespType = {
			GuestEmail: data.sEmail,
			GuestPhone: data.sPhone,
			GuestResp: data.sCritique,
			//GuestFirst: this.guest.sNameFirst,
			//GuestLast: this.guest.sNameLast,
			//GuestRoom: this.guest.sRoom,
			TargetEmail: this.emails.toString(),
			TimeStamp: nTime,
		};
		//console.info("hcnCritiqueState::send() resp is [", resp, "]");

		this.srvcMsg.actionAdd(resp, []);
	}

}
