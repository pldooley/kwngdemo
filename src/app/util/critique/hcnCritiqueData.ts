/**********************************************************************
 *
 * app/util/critique/hcnCritiqueData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter }             from '@angular/core';
import { Injectable }               from '@angular/core';

import { hcnCritiqueType }			from './hcnCritiqueType';
// @formatter:off



@Injectable()
export class hcnCritiqueData
{
	private broadcast: EventEmitter<hcnCritiqueType>;

	data: hcnCritiqueType;

	changed$: EventEmitter<hcnCritiqueType>;

	constructor()
	{
		//console.log("hcnCritiqueData::constructor() called.");

		this.broadcast = new EventEmitter<hcnCritiqueType>();
		this.changed$=this.broadcast;
	}

	change(data: hcnCritiqueType) : void
	{
		//console.log("hcnCritiqueData::change() called.");

		if (!data)
		{
			console.error("hcnCritiqueData::change() data is invalid.");
			return
		}
		//console.info("hcnCritiqueData::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear() : void
	{
		//console.log("hcnCritiqueData::clear() called.");
		this.data = null;
	};

	retrieve() : hcnCritiqueType
	{
		//console.log("hcnCritiqueData::retrieve() called.");
		return this.data;
	};

}
