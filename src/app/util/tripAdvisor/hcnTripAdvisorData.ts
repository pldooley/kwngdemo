/**********************************************************************
 *
 * app/util/tripAdvisor/hcnTripAdvisorData.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { EventEmitter } from '@angular/core';
import { Injectable }	 from '@angular/core';
// @formatter:off


const sURL: string =
	"//www.tripadvisor.com/UserReviewEdit-g155004-d4852931-Dandy_Brisket-Ottawa_Ontario.html";


@Injectable()
export class hcnTripAdvisorData{

	// Observable email sources
	private broadcast: EventEmitter<string>;

	data: string = sURL;

	// Observable string streams
	changed$ = this.broadcast;


	constructor(){

		//console.log("hcnTripAdvisorData::constructor() called.");

		this.broadcast = new EventEmitter<string>();
		this.changed$=this.broadcast;
	}

	change(data: string): void
	{

		//console.log("hcnTripAdvisorData::change() called.");

		if (!data) {
			console.error("hcnTripAdvisorData::change() data is invalid.");
			return
		}
		//console.info("hcnTripAdvisorData::change() data is [", data, "]");

		this.data = data;

		this.broadcast.emit(data);
	}

	clear(): void
	{
		//console.log("hcnTripAdvisorData::clear() called.");
		this.data = null
	};

	retrieve() : string
	{
		//console.log("hcnTripAdvisorData::retrieve() called.");
		return this.data;
	};


}
