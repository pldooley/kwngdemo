/**********************************************************************
 *
 * app/util/tripAdvisor/hcnTripAdvisorState.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { Component }					from '@angular/core';
import { OnDestroy }					from '@angular/core';
import { OnInit }						from '@angular/core';
// @formatter:off

const sKEY: string = 'DefTripAdvisorUrl';

@Component({
	selector: 'hcn-tripadvisor-state',
	template: ``,
})
export class hcnTripAdvisorState implements OnInit, OnDestroy
{

	constructor(		)
	{

		//console.log("hcnTripAdvisorState::constructor() called");
	}

	ngOnDestroy()
	{
		//console.log("hcnTripAdvisorState::ngOnDestroy() called");

	};

	ngOnInit()
	{
		//console.log("hcnTripAdvisorState::ngOnInit() called");

	};




}
