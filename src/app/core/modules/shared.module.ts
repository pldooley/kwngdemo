/**********************************************************************
 *
 * app/class/acc/hcnHttpAcc.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { NgModule }							 from '@angular/core';

import { CommonModule }					 from '@angular/common';
import { FormsModule }						from '@angular/forms';
import { ReactiveFormsModule }		from '@angular/forms';

import { MaterialModule }				 from './material.module';
import { FlexLayoutModule }			 from '@angular/flex-layout';
// @formatter:on


@NgModule({
	declarations: [],
	imports: [
		CommonModule,
		FlexLayoutModule,
		FormsModule,
		MaterialModule,
		ReactiveFormsModule,
	],
	exports: [
		CommonModule,
		FlexLayoutModule,
		FormsModule,
		MaterialModule,
		ReactiveFormsModule,
	],
	entryComponents: [],
	providers: []
})

export class SharedModule
{

}
