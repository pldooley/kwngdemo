/**********************************************************************
 *
 * app/class/acc/hcnHttpAcc.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { NgModule }									 from '@angular/core';

import {
	MatButtonModule,
	MatDialogModule,
	MatFormFieldModule,
	MatIconModule,
	MatInputModule,
	MatSelectModule,
	MatStepperModule,
} from '@angular/material';
// @formatter:on
@NgModule(
	{
		imports: [
			MatButtonModule,
			MatDialogModule,
			MatFormFieldModule,
			MatIconModule,
			MatInputModule,
			MatSelectModule,
			MatStepperModule,
		],
		exports: [
			MatButtonModule,
			MatDialogModule,
			MatFormFieldModule,
			MatIconModule,
			MatInputModule,
			MatSelectModule,
			MatStepperModule,
		],
	})
export class MaterialModule {}
