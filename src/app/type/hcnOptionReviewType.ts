/**********************************************************************
 *
 * app/type/hcnOptionReviewType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { kwLinkType }			    from '../../kw/type/kwLinkType';
import { kwTitleType }			    from '../../kw/type/kwTitleType';
// @formatter:on

export class hcnOptionReviewType
{
	title: kwTitleType;
	link: kwLinkType;
}
