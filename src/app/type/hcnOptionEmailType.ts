/**********************************************************************
 *
 * app/type/hcnOptionEmailType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { kwTextType }		    from '../../kw/type/kwTextType';
import { kwTitleType }			from '../../kw/type/kwTitleType';
// @formatter:on

export class hcnOptionEmailType
{
	email: kwTextType;
	title?: kwTitleType;
}
