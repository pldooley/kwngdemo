/**********************************************************************
 *
 * app/great.component.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { Component }						from '@angular/core';
import { MatDialogRef }						from "@angular/material";
import { OnInit }							from '@angular/core';

import { kwDlg }							from "../kwDlg/asset/kwDlg";
import { kwDlgStateData }					from '../kwDlg/state/kwDlgStateData';
import { kwDlgType }					    from '../kwDlg/type/kwDlgType';
import { kwDlgTitleType }				    from '../kwDlg/type/kwDlgTitleType';
import { kwMetric }                         from "../kw/class/metric/kwMetric";
import { kwMetricMsg }                      from "../kw/main/metric/kwMetricMsg";
import { kwMetricSrvc }                     from "../kw/class/metric/kwMetricSrvc";
import { kwMetricType }                     from "../kw/class/metric/kwMetricType";

import { hcnGRMetricEnum }					from "./enum/hcnGRMetricEnum";
import { hcnOptionEmailType }				from './type/hcnOptionEmailType';
import { hcnOptionReviewType }				from './type/hcnOptionReviewType';
// @formatter:on

const DLG_TITLE: kwDlgTitleType = {
	title: {
		sMain: "NICE WE LIKE TO HEAR THAT!",
		sSub: "Care to leave a review?",
		nHeight: 35,
	},
	sImage: "/fairmont.jpg",
	logo: {
		sUrl: "/Fairmont_Logo.gif"
	},
	bIsXVisible: true,
	sUrl: "/great"
};


const DLG: kwDlgType = {
	title: DLG_TITLE,
};

const INFO_LEFT: hcnOptionReviewType = {
	title: {
		sMain: "Ok",
		sSub: "We will redirect you to tripAdvisor's form.",
		sThird: "",
		nHeight: 50
	},
	link: {
		sUrl: "/great",
		sIcon: "check_circle",
	},
};

const INFO_RIGHT: hcnOptionEmailType = {
	title: {
		sMain: "No thanks",
		sSub: "We will send a review form after your stay.",
		sThird: "",
		nHeight: 50
	},
	email: {
		sHint: "e.g. Harry@gmail.com",
		sPlaceHolder: "Your E-mail address"
	},
};

const sTA: string = "https://static.tacdn.com/"
	+ "UserReview-g155004-d4852931-m29834-Dandy_Brisket-Ottawa_Ontario.html";

const sMETRIC               = "STEP_ONE_GOOD";
const sMETRIC_CANCELLED     = "STEP_ONE_GOOD_CANCELLED";


@Component({
	selector: 'hcn-great',
	templateUrl: './great.component.html',
	styleUrls: [ './great.component.css' ]
})
export class GreatComponent implements OnInit
{

	dlg: kwDlgType = DLG;
	infoLeft: hcnOptionReviewType = INFO_LEFT;
	infoRight: hcnOptionEmailType = INFO_RIGHT;

	constructor(    private srvcDlg: kwDlgStateData,
					private srvcMsg: kwMetricMsg,
					private dlgRef: MatDialogRef<kwDlg>)
	{
		//console.log("GreatComponent::constructor() called.");
	}

	ngOnInit(): void
	{
		//console.log("GreatComponent::ngOnInit() called.");

		this.srvcDlg.change(this.dlg);

		this.send();
	}

	onCancel(): void
	{
		//console.log("GreatComponent::onCancel() called.");

		let	data: kwMetricType =  {
			sButton: sMETRIC_CANCELLED
		}

		let metric: kwMetric = new kwMetric(data);
		if (!metric.init())
		{
			console.error("GreatComponent::send() error creating metric.");
			return;
		}

		this.srvcMsg.actionAdd(metric, []);

		this.dlgRef.close();
	}

	onSubmit(): void
	{
		//console.log("GreatComponent::onSubmit() called.");

		window.open(sTA);

		this.dlgRef.close();
	}

	send(): void
	{
		//console.log("GreatComponent::send() called.");

		let	type: kwMetricType =  {
			sButton: sMETRIC
		};

		let metric: kwMetric = new kwMetric(type);
		if (!metric.init())
		{
			console.error("GreatComponent::send() error creating metric.");
			return;
		}

		this.srvcMsg.actionAdd(metric, []);
	}

}
