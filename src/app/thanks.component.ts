/**********************************************************************
 *
 * app/class/acc/hcnHttpAcc.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { Component }			    from '@angular/core';
import { OnInit }				    from '@angular/core';

import { kwApp }                    from "../kw/class/app/kwApp";
import { kwAppClose }               from "../kw/class/app/kwAppClose";
import { kwAppData }                from "../kw/state/app/kwAppData";
import { kwDlgStateData }           from "../kwDlg/state/kwDlgStateData";
import { kwDlgTitleType }			from '../kwDlg/type/kwDlgTitleType';
import { kwDlgType }				from '../kwDlg/type/kwDlgType';
// @formatter:on


const TITLE: kwDlgTitleType = {
	title: {
		sMain: "Thank you",
		sSub: "Someone from this hotel will get back to you as soon as possible.",
		sThird: "We appreciate your input.",
		nHeight: 50
	},
	sImage: "/fairmont.jpg",
	logo: {
		sUrl: "/Fairmont_Logo.gif"
	},
	bIsXVisible: true,
	sUrl: "/thanks"
};

const DLG: kwDlgType = {
	title: TITLE
}

const sWISH = "Fairmont San Francisco wishes you happy travels!";


@Component({
	selector: 'hcn-thanks',
	templateUrl: './thanks.component.html',
	styleUrls: [ './thanks.component.css' ]
})
export class ThanksComponent implements OnInit
{

	info: kwDlgType = DLG;
	sWish: string = sWISH;

	constructor(    private srvcApp: kwAppData,
					private srvcDlg: kwDlgStateData )
	{
		//console.log("ThanksComponent::constructor() called.");
	}

	ngOnInit(): void
	{

		//console.log("ThanksComponent::ngOnInit() called.");

		this.srvcDlg.change(this.info);
	}

	onClose(): void
	{
		//console.log("ThanksComponent::onClose() called.");

		let app: kwApp = new kwAppClose();
		if (!app.init)
		{
			console.error("ThanksComponent::onClose() error creating app.");
			return;
		}

		this.srvcApp.change(app);
	}

}

