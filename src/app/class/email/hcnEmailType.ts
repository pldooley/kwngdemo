/**********************************************************************
 *
 * hcn/class/email/hcnEmailType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
// @formatter:on


export class hcnEmailType
{
	sEmail: string;
}
