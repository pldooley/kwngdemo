/**********************************************************************
 *
 * hcn/class/api/hcnEmail.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { kw }		                    from "../../../kw/kw";

import { hcnEmailSrvc }                 from "./hcnEmailSrvc";
import { hcnEmailType }                 from "./hcnEmailType";
// @formatter:on


export class hcnEmail
{
	sEmail: string;

	constructor(private type: hcnEmailType)
	{
		console.log("hcnEmail::constructor() called");
	};

	init(): boolean
	{
		console.log("hcnEmail::init() is called.");

		if (!hcnEmailSrvc.isType(this.type))
		{
			console.error("hcnEmail::init() type is invalid.");
			return false;
		}
		console.info("hcnEmail::init() type is ", this.type);

		let sEmail: string = this.type.sEmail;
		if(!kw.isString(sEmail))
		{
			console.error("hcnEmail::init() sEmail is invalid.");
			return false;
		}
		console.info("hcnEmail::init() sEmail is ", sEmail);
		this.sEmail = sEmail;

		return true;
	}

	geEmail(): string
	{
		return this.sEmail;
	};

	static is(obj: object): boolean
	{
		return kw.is(obj, hcnEmail)
	}

}

