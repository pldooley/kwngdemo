/**********************************************************************
 *
 * hcn/class/email/hcnEmailSrvc.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { kw }		            from "../../../kw/kw";

import { hcnEmailType }			from "./hcnEmailType";
// @formatter:on


export class hcnEmailSrvc
{

	static isType(obj: object): boolean
	{
		return kw.is(obj, hcnEmailType)
	}

	static in(nVal: number): boolean
	{
		return false
	}

	static toEnum(sVal: string): number
	{
		return -1;
	};
}

