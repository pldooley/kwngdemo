/**********************************************************************
 *
 * hcn/class/email/hcnEmailsType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { hcnEmailType }			from "../email/hcnEmailType";
// @formatter:on


export class hcnEmailsType
{
	emails: hcnEmailType[];
}
