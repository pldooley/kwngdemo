/**********************************************************************
 *
 * hcn/class/api/hcnEmails.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { kw }		                    from "../../../kw/kw";
import { hcnEmail }                     from "../email/hcnEmail";
import { hcnEmailType }                 from "../email/hcnEmailType";

import { hcnEmailsSrvc }                from "./hcnEmailsSrvc";
import { hcnEmailsType }                from "./hcnEmailsType";
// @formatter:on


export class hcnEmails
{
	emails: hcnEmail[];

	constructor(private type: hcnEmailsType)
	{
		console.log("hcnEmail::constructor() called");
	};

	init(): boolean
	{
		console.log("hcnEmail::init() is called.");

		if (!hcnEmailsSrvc.isType(this.type))
		{
			console.error("hcnEmail::init() type is invalid.");
			return false;
		}
		console.info("hcnEmail::init() type is ", this.type);

		let types: hcnEmailType[] = this.type.emails;
		if (kw.isNull(types))
		{
			console.error("hcnEmail::init() types is invalid.");
			return false;
		}
		console.info("hcnEmail::init() types is ", types);

		let emails: hcnEmail[] = hcnEmailsSrvc.create(types);
		if (!kw.isNull(emails))
		{
			console.error("hcnEmail::init() error creating emails.");
			return false;
		}
		this.emails = emails;

		return true;
	}

	getEmails(): hcnEmail[]
	{
		return this.emails;
	};

	toString(): string
	{
		return "hello";
	};

	static is(obj: object): boolean
	{
		return kw.is(obj, hcnEmails)
	}


}

