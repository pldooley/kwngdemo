/**********************************************************************
 *
 * hcn/class/email/hcnEmailsSrvc.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 The Hotel Communication Network Incorporated
 *
 **********************************************************************/

// @formatter:off
import { kw }		            from "../../../kw/kw";
import { hcnEmail }		        from "../email/hcnEmail";
import { hcnEmailType }			from "../email/hcnEmailType";
import { hcnEmailSrvc }         from "../email/hcnEmailSrvc";

import { hcnEmailsType }        from "./hcnEmailsType";
// @formatter:on


export class hcnEmailsSrvc
{
	static create(types: hcnEmailType[]): hcnEmail[]
	{
		console.log("hcnEmailsSrvc::init() is called.");

		if(kw.isNull(types))
		{
			console.error("hcnEmailsSrvc::create() types is invalid.");
			return;
		}
		console.info("hcnEmailsSrvc::create() types is ", types);

		let items: hcnEmail[];

		for (let i=0; i< types.length; i++)
		{
			let type: hcnEmailType = types[i];
			if(!hcnEmailSrvc.isType(type))
			{
				console.error("hcnEmailsSrvc::create() type is invalid.");
				return;
			}
			console.info("hcnEmailsSrvc::init() create creating emails.");

			let item: hcnEmail = new hcnEmail(type);
			if (!item.init())
			{
				console.error("hcnEmailsSrvc::create() error creating item.");
				return;
			}
			items.push(item)
		}

		return items;
	}

	static isType(obj: object): boolean
	{
		return kw.is(obj, hcnEmailsType)
	}

	static in(nVal: number): boolean
	{
		return false
	}

	static toEnum(sVal: string): number
	{
		return -1;
	};
}

