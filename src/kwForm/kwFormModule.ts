// @formatter:off
import { NgModule }								 from '@angular/core';

import { kwModule }							    from '../kw/kwModule';

import { kwFormComplete }						from './asset/kwFormComplete';
import { kwFormContact }					    from './asset/kwFormContact';
import { kwFormOption }					        from './asset/kwFormOption';
import { kwFormText }					        from './asset/kwFormText';
// @formatter:on

@NgModule(
	{
		imports:
			[],
		declarations:
			[
				kwFormComplete,
				kwFormContact,
				kwFormOption,
				kwFormText,
			],
		providers:
			[
			],
		exports:
			[
				kwFormComplete,
				kwFormContact,
				kwFormOption,
				kwFormText,
			]
	})
export class kwFormModule {}
