/**********************************************************************
 *
 * kwForm/assets/kwFormComplete.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Component }						from '@angular/core';
import { EventEmitter }						from '@angular/core';
import { Output }							from '@angular/core';
// @formatter:on

@Component({
	selector: 'kw-form-complete',
	templateUrl: './kwFormComplete.html'
})
export class kwFormComplete
{

	@Output() cancel = new EventEmitter<boolean>();
	@Output() submit = new EventEmitter<boolean>();

	constructor()
	{
		console.log("kwFormComplete::constructor() called.");
	}

	onCancel(): void
	{
		console.log("kwFormComplete::onCancel() called.");

		this.cancel.emit(true);
	}

	onSubmit(): void
	{
		console.log("kwFormComplete::onSubmit() called.");

		this.submit.emit(true);
	}

}
