/**********************************************************************
 *
 * kwForm/assets/FormTextComponent.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Component }						from '@angular/core';
import { EventEmitter }						from '@angular/core';
import { FormGroup }						from '@angular/forms';
import { Input }							from "@angular/core";
import { OnInit }							from '@angular/core';
import { Output }							from '@angular/core';
// @formatter:on


@Component({
	selector: 'kw-form-option',
	templateUrl: './kwFormOption.html'
})
export class kwFormOption implements OnInit
{
	@Input() group: FormGroup;

	@Output() cancel = new EventEmitter<boolean>();

	constructor()
	{
		console.log("kwFormOption::constructor() called.");
	}

	ngOnInit(): void
	{
		console.log("kwFormOption::ngOnInit() called.");

	}

	onCancel(): void
	{
		console.log("kwFormOption::onCancel() called.");

		this.cancel.emit(true);
	}

}
