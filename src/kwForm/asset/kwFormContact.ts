/**********************************************************************
 *
 * kwForm/assets/FormTextComponent.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Component }						from '@angular/core';
import { EventEmitter }						from '@angular/core';
import { FormGroup }						from '@angular/forms';
import { Input }							from "@angular/core";
import { Output }							from '@angular/core';
// @formatter:on

@Component({
	selector: 'kw-contact-form',
	templateUrl: './kwFormContact.html'
})
export class kwFormContact
{
	@Input() group: FormGroup;

	@Output() cancel = new EventEmitter<boolean>();

	constructor()
	{
		console.log("kwFormContact::constructor() called.");
	}

	onCancel(): void
	{
		console.log("kwFormContact::onCancel() called.");
		this.cancel.emit(true);
	}
}
