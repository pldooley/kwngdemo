/**********************************************************************
 *
 * kwForm/assets/kwFormText.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { Component }						from '@angular/core';
import { EventEmitter }						from '@angular/core';
import { FormGroup }						from '@angular/forms';
import { Input }							from '@angular/core';
import { Output }							from '@angular/core';
// @formatter:on

@Component({
	selector: 'kw-form-text',
	templateUrl: './kwFormText.html'
})
export class kwFormText
{
	@Input() group: FormGroup;

	@Output() cancel = new EventEmitter<boolean>();

	constructor()
	{
		console.log("kwFormText::constructor() called.");
	}

	onCancel(): void
	{
		console.log("kwFormText::onCancel() called.");

		this.cancel.emit(true);
	}

}
