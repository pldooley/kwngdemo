source ./env_proj.sh

export HCN_HOST='0.0.0.0'
#printenv HCN_HOST

export HCN_DIR='/usr/local/hcn/services'
#printenv HCN_DIR

export HCN_DIR_PROJ=$HCN_DIR"/"$HCN_PROJ
#printenv HCN_DIR_PROJ

export HCN_CONT_PROJ="cont_"$HCN_PROJ
#printenv HCN_CONT_PROJ

export HCN_IMG_PROJ="img_"$HCN_PROJ
#printenv HCN_IMG_PROJ

export HCN_IMG_PROJ_BASE=$HCN_IMG_PROJ"_base"
#printenv HCN_IMG_PROJ_BASE
