# GuestrespMD

Simple Demo App created for demonstration of kWNg

## Build Docker base image
run './buildbase.sh'

## Build Docker image
run './build.sh'

## Run Docker image
run './run.sh'

##debug app while in container
run './debug.sh'

##build release while in container
run './release.sh'
